package com.iping.airbillonline.holder;

import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.iping.airbillonline.R;

public class CodewiseHolder extends RecyclerView.ViewHolder {
    public TextView tv_sr_no;
    public TextView tv_item_name1;
    public TextView tv_rate;
    public TextView tv_qty1;
    public TextView tv_amount;
    public TextView tv_delete;
    public CheckBox chkIos;

    public CodewiseHolder(@NonNull View view) {
        super(view);
        tv_sr_no = view.findViewById(R.id.tv_sr_no);
        tv_item_name1 = view.findViewById(R.id.tv_item_name);
        tv_rate = view.findViewById(R.id.tv_rate);
        tv_qty1 = view.findViewById(R.id.tv_qty);
        tv_amount = view.findViewById(R.id.tv_amount);
        tv_delete = view.findViewById(R.id.tv_delete);
        chkIos = view.findViewById(R.id.chkIos);

    }
}
