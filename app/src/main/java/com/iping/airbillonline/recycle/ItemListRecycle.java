package com.iping.airbillonline.recycle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.iping.airbillonline.R;
import com.iping.airbillonline.pojo.ItemListPOJO;

import java.util.List;

public class ItemListRecycle  extends RecyclerView.Adapter<ItemListRecycle.ItemListHolder> {

    private List<ItemListPOJO> itemList;

    public ItemListRecycle(List<ItemListPOJO> itemList){
        this.itemList=itemList;
    }


    @NonNull
    @Override
    public ItemListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.item_layout,parent,false);
        return new ItemListHolder(itemView);

    }
    @Override
    public void onBindViewHolder(@NonNull ItemListHolder holder, int position) {
        ItemListPOJO item = itemList.get(position);
        holder.tv_itemcode.setText(item.getCode());
        holder.tv_itemname.setText(item.getName());
        holder.tv_itemprice.setText(item.getRate());
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    static class ItemListHolder extends RecyclerView.ViewHolder{
        TextView tv_itemcode;
        TextView tv_itemname;
        TextView tv_itemprice;

        ItemListHolder(@NonNull View itemView) {
            super(itemView);
            tv_itemcode = itemView.findViewById(R.id.tv_itemcode);
            tv_itemname = itemView.findViewById(R.id.tv_itemname);
            tv_itemprice = itemView.findViewById(R.id.tv_itemprice);
        }
    }
}
