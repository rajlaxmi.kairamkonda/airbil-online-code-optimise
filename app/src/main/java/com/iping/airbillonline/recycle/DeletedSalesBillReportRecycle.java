package com.iping.airbillonline.recycle;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.iping.airbillonline.R;
import com.iping.airbillonline.pojo.DeletedSalesBillReportPOJO;
import com.iping.airbillonline.pojo.SalesBillReportPOJO;

import java.util.List;

public class DeletedSalesBillReportRecycle extends RecyclerView.Adapter<DeletedSalesBillReportRecycle.DeletedSalesBillReportHolder> {

    private List<DeletedSalesBillReportPOJO> deletedSalesBillReportPOJOS;

    public DeletedSalesBillReportRecycle(List<DeletedSalesBillReportPOJO> item){
        this.deletedSalesBillReportPOJOS=item;
    }

    @NonNull
    @Override
    public DeletedSalesBillReportHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.delete_sales_layout,parent,false);
        return new DeletedSalesBillReportHolder(itemView);

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull DeletedSalesBillReportHolder holder, int position) {
        DeletedSalesBillReportPOJO item = deletedSalesBillReportPOJOS.get(position);
        holder.tv_sr_no.setText(item.getSerNo());
        holder.tv_bill_no.setText(item.getBillNo()+ "");
        holder.tv_total_amt.setText(item.getTotalAmt() + "");
        holder.tv_cash_credit.setText(item.getCashCredit() + "");
    }

    @Override
    public int getItemCount() {
        return deletedSalesBillReportPOJOS.size();
    }

    static class DeletedSalesBillReportHolder extends RecyclerView.ViewHolder{
        TextView tv_sr_no;
        TextView tv_bill_no;
        TextView tv_total_amt;
        TextView tv_cash_credit;
        TextView tv_delete;

        DeletedSalesBillReportHolder(@NonNull View convertView) {
            super(convertView);
            tv_sr_no = convertView.findViewById(R.id.tv_sr_no);
            tv_bill_no = convertView.findViewById(R.id.tv_bill_no);
            tv_total_amt = convertView.findViewById(R.id.tv_total_amt);
            tv_cash_credit = convertView.findViewById(R.id.tv_cash_credit);
            tv_delete = convertView.findViewById(R.id.tv_delete);

        }
    }
}
