package com.iping.airbillonline.recycle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.iping.airbillonline.R;
import com.iping.airbillonline.pojo.ItemListPOJO;

import java.util.List;

public class CategoryListRecycle extends RecyclerView.Adapter<CategoryListRecycle.CategoryListHolder> {

    private List<ItemListPOJO> itemList;

    public CategoryListRecycle(List<ItemListPOJO> itemList){
        this.itemList=itemList;
    }

    @NonNull
    @Override
    public CategoryListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.categorylayout,parent,false);
        return new CategoryListHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryListHolder holder, int position) {
        ItemListPOJO item = itemList.get(position);
        holder.tv_cat.setText(item.getName());
        holder.tv_cat1.setText(item.getRate());
        holder.tv_cat2.setText(item.getCode());
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    static class CategoryListHolder extends RecyclerView.ViewHolder{
        TextView tv_cat;
        TextView tv_cat1;
        TextView tv_cat2;

        CategoryListHolder(@NonNull View itemView) {
            super(itemView);
            tv_cat = itemView.findViewById(R.id.tv_cat);
            tv_cat1 = itemView.findViewById(R.id.tv_cat1);
            tv_cat2 = itemView.findViewById(R.id.tv_cat2);
        }
    }
}
