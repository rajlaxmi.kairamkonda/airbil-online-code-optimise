package com.iping.airbillonline.recycle;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.iping.airbillonline.R;
import com.iping.airbillonline.dialog.CustomDialog;
import com.iping.airbillonline.fragments.SalesBillReportFragment;
import com.iping.airbillonline.pojo.SalesBillReportPOJO;

import java.util.List;

public class SalesBillReportRecycle extends RecyclerView.Adapter<SalesBillReportRecycle.SalesBillReportHolder> {

    private List<SalesBillReportPOJO> itemList;

    private FragmentActivity activity;

    public void setListener(SalesBillReportRecycle.ItemListener listener) {
        this.listener = listener;
    }

    public void setDeleteSalesBillListener(DeleteItemListener deleteSalesBillListener) {
        this.deleteSalesBillListener = deleteSalesBillListener;
    }

    public interface ItemListener{
        void onClick(SalesBillReportPOJO item);
    }

    public interface DeleteItemListener{
        void onDeleteClick(final String cat_id, final String catname, String title, String msg);
    }


    private SalesBillReportRecycle.ItemListener listener;
    private DeleteItemListener deleteSalesBillListener;


    public SalesBillReportRecycle(List<SalesBillReportPOJO> itemList,FragmentActivity context){
        this.itemList=itemList;
        this.activity=context;
    }

    @NonNull
    @Override
    public SalesBillReportHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.sales_layout,parent,false);
        return new SalesBillReportHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final SalesBillReportHolder holder, int position) {
        final SalesBillReportPOJO item = itemList.get(position);
        holder.tv_sr_no.setText(item.getSerNo());
        holder.tv_bill_no.setText(item.getBillNo());
        holder.tv_total_amt.setText(item.getTotalAmt());
        holder.tv_cash_credit.setText(item.getCashCredit());
        final String bdate=item.getBdate();

        holder.tv_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(item);
            }
        });
        holder.tv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(activity,"Date="+bdate,Toast.LENGTH_SHORT).show();
                //CustomDialog.RemoveMessageBox(holder.tv_bill_no.getText().toString(), bdate, "Remove", "Do you want to delete the Bill no  " + holder.tv_bill_no.getText().toString() + "?",activity);
                deleteSalesBillListener.onDeleteClick(holder.tv_bill_no.getText().toString(), bdate, "Remove", "Do you want to delete the Bill no  " + holder.tv_bill_no.getText().toString() + "?");
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    static class SalesBillReportHolder extends RecyclerView.ViewHolder{
        TextView tv_sr_no;
        TextView tv_bill_no;
        TextView tv_total_amt;
        TextView tv_cash_credit;
        TextView tv_delete,tv_print;

        SalesBillReportHolder(@NonNull View convertView) {
            super(convertView);
            tv_sr_no = convertView.findViewById(R.id.tv_sr_no);
            tv_bill_no = convertView.findViewById(R.id.tv_bill_no);
            tv_total_amt = convertView.findViewById(R.id.tv_total_amt);
            tv_cash_credit = convertView.findViewById(R.id.tv_cash_credit);
            tv_delete = convertView.findViewById(R.id.tv_delete);
            tv_print = convertView.findViewById(R.id.tv_print);
        }
    }
}
