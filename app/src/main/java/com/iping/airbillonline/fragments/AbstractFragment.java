package com.iping.airbillonline.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.iping.airbillonline.database.DatabaseHelper;
import com.iping.airbillonline.printer.PrintClass;
import com.iping.airbillonline.utils.SessionManager;
import com.iping.airbillonline.utils.ThreadExecutor;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class AbstractFragment extends Fragment implements View.OnClickListener {

    protected DatabaseHelper db;

    protected ThreadExecutor executor;

    protected PrintClass printClass;

    protected SessionManager manager;

    protected final static String ITEMNAME="itemname";
    protected final static String CUSTOMER_NAME="customer_name";

    protected String print_option;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        manager=SessionManager.getInstance(getContext());
        String selectprinter_option = manager.getPrinter();
        printClass=PrintClass.getInstance((Activity) context,selectprinter_option);
        print_option=manager.getPrintOption();
    }

    public AbstractFragment(){
        db = DatabaseHelper.getInstance(getActivity());
        executor=ThreadExecutor.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(getFragmentLayout(),container,false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(getMenuLayout(), menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    public abstract int getMenuLayout();

    public abstract int getFragmentLayout();

    protected long milliseconds(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date mDate = sdf.parse(date);
            long timeInMilliseconds = mDate.getTime();
            return timeInMilliseconds;
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }
    public static int getPixelsFromDPs(Activity activity, int dps){
        Resources r = activity.getResources();
        int  px = (int) (TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, dps, r.getDisplayMetrics()));
        return px;
    }

    @Override
    public void onClick(View v){
    }
    }
