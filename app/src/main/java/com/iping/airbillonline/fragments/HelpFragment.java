package com.iping.airbillonline.fragments;

import com.iping.airbillonline.R;

public class HelpFragment extends AbstractFragment {
    @Override
    public int getMenuLayout() {
        return R.menu.default_menu;
    }

    @Override
    public int getFragmentLayout() {
        return R.layout.help_fragment;
    }
}
