package com.iping.airbillonline.fragments;

import android.database.Cursor;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.iping.airbillonline.R;
import com.iping.airbillonline.database.DatabaseHelper;
import com.iping.airbillonline.utils.HeadFootSetting;
import com.iping.airbillonline.utils.Message;

import java.util.Objects;

public class HeaderFooterSettingFragment extends AbstractFragment {

    private Button bt_ok;
    private EditText et_h1;
    private EditText et_h2;
    private EditText et_h3;
    private EditText et_h4;
    private EditText et_h5;
    private EditText et_f1;
    private EditText et_f2;
    private EditText et_f3;
    private EditText et_f4;
    private EditText et_f5;
    private TextView tv_head_shop_name,tv_h2,tv_h3,tv_h4,tv_h5;
    private TextView tv_thank_you,tv_f2,tv_f3,tv_f4,tv_f5;

    public HeaderFooterSettingFragment(){

    }

    @Override
    public void onStart() {
        super.onStart();
        View view=getView();
        bt_ok= Objects.requireNonNull(view).findViewById(R.id.bt_ok);
        et_h1= view.findViewById(R.id.et_h1);
        et_h2= view.findViewById(R.id.et_h2);
        et_h3= view.findViewById(R.id.et_h3);
        et_h4= view.findViewById(R.id.et_h4);
        et_h5= view.findViewById(R.id.et_h5);
        et_f1= view.findViewById(R.id.et_f1);
        et_f2= view.findViewById(R.id.et_f2);
        et_f3= view.findViewById(R.id.et_f3);
        et_f4= view.findViewById(R.id.et_f4);
        et_f5= view.findViewById(R.id.et_f5);

        tv_thank_you = view.findViewById(R.id.tv_thank_you);
        tv_f2 = view.findViewById(R.id.tv_f2);
        tv_f3 = view.findViewById(R.id.tv_f3);
        tv_f4 = view.findViewById(R.id.tv_f4);
        tv_f5 = view.findViewById(R.id.tv_f5);
        tv_head_shop_name = view.findViewById(R.id.tv_head_shop_name);
        tv_h2 = view.findViewById(R.id.tv_h2);
        tv_h3 = view.findViewById(R.id.tv_h3);
        tv_h4 = view.findViewById(R.id.tv_h4);
        tv_h5 = view.findViewById(R.id.tv_h5);

        et_h1.setText(tv_head_shop_name.getText().toString());
        et_f1.setText(tv_thank_you.getText().toString());

//        bt_ok.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                tv_head_shop_name.setText(et_h1.getText().toString());
//                tv_thank_you.setText(et_f1.getText().toString());
//                db.AddHeaderFooter(tv_head_shop_name.getText().toString(), "0", et_h2.getText().toString(), "0", et_h3.getText().toString(), "0", et_h4.getText().toString(), "0", et_h5.getText().toString(),"0", tv_thank_you.getText().toString(), "0", et_f2.getText().toString(), "0", et_f3.getText().toString(), "0", et_f4.getText().toString(), "0", et_f5.getText().toString(), "0");
//                Message.message(getContext(), "Header footer added sucessfully");
//            }
//        });


        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                final Cursor c = db.getAllHeaderFooter();
                Log.v("TAGFG", String.valueOf(c.getCount()));
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if (c.getCount() > 0) {
                            while (c.moveToNext()) {
                                tv_head_shop_name.setText(c.getString(1));
                                tv_h2.setText(c.getString(3));
                                if(tv_h2.getText().toString().equals("")){
                                    tv_h2.setVisibility(View.GONE);
                                }else {
                                    tv_h2.setVisibility(View.VISIBLE);
                                }

                                tv_h3.setText(c.getString(5));
                                if(tv_h3.getText().toString().equals("")){
                                    tv_h3.setVisibility(View.GONE);
                                }else {
                                    tv_h3.setVisibility(View.VISIBLE);
                                }

                                tv_h4.setText(c.getString(7));
                                if(tv_h4.getText().toString().equals("")){
                                    tv_h4.setVisibility(View.GONE);
                                }else {
                                    tv_h4.setVisibility(View.VISIBLE);
                                }

                                tv_h5.setText(c.getString(9));
                                if(tv_h5.getText().toString().equals("")){
                                    tv_h5.setVisibility(View.GONE);
                                }else {
                                    tv_h5.setVisibility(View.VISIBLE);
                                }

                                et_h1.setText(c.getString(1));
                                et_h2.setText(c.getString(3));
                                et_h3.setText(c.getString(5));
                                et_h4.setText(c.getString(7));
                                et_h5.setText(c.getString(9));

                                tv_thank_you.setText(c.getString(11));
                                tv_f2.setText(c.getString(13));
                                if(tv_f2.getText().toString().equals("")){
                                    tv_f2.setVisibility(View.GONE);
                                }else {
                                    tv_f2.setVisibility(View.VISIBLE);
                                }

                                tv_f3.setText(c.getString(15));
                                if(tv_f3.getText().toString().equals("")){
                                    tv_f3.setVisibility(View.GONE);
                                }else {
                                    tv_f3.setVisibility(View.VISIBLE);
                                }

                                tv_f4.setText(c.getString(17));
                                if(tv_f4.getText().toString().equals("")){
                                    tv_f4.setVisibility(View.GONE);
                                }else {
                                    tv_f4.setVisibility(View.VISIBLE);
                                }

                                tv_f5.setText(c.getString(19));
                                if(tv_f5.getText().toString().equals("")){
                                    tv_f5.setVisibility(View.GONE);
                                }else {
                                    tv_f5.setVisibility(View.VISIBLE);
                                }

                                et_f1.setText(c.getString(11));
                                et_f2.setText(c.getString(13));
                                et_f3.setText(c.getString(15));
                                et_f4.setText(c.getString(17));
                                et_f5.setText(c.getString(19));
                            }
                        }
                    }
                });
            }
        };
        executor.execute(runnable);

        bt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(et_h1.getText().toString().length()>0 && et_h1.getText().toString().trim().equals("")){
                    et_h1.setError("Blank Spaces are not allowed");
                }else if(et_h2.getText().toString().length()>0 && et_h2.getText().toString().trim().equals("")){
                    et_h2.setError("Blank Spaces are not allowed");
                }else if(et_h3.getText().toString().length()>0 && et_h3.getText().toString().trim().equals("")){
                    et_h3.setError("Blank Spaces are not allowed");
                }else if(et_h4.getText().toString().length()>0 && et_h4.getText().toString().trim().equals("")){
                    et_h4.setError("Blank Spaces are not allowed");
                }else if(et_h5.getText().toString().length()>0 && et_h5.getText().toString().trim().equals("")){
                    et_h5.setError("Blank Spaces are not allowed");
                }else if(et_f1.getText().toString().length()>0 && et_f1.getText().toString().trim().equals("")){
                    et_f1.setError("Blank Spaces are not allowed");
                }else if(et_f2.getText().toString().length()>0 && et_f2.getText().toString().trim().equals("")){
                    et_f2.setError("Blank Spaces are not allowed");
                }else if(et_f3.getText().toString().length()>0 && et_f3.getText().toString().trim().equals("")){
                    et_f3.setError("Blank Spaces are not allowed");
                }else if(et_f4.getText().toString().length()>0 && et_f4.getText().toString().trim().equals("")){
                    et_f4.setError("Blank Spaces are not allowed");
                }else if(et_f5.getText().toString().length()>0 && et_f5.getText().toString().trim().equals("")){
                    et_f5.setError("Blank Spaces are not allowed");
                }else {
                    et_h1.setText(et_h1.getText().toString().trim());
                    et_h2.setText(et_h2.getText().toString().trim());
                    et_h3.setText(et_h3.getText().toString().trim());
                    et_h4.setText(et_h4.getText().toString().trim());
                    et_h5.setText(et_h5.getText().toString().trim());

                    et_f1.setText(et_f1.getText().toString().trim());
                    et_f2.setText(et_f2.getText().toString().trim());
                    et_f3.setText(et_f3.getText().toString().trim());
                    et_f4.setText(et_f4.getText().toString().trim());
                    et_f5.setText(et_f5.getText().toString().trim());
                    executor.execute(new Runnable() {
                        @Override
                        public void run() {
                            db.AddHeaderFooter(et_h1.getText().toString(), "0", et_h2.getText().toString(), "0", et_h3.getText().toString(), "0", et_h4.getText().toString(), "0", et_h5.getText().toString(), "0", et_f1.getText().toString(), "0", et_f2.getText().toString(), "0", et_f3.getText().toString(), "0", et_f4.getText().toString(), "0", et_f5.getText().toString(), "0");
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    tv_head_shop_name.setText(et_h1.getText().toString());
                                    tv_h2.setText(et_h2.getText().toString());
                                    tv_h3.setText(et_h3.getText().toString());
                                    tv_h4.setText(et_h4.getText().toString());
                                    tv_h5.setText(et_h5.getText().toString());

                                    tv_thank_you.setText(et_f1.getText().toString());
                                    tv_f2.setText(et_f2.getText().toString());
                                    tv_f3.setText(et_f3.getText().toString());
                                    tv_f4.setText(et_f4.getText().toString());
                                    tv_f5.setText(et_f5.getText().toString());

                                    if (tv_h2.getText().toString().equals("")) {
                                        tv_h2.setVisibility(View.GONE);
                                    } else {
                                        tv_h2.setVisibility(View.VISIBLE);
                                    }

                                    if (tv_h3.getText().toString().equals("")) {
                                        tv_h3.setVisibility(View.GONE);
                                    } else {
                                        tv_h3.setVisibility(View.VISIBLE);
                                    }

                                    if (tv_h4.getText().toString().equals("")) {
                                        tv_h4.setVisibility(View.GONE);
                                    } else {
                                        tv_h4.setVisibility(View.VISIBLE);
                                    }

                                    if (tv_h5.getText().toString().equals("")) {
                                        tv_h5.setVisibility(View.GONE);
                                    } else {
                                        tv_h5.setVisibility(View.VISIBLE);
                                    }

                                    if (tv_f2.getText().toString().equals("")) {
                                        tv_f2.setVisibility(View.GONE);
                                    } else {
                                        tv_f2.setVisibility(View.VISIBLE);
                                    }

                                    if (tv_f3.getText().toString().equals("")) {
                                        tv_f3.setVisibility(View.GONE);
                                    } else {
                                        tv_f3.setVisibility(View.VISIBLE);
                                    }

                                    if (tv_f4.getText().toString().equals("")) {
                                        tv_f4.setVisibility(View.GONE);
                                    } else {
                                        tv_f4.setVisibility(View.VISIBLE);
                                    }

                                    if (tv_f5.getText().toString().equals("")) {
                                        tv_f5.setVisibility(View.GONE);
                                    } else {
                                        tv_f5.setVisibility(View.VISIBLE);
                                    }

                                    Message.message(getContext(), "Header footer added sucessfully");
                                }
                            });
                        }
                    });
                }
            }
        });


    }

    @Override
    public int getMenuLayout() {
        return R.menu.default_menu;
    }

    @Override
    public int getFragmentLayout() {
        return R.layout.header_footer_setting;
    }
}
