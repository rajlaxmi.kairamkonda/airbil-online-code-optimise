package com.iping.airbillonline.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.iping.airbillonline.R;
import com.iping.airbillonline.holder.CodewiseHolder;
import com.iping.airbillonline.pojo.ClassCodeWiseList;
import com.iping.airbillonline.utils.Message;
import com.iping.airbillonline.utils.ModelSale;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import static android.util.Log.i;

public class CodewiseFragment extends AbstractFragment {

    private Button btn_print;
    private Button btn_clear;
    private Button btn_enter;
    private Button btn_zero;
    private Button btn_one;
    private Button btn_two;
    private Button btn_three;
    private Button btn_four;
    private Button btn_five;
    private Button btn_six;
    private Button btn_seven;
    private Button btn_eight;
    private Button btn_nine;
    private Button btn_dot;

    private TextView tv_bill_no;
    private TextView tv_item_name;
    private TextView tv_qty;
    private TextView tv_total_amt;
    private TextView tv_dis;
    private TextView tv_tax_val;
    private TextView tv_item_wise_rate;
    private TextView tv_entered_code;

    private RecyclerView codewiseRecycle;

    private String str_code="";
    private String code_item_name="";
    private String code_item_rate="";
    private String code_item_qty="";
    private String code_item_price="";
    private String code_item_basic_rate="";
    private String code_item_tax="";
    private String code_item_dis="";
    private String code_item_dis_rate="";

    private String gst_option="";

    private String payment_mode_id="";
    private String order_details="";
    private String payment_deatils="";
    private String empcode="";
    private String emp_code="";
    private String str_set_cust_name="";
    private String str_set_id_cust="";
    private String  str_set_payment="";
    private String str_set_pointofsale="";
    private String Name="";

    private int code_cnt=0;
    private int cnt=1;
    private int name_index=0;
    private int billno=1;
    private int counter = 1;

    private String lid="";
    private String cid="";
    private String empid="";

    private double total_bill_amount=0.0;

    private Cursor ids;

    private ArrayList<ClassCodeWiseList> itemArrayList;
    private List<String> checkArray=new ArrayList<>();
    private List<String> taxlistupdated=new ArrayList<String>();


    private CodeWiseRecycle adapter;

    public CodewiseFragment(){}

    @Override
    public void onStart() {
        super.onStart();
        View view=getView();
        itemArrayList=new ArrayList<>();
        btn_clear= Objects.requireNonNull(view).findViewById(R.id.btn_clear);
        btn_enter= view.findViewById(R.id.btn_enter);
        btn_zero= view.findViewById(R.id.btn_zero);
        btn_one= view.findViewById(R.id.btn_one);
        btn_two= view.findViewById(R.id.btn_two);
        btn_three= view.findViewById(R.id.btn_three);
        btn_four= view.findViewById(R.id.btn_four);
        btn_five= view.findViewById(R.id.btn_five);
        btn_six= view.findViewById(R.id.btn_six);
        btn_seven= view.findViewById(R.id.btn_seven);
        btn_eight= view.findViewById(R.id.btn_eight);
        btn_nine= view.findViewById(R.id.btn_nine);
        btn_dot= view.findViewById(R.id.btn_dot);

        btn_enter.setOnClickListener(this);
        btn_clear.setOnClickListener(this);

        tv_bill_no= view.findViewById(R.id.tv_bill_no);
        tv_item_name= view.findViewById(R.id.tv_item_name);
        tv_qty= view.findViewById(R.id.tv_qty);
        tv_total_amt= view.findViewById(R.id.tv_total_amt);
        tv_dis= view.findViewById(R.id.tv_dis);
        tv_tax_val= view.findViewById(R.id.tv_tax_val);
        tv_item_wise_rate= view.findViewById(R.id.tv_item_wise_rate);
        tv_entered_code= view.findViewById(R.id.tv_entered_code);

        codewiseRecycle=view.findViewById(R.id.codewise_recycle);
        final LinearLayoutManager manager = new LinearLayoutManager(getContext());
        codewiseRecycle.setLayoutManager(manager);
        codewiseRecycle.setHasFixedSize(true);
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getContext(),R.anim.slide_from_right_layout);
        codewiseRecycle.setLayoutAnimation(animation);
        adapter=new CodeWiseRecycle();
        codewiseRecycle.setAdapter(adapter);

        gst_option=this.manager.getGstOption();
        lid=this.manager.getLid();
        cid=this.manager.getCid();
        empid=this.manager.getEmpId();
    }

    public void clr_all_mtd(){
        tv_item_name.setText("");
        tv_qty.setText("");
        tv_entered_code.setText("");
        str_code="";
        tv_total_amt.setText("");
        total_bill_amount=0.0;
        code_cnt=0;
        tv_entered_code.setHint("Enter Item");
        tv_item_wise_rate.setText("");

        ModelSale.arr_item_rate.clear();
        ModelSale.arry_item_name.clear();
        ModelSale.arr_item_qty.clear();
        ModelSale.arr_item_price.clear();
        ModelSale.arr_item_basicrate.clear();
        ModelSale.arr_item_dis_rate.clear();
        ModelSale.arr_item_dis.clear();
        ModelSale.arr_item_tax.clear();
        ModelSale.array_item_amt.clear();

        code_item_name="";
        code_item_rate="";
        code_item_qty="";
        code_item_price="";
        code_item_basic_rate="";
        code_item_tax="";
        code_item_dis="";
        code_item_dis_rate="";

        itemArrayList.clear();
        checkArray.clear();
        cnt=1;
        payment_deatils="";
        order_details="";
        str_set_cust_name="";
        str_set_payment="";
        payment_mode_id="";
        Name="";
        str_set_pointofsale="";
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();

        btn_zero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(str_code.length()<5){
                    str_code = str_code + "0";
                    tv_entered_code.setText(str_code);
                }
            }
        });
        btn_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(str_code.length()<5){
                    str_code = str_code + "1";
                    tv_entered_code.setText(str_code);
                }
            }
        });
        btn_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(str_code.length()<5){
                    str_code = str_code + "2";
                    tv_entered_code.setText(str_code);
                }
            }
        });
        btn_three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(str_code.length()<5){
                    str_code = str_code + "3";
                    tv_entered_code.setText(str_code);
                }
            }
        });
        btn_four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(str_code.length()<5){
                    str_code = str_code + "4";
                    tv_entered_code.setText(str_code);
                }
            }
        });
        btn_five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(str_code.length()<5){
                    str_code=str_code+"5";
                    tv_entered_code.setText(str_code);
                }
            }
        });
        btn_six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(str_code.length()<5) {
                    str_code = str_code + "6";
                    tv_entered_code.setText(str_code);
                }
            }
        });
        btn_seven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(str_code.length()<5) {
                    str_code = str_code + "7";
                    tv_entered_code.setText(str_code);
                }
            }
        });
        btn_eight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(str_code.length()<5) {
                    str_code = str_code + "8";
                    tv_entered_code.setText(str_code);
                }
            }
        });
        btn_nine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(str_code.length()<5) {
                    str_code = str_code + "9";
                    tv_entered_code.setText(str_code);
                }
            }
        });
        btn_dot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(str_code.length()<5) {
                    str_code = str_code + ".";
                    tv_entered_code.setText(str_code);
                }
            }
        });


    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        if (btn_clear.getId()==v.getId()){
            if(code_cnt==1){
                if(ModelSale.arry_item_name.contains(tv_item_name.getText().toString())){
                    int idx=ModelSale.arry_item_name.indexOf(tv_item_name.getText().toString());
                    if(ModelSale.arr_item_qty.get(idx)!=0.0){
                        ModelSale.arr_item_rate.set(idx,""+(Double.parseDouble(ModelSale.arr_item_rate.get(idx))-1.0));
                    }else {

                        ModelSale.arry_item_name.remove(idx);
                        ModelSale.arr_item_rate.remove(idx);
                        ModelSale.arr_item_qty.remove(idx);
                        ModelSale.arr_item_price.remove(idx);
                        ModelSale.arr_item_dis_rate.remove(idx);
                        ModelSale.arr_item_dis.remove(idx);
                        ModelSale.arr_item_basicrate.remove(idx);
                        ModelSale.arr_item_tax.remove(idx);

                        itemArrayList.remove(idx);
                    }
                }
                code_item_name="";
                code_item_rate="";
                code_item_qty="";
                code_item_price="";
                code_item_basic_rate="";
                code_item_tax="";
                code_item_dis="";
                code_item_dis_rate="";
                tv_item_wise_rate.setText("");
                code_cnt=0;
                tv_item_name.setText("");
                tv_qty.setText("");
                tv_entered_code.setText("");
                str_code="";
                tv_entered_code.setHint("Enter Item");
            }
            else{
                tv_item_wise_rate.setText("");
                code_cnt=0;
                tv_item_name.setText("");
                tv_qty.setText("");
                tv_entered_code.setText("");
                str_code="";
                code_item_name="";
                code_item_rate="";
                code_item_qty="";
                code_item_price="";
                code_item_basic_rate="";
                code_item_tax="";
                code_item_dis="";
                code_item_dis_rate="";
            }
        }

        if (btn_enter.getId()==v.getId()) {
            String[] arrayString;
            String sp_str_item = "", sp_str_dis = "", sp_str_taxval = "", str_item_name = "", str_item_rate = "", str_item_basicrate = "", str_item_disrate;
            double item_rate = 0.0, final_itemwise_rate = 0.0, item_qty = 0.0;
            if (str_code.equals("")) {
                if(code_cnt==0) {
                    Toast.makeText(getContext(), "Please Enter Item code", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(getContext(), "Please Enter Quantity", Toast.LENGTH_SHORT).show();
                }
            } else if (code_cnt == 0) {
                int deciflag = 0;
                try {
                    str_item_name = db.getItemFromcode(Integer.parseInt(str_code));
                    Log.v("TAGG",str_item_name);
                    deciflag = 0;
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    deciflag = 1;
                }
                if (deciflag == 0) {
                    if (str_item_name.equals("")) {
//                        Toast.makeText(getApplicationContext(),"Item not Exist!!",Toast.LENGTH_SHORT).show();
                        tv_item_name.setText("Item not Exist");
                        tv_dis.setTextColor(Color.BLACK);
                        code_cnt = 0;
                        str_code = "";
                        tv_entered_code.setText(str_code);
                    } else {
                        if (gst_option.equals("GST Disable")) {
                            arrayString = str_item_name.split(",");
                            sp_str_item = arrayString[0];
                            sp_str_dis = arrayString[1];
                            sp_str_taxval = arrayString[2];
                            str_item_rate = arrayString[3];
                            str_item_basicrate = arrayString[4];
                            str_item_disrate = arrayString[5];
                            tv_item_wise_rate.setText(str_item_rate);
                            tv_item_name.setText(sp_str_item);
                            tv_dis.setText(sp_str_dis);
                            tv_tax_val.setText(sp_str_taxval);

                            code_item_name=sp_str_item;
                            code_item_rate=str_item_rate;
                            //ModelSale.arr_item_qty.add(0.0);
                            //ModelSale.arr_item_price.add(0.0);
                            code_item_basic_rate=str_item_basicrate;
                            code_item_tax=sp_str_taxval;
                            code_item_dis=sp_str_dis;
                            code_item_dis_rate=str_item_disrate;

                            code_cnt = 1;
                            str_code = "";
                            tv_entered_code.setText(str_code);
                            tv_entered_code.setHint("Enter Quantity");
                        } else if (gst_option.equals("GST Enable")) {
                            arrayString = str_item_name.split(",");
                            sp_str_item = arrayString[0];
                            sp_str_dis = arrayString[1];
                            sp_str_taxval = arrayString[2];
                            str_item_rate = arrayString[3];
                            str_item_basicrate = arrayString[4];
                            str_item_disrate = arrayString[5];
                            tv_item_wise_rate.setText(str_item_rate);
                            tv_item_name.setText(sp_str_item);
                            tv_dis.setText(sp_str_dis);
                            tv_tax_val.setText(sp_str_taxval);

                            code_item_name=sp_str_item;
                            code_item_rate=str_item_rate;
                            //ModelSale.arr_item_qty.add(0.0);
                            //ModelSale.arr_item_price.add(0.0);
                            code_item_basic_rate=str_item_basicrate;
                            code_item_tax=sp_str_taxval;
                            code_item_dis=sp_str_dis;
                            code_item_dis_rate=str_item_disrate;

                            code_cnt = 1;
                            str_code = "";
                            tv_entered_code.setText(str_code);
                            tv_entered_code.setHint("Enter Quantity");
                        }
                    }
                } else {
                    Toast.makeText(getContext(), "Please enter Correct Item code", Toast.LENGTH_SHORT).show();
                    code_cnt = 0;
                    str_code = "";
                    tv_entered_code.setText(str_code);
                }
            } else if (code_cnt == 1) {

                if(str_code.equals(".")){
                    Toast.makeText(getContext(),"Enter Quantity",Toast.LENGTH_SHORT).show();
                    str_code="";
                    tv_entered_code.setText(str_code);
                    tv_entered_code.setHint("Enter Quantity");
                    return;
                }

                try {
                    tv_qty.setText(str_code);
                    item_rate = Double.parseDouble(tv_item_wise_rate.getText().toString());
                    final_itemwise_rate = item_rate * Double.parseDouble(str_code);
                }catch (Exception e){
                    Toast.makeText(getContext(),"Enter Quantity",Toast.LENGTH_SHORT).show();
                    str_code="";
                    tv_entered_code.setText(str_code);
                    tv_entered_code.setHint("Enter Quantity");
                    return;
                }

                total_bill_amount = total_bill_amount + final_itemwise_rate;

                if (ModelSale.arry_item_name.contains(tv_item_name.getText().toString())) {
                    int index = 0;
                    index = ModelSale.arry_item_name.indexOf(tv_item_name.getText().toString());
                    ModelSale.arr_item_qty.set(index, (ModelSale.arr_item_qty.get(index) + Double.parseDouble(str_code)));
                    ModelSale.arr_item_price.set(index, Double.parseDouble(String.format("%.02f",ModelSale.arr_item_price.get(index))) + Double.parseDouble(String.format("%.02f",final_itemwise_rate)));
                    itemArrayList.set(index, new ClassCodeWiseList("" + (index + 1), "" + tv_item_name.getText().toString(), "" + String.format("%.02f", Double.parseDouble(tv_item_wise_rate.getText().toString())), "" + ModelSale.arr_item_qty.get(index), "" + String.format("%.02f", ModelSale.arr_item_price.get(index)), ""));
                }
                else {
                    ModelSale.arry_item_name.add(code_item_name);
                    ModelSale.arr_item_basicrate.add(code_item_basic_rate);
                    ModelSale.arr_item_rate.add(code_item_rate);
                    ModelSale.arr_item_qty.add(Double.parseDouble(str_code));
                    ModelSale.arr_item_price.add(Double.parseDouble(String.format("%.02f",final_itemwise_rate)));
                    ModelSale.arr_item_tax.add(code_item_tax);
                    ModelSale.arr_item_dis.add(code_item_dis);
                    ModelSale.arr_item_dis_rate.add(code_item_dis_rate);
                    itemArrayList.add(new ClassCodeWiseList("" + cnt, "" + tv_item_name.getText().toString(), ""+String.format("%.02f", Double.parseDouble(tv_item_wise_rate.getText().toString())), ""+Double.parseDouble(str_code), ""+String.format("%.02f", final_itemwise_rate), ""));
                    cnt++;
                }

                code_cnt = 2;
                adapter.notifyDataSetChanged();

                tv_total_amt.setText("" + String.format("%.02f", total_bill_amount));
                code_cnt = 0;
                str_code = "";
                tv_qty.setText("");
                tv_item_name.setText("");
                str_code = "";
                tv_entered_code.setText(str_code);
                tv_entered_code.setHint("Enter Item");
                tv_qty.setHint("");

                tv_item_wise_rate.setText("");
                code_item_name="";
                code_item_rate="";
                code_item_qty="";
                code_item_price="";
                code_item_basic_rate="";
                code_item_tax="";
                code_item_dis="";
                code_item_dis_rate="";
            }
        }
    }

    private void CustomMessageBox1(){
        Button btn_save_cashcredit;
        final Dialog dialog1=new Dialog(Objects.requireNonNull(getContext()));
        dialog1.setContentView(R.layout.layout_payment_details);
        dialog1.setTitle("Payment Details");

        List<String> users = new ArrayList<>();
        final Spinner stransaction= dialog1.findViewById(R.id.TransactionStatus);

        users.add(0, "Transaction status");
        users.add(1, "Success");
        users.add(2, "Fail");
        users.add(3, "Processing");
        //adapter for spinner
        // Toast.makeText(getApplicationContext(),"Values are="+users,Toast.LENGTH_LONG).show();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, users);
        //attach adapter to spinner
        stransaction.setAdapter(adapter);
        btn_save_cashcredit= dialog1.findViewById(R.id.btn_save_cashcredit1);
        final AutoCompleteTextView tid= dialog1.findViewById(R.id.TransactionID);
        final AutoCompleteTextView tdetails= dialog1.findViewById(R.id.TransactionDetails);
        AutoCompleteTextView todetails= dialog1.findViewById(R.id.OrderDetails);

        btn_save_cashcredit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    JSONArray array = new JSONArray();
                    JSONObject obj = new JSONObject();
                    obj.put("t_id", tid.getText().toString());
                    obj.put("t_details", tdetails.getText().toString());
                    obj.put("t_status", stransaction.getSelectedItem());
                    array.put(obj);
                    payment_deatils=""+array;

                    JSONArray array1 = new JSONArray();
                    JSONObject obj1 = new JSONObject();
                    obj1.put("o_details", tdetails.getText().toString());
                    array1.put(obj1);
                    order_details=""+array1;
                    dialog1.dismiss();

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        dialog1.setCancelable(true);
        dialog1.show();
    }

    private void CustomMessageBox(){
        Button btn_save_cashcredit,payment_details;
        final Dialog dialog=new Dialog(Objects.requireNonNull(getContext()));
        dialog.setContentView(R.layout.layout_cash_credit);
        dialog.setTitle("Payment Details");
//        rg_bill_screen=(RadioGroup)dialog.findViewById(R.id.rg_cash_credit);

        List<String> users = new ArrayList<>();
        List<String> users1 = new ArrayList<>();
        final List<String> users2 = new ArrayList<>();


        final Spinner sp_payment= dialog.findViewById(R.id.sp_cash_credit);
        final Spinner sppoint_of_contact= dialog.findViewById(R.id.sp_modeofpayment);
        btn_save_cashcredit= dialog.findViewById(R.id.btn_save_cashcredit);
        final AutoCompleteTextView at_customer_name= dialog.findViewById(R.id.at_customer_name);

        users.add(0, "Select Payment Type");
        Cursor cpm=db.payment_details();
        if(cpm==null || cpm.getCount() == 0) {

        }else {
            while (cpm.moveToNext()) {
                users.add(""+cpm.getString(2));
            }
        }

        users1.add(0, "Select Point Of Contact");
        users2.add("0");

        Cursor cpc=db.point_contact_details();
        if(cpc==null || cpc.getCount() == 0) {

        }else {
            while (cpc.moveToNext()) {
                users2.add(""+cpc.getString(1));
                users1.add(""+cpc.getString(2));
            }
        }

        ArrayAdapter adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, users);
        sp_payment.setAdapter(adapter);

        if (!str_set_payment.equals("")) {
            int spinnerPosition = adapter.getPosition(str_set_payment);
            sp_payment.setSelection(spinnerPosition);
        }

        ArrayAdapter adapter1 = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, users1);
        sppoint_of_contact.setAdapter(adapter1);

        if (!str_set_pointofsale.equals("")) {
            int spinnerPosition = adapter1.getPosition(str_set_pointofsale);
            sppoint_of_contact.setSelection(spinnerPosition);
        }


        payment_details= dialog.findViewById(R.id.btn_payment_details);
        payment_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomMessageBox1();
            }
        });

        final List<String> NameList= new ArrayList<>();
        List<String> IdList= new ArrayList<>();

        Cursor c =db.getCustomerDetails();
        if(c.getCount() == 0) {
            Message.message(getContext(),"Nothing found");
        }else {
            while (c.moveToNext()) {
                IdList.add(c.getString(0));
                NameList.add(c.getString(2));
            }
        }

        ArrayAdapter<String> adapter2=new ArrayAdapter<String>(getContext(),android.R.layout.select_dialog_item,NameList);
        at_customer_name.setThreshold(1);
        at_customer_name.setAdapter(adapter2);

        if(!Name.equals("")){
            at_customer_name.setText(Name);
        }

        at_customer_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Name=at_customer_name.getText().toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
                Name=at_customer_name.getText().toString();
            }
        });

        btn_save_cashcredit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Name=Name.trim();

                if(!Name.matches("[a-zA-Z ]*") || Name.equals("")){
                    at_customer_name.setError("Enter Characters only");
                }else {

                    if(sp_payment.getSelectedItem().equals("Select Payment Type")){
                        str_set_payment="Cash";
                    }else{
                        str_set_payment = "" + sp_payment.getSelectedItem();
                    }

                    if(sppoint_of_contact.getSelectedItem().equals("Select Point Of Contact")){
                        str_set_pointofsale="Counter";
                        payment_mode_id = users2.get(0);
                    }else{
                        str_set_pointofsale="" +sppoint_of_contact.getSelectedItem();
                        int point_of_saleid= (int) sppoint_of_contact.getSelectedItemId();
                        payment_mode_id = users2.get(point_of_saleid);
                    }

                    str_set_cust_name=Name;

                    if(NameList.contains(Name)){
                        name_index=NameList.indexOf(Name);
                        Toast.makeText(getContext(),""+name_index,Toast.LENGTH_SHORT).show();
                        str_set_cust_name=Name;
                        dialog.dismiss();
                    }else {
                        //Toast.makeText(getApplicationContext(),"New Customer added!!!",Toast.LENGTH_SHORT).show();
                        str_set_cust_name=Name;
                        //db.AddCutomer("",str_set_cust_name,"","","","");
                        dialog.dismiss();
                    }
                }
            }
        });

        dialog.setCancelable(true);
        dialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_print: {
                onPrintBill();
                return true;
            }
            case R.id.action_customer: {
                if(!(tv_total_amt.getText().toString().equals("0.0")
                        || tv_total_amt.getText().toString().equals("0.0")
                        || tv_total_amt.getText().toString().equals("")
                        || tv_total_amt.getText().toString().equals("0"))) {
                    CustomMessageBox();
                }
                return true;
            }
            case R.id.action_clear_all: {
                clr_all_mtd();
                return true;
            }
            default:
            return super.onOptionsItemSelected(item);
        }
    }

    private void addDBGSTDisable() {
        taxlistupdated.clear();
        Double totalPrice = 0.0;//Double.parseDouble(tv_total_amt.getText().toString());
        if (checkArray != null && checkArray.size() != 0) {
            for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                // totalPrice = Double.parseDouble(tv_total_amt.getText().toString());
                for (int j = 0; j < checkArray.size(); j++) {
                    if (checkArray.get(j).equals(ModelSale.arry_item_name.get(i))) {
                        Double value = ModelSale.arr_item_price.get(i);
                        totalPrice = totalPrice + value;
                    }
                }
            }
            totalPrice = Double.parseDouble(tv_total_amt.getText().toString()) - totalPrice;
            Log.v("TAgGGGG", String.valueOf(totalPrice));

            for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                if (checkArray.contains(ModelSale.arry_item_name.get(i))) {
                    db.Addbill(billno, str_set_cust_name, str_set_payment, ModelSale.arry_item_name.get(i),
                            ModelSale.arr_item_qty.get(i), Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                            0.0, totalPrice, cid, lid, empid,
                            "" + billno, payment_mode_id, payment_deatils, order_details, gst_option, emp_code,
                            ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                            Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                    taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                    db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                } else {
                    db.Addbill(billno, str_set_cust_name, str_set_payment, ModelSale.arry_item_name.get(i),
                            ModelSale.arr_item_qty.get(i), Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                            Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                            totalPrice, cid, lid, empid,
                            "" + billno, payment_mode_id, payment_deatils, order_details, gst_option, emp_code,
                            ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                            Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                    taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                    db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                }
            }
        }
        else {
            Log.v("TAGGGG MODEL", String.valueOf(ModelSale.arry_item_name.size()));
            for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                db.Addbill(billno, str_set_cust_name, str_set_payment, ModelSale.arry_item_name.get(i),
                        ModelSale.arr_item_qty.get(i), Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                        Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                        Double.parseDouble(tv_total_amt.getText().toString()), cid, lid, empid,
                        "" + billno, payment_mode_id, payment_deatils, order_details, gst_option, emp_code,
                        ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                        Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
            }
        }
    }

    private void addDBGSTEnable() {
        taxlistupdated.clear();
        Double totalPrice = 0.0;//Double.parseDouble(tv_total_amt.getText().toString());
        if (checkArray != null && checkArray.size() != 0) {
            for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                // totalPrice = Double.parseDouble(tv_total_amt.getText().toString());
                for (int j = 0; j < checkArray.size(); j++) {
                    if (checkArray.get(j).equals(ModelSale.arry_item_name.get(i))) {
                        Double value = ModelSale.arr_item_price.get(i);
                        totalPrice = totalPrice + value;
                    }
                }
            }
            totalPrice = Double.parseDouble(tv_total_amt.getText().toString()) - totalPrice;
            Log.v("TAgGGGG", String.valueOf(totalPrice));

            for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                if (checkArray.contains(ModelSale.arry_item_name.get(i))) {
                    db.Addbill(billno, str_set_cust_name, str_set_payment, ModelSale.arry_item_name.get(i),
                            ModelSale.arr_item_qty.get(i), Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                            0.0, totalPrice, cid, lid, empid,
                            "" + billno, payment_mode_id, payment_deatils, order_details, gst_option, emp_code,
                            ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                            Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                    taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                    db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                } else {
                    db.Addbill(billno, str_set_cust_name, str_set_payment, ModelSale.arry_item_name.get(i),
                            ModelSale.arr_item_qty.get(i), Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                            Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                            totalPrice, cid, lid, empid,
                            "" + billno, payment_mode_id, payment_deatils, order_details, gst_option, emp_code,
                            ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                            Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                    taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                    db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                }
            }
        }
        else {
            for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                db.Addbill(billno, str_set_cust_name, str_set_payment, ModelSale.arry_item_name.get(i),
                        ModelSale.arr_item_qty.get(i), Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                        Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                        Double.parseDouble(tv_total_amt.getText().toString()), cid, lid, empid,
                        "" + billno, payment_mode_id, payment_deatils, order_details, gst_option, emp_code,
                        ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                        Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
            }
        }
    }

    private void onPrintBill() {
        String billwithprint=manager.getBillWithPrint();
        String multipleprint=manager.getMultipleOption();
        if (print_option.equals("3 inch")) {
            if (print_option.equals("3 inch") && gst_option.equals("GST Disable")) {
                if (billwithprint.equals("No") && multipleprint.equals("No")) {
                    addDBGSTDisable();
                    clr_all_mtd();
                    ids = db.getAllbillID();
                    autoinnc();
                    TextView t = new TextView(getContext());
                    Message.message(getContext(), "Bill saved");
                } else if (billwithprint.equals("Yes") && multipleprint.equals("No")) {
                    addDBGSTDisable();
                    if (ModelSale.arry_item_name.size() == 0)
                        Message.message(getContext(), "Please select items");
                    else
                        gst_data_blueprints_three_inch_gst_disable();
                } else if (billwithprint.equals("Yes") && multipleprint.equals("Yes")) {
                    if (counter <= 1) {
                        addDBGSTDisable();
                    }if (counter == 1) {
                        counter++;
                        if (ModelSale.arry_item_name.size() == 0)
                            Message.message(getContext(), "Please select items");
                        else
                            gst_data_blueprints_three_inch_gst_disable();
                        }
                        if (counter >= 2) {
                            if (ModelSale.arry_item_name.size() == 0)
                                Message.message(getContext(), "Please select items");
                             else
                                RemoveMessageBox("gst_data_bluePrints_three_inch_gst_disable", "Reprinting", "Do you want bill again ?", billno + "?");
                        }
                    }
            }
            if (print_option.equals("3 inch") && gst_option.equals("GST Enable")) {
                if (billwithprint.equals("No") && multipleprint.equals("No")) {
                        addDBGSTEnable();
                        TextView t = new TextView(getContext());
                        clr_all_mtd();
                        ids = db.getAllbillID();
                        autoinnc();
                        Message.message(getContext(), "Bill saved");
                    } else if (billwithprint.equals("Yes") && multipleprint.equals("No")) {
                        addDBGSTEnable();
                        if (ModelSale.arry_item_name.size() == 0)
                            Message.message(getContext(), "Please select items");
                         else
                            gst_data_blueprints_three_inch_gst_enable();

                    } else if (billwithprint.equals("Yes") && multipleprint.equals("Yes")) {
                        if (counter <= 1) {
                          addDBGSTEnable();
                        }
                        if (counter == 1) {
                            counter++;
                            if (ModelSale.arry_item_name.size() == 0)
                                Message.message(getContext(), "Please select items");
                             else
                                gst_data_blueprints_three_inch_gst_enable();
                        }
                        if (counter >= 2) {
                            if (ModelSale.arry_item_name.size() == 0)
                                Message.message(getContext(), "Please select items");
                             else
                                RemoveMessageBox("gst_data_bluePrints_three_inch_gst_enable", "Reprinting", "Do you want bill again ?", billno + "?");
                        }
                    }
                }
            }
        if (print_option.equals("2 inch")) {
                if (print_option.equals("2 inch") && gst_option.equals("GST Disable")) {
                    if (billwithprint.equals("No")) {
                        addDBGSTDisable();
                        clr_all_mtd();
                        ids = db.getAllbillID();
                        autoinnc();
                        Message.message(getContext(), "3");
                    }
                    else if (billwithprint.equals("Yes") && multipleprint.equals("No")) {
                        Log.v("TAGGGGGG","NO");
                        addDBGSTDisable();
                        if (ModelSale.arry_item_name.size() == 0)
                            Message.message(getContext(), "Please select items");
                         else
                             gst_data_blueprints_two_inch_gst_disable();

                    } else if (billwithprint.equals("Yes") && multipleprint.equals("Yes")) {
                        if (counter <= 1) {
                            Log.v("TAGGGGGG","YES");
                            addDBGSTDisable();
                        }
                        if (counter == 1) {
                            counter++;
                            if (ModelSale.arry_item_name.size() == 0)
                                Message.message(getContext(), "Please select items");
                             else
                                gst_data_blueprints_two_inch_gst_disable();
                        }
                        if (counter >= 2) {
                            if (ModelSale.arry_item_name.size() == 0)
                                Message.message(getContext(), "Please select items");
                            else
                                RemoveMessageBox("gst_data_blueprints_two_inch_gst_disable", "Reprinting", "Do you want bill again ?", billno + "?");
                        }
                    }
                }
                if (print_option.equals("2 inch") && gst_option.equals("GST Enable")) {
                    if (billwithprint.equals("No")) {
                        addDBGSTEnable();
                        clr_all_mtd();
                        ids = db.getAllbillID();
                        autoinnc();
                        Message.message(getContext(), "Bill saved");
                    } else if (billwithprint.equals("Yes") && multipleprint.equals("No")) {
                       addDBGSTDisable();
                        if (ModelSale.arry_item_name.size() == 0)
                            Message.message(getContext(), "Please select items");
                         else
                            gst_data_blueprints_two_inch_gst_enable();
                    } else if (billwithprint.equals("Yes") && multipleprint.equals("Yes")){
                        Log.v("TAG", "ENALBE");
                        if (counter <= 1) {
                           addDBGSTEnable();
                        }

                        if (counter == 1) {
                            counter++;
                            if (ModelSale.arry_item_name.size() == 0)
                                Message.message(getContext(), "Please select items");
                             else
                                gst_data_blueprints_two_inch_gst_enable();
                        }
                        if (counter >= 2) {
                            if (ModelSale.arry_item_name.size() == 0) {
                                Message.message(getContext(), "Please select items");
                            } else {
                                RemoveMessageBox("gst_data_blueprints_two_inch_gst_enable", "Reprinting", "Do you want bill again ?", billno + "?");
                            }
                        }
                    }
                }
            }
        }

    private void RemoveMessageBox(String gst_data_blueprints_two_inch_gst_enable, String reprinting, String s, String s1) {

    }

    private void threeInchGSTDisable(){ {
            Cursor res2 = db.getAllbillWithID(billno);
            Cursor res1 = db.getAllHeaderFooter();
            String h1 = null, h1size = "10", h2 = null, h2size = "9", h3 = null, h3size = "8", h4 = null, h4size = "7", h5 = null, h5size = "6", f1 = null, f1size = "10", f2 = null, f2size = "10", f3 = null, f3size = "10", f4 = null, f4size = "10", f5 = null, f5size = "10";
            while (res1.moveToNext()) {
                h1 = res1.getString(1);
                h1size = res1.getString(2);
                h2 = res1.getString(3);
                h2size = res1.getString(4);
                h3 = res1.getString(5);
                h3size = res1.getString(6);
                h4 = res1.getString(7);
                h4size = res1.getString(8);
                h5 = res1.getString(9);
                h5size = res1.getString(10);

                f1 = res1.getString(11);
                f1size = res1.getString(12);
                f2 = res1.getString(13);
                f2size = res1.getString(14);
                f3 = res1.getString(15);
                f3size = res1.getString(16);
                f4 = res1.getString(17);
                f4size = res1.getString(18);
                f5 = res1.getString(19);
                f5size = res1.getString(20);
            }

            try {
                Date today = new Date();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                String dateToStr = format.format(today);
                String data = null;

                data = "                 " + "TechMart Cafe" + "             \n";
                String d = "________________________________\n";

                if (h1 == null || h1.length() == 0 || h1 == "") {
                    h1 = null;
                } else {
                    if (h1.length() < h2.length()) {
                        data = "          " + h1 + "\n";
                    } else {
                        data = "         " + h1 + "\n";
                    }
                    try {
                        printClass.IntentPrint(data);
                    }catch (Exception ex) {
                        Message.message(getContext(), "Printer not connected please connect from main settings");
                    }
                }

                if (h2 == null || h2.length() == 0 || h2 == "") {
                    h2 = null;
                } else {
                    if (h2.length() > 16) {
                        data = "     " + h2 + "\n";
                    } else {
                        data = "           " + h2 + "\n";
                    }
                    printClass.IntentPrint(data);
                }
                if (h3 == null || h3.length() == 0 || h3 == "") {
                    h3 = null;
                } else {
                    if (h3.length() > 16) {
                        data = "     " + h3 + "\n";
                    } else {
                        data = "           " + h3 + "\n";
                    }
                    printClass.IntentPrint(data);
                }
                if (h4 == null || h4.length() == 0 || h4 == "") {
                    h4 = null;
                } else {
                    if (h4.length() > 16) {
                        data = "     " + h4 + "\n";
                    } else {
                        data = "           " + h4 + "\n";
                    }
                    printClass.IntentPrint(data);
                }
                if (h5 == null || h5.length() == 0 || h5 == "") {
                    h5 = null;
                } else {
                    if (h4.length() > 16) {
                        data = "     " + h5 + "\n";
                    } else {
                        data = "           " + h5 + "\n";
                    }
                    printClass.IntentPrint(data);
                }

                data = "\nCash Memo    BILL No:" + billno + "\n";
                try {
                    printClass.IntentPrint(data);
                }catch (Exception e){
                    Message.message(getContext(),"Printer not connected please connect from main settings");


                }
                data = "Date:-" + dateToStr + "\n";
                printClass.IntentPrint(data);
                if (str_set_cust_name == "" || str_set_cust_name == null) {
                    str_set_cust_name = "";
                    data = "\nNo|   Item Name   | Qty | Rate |Dis|Tax| Amt  \n";


                } else {
                    data = "Customer Name:- " + str_set_cust_name + "\n";
                    printClass.IntentPrint(data);
                    data = "\nNo|   Item Name   | Qty | Rate |Dis|Tax| Amt  \n";

                }
                printClass.IntentPrint(data);
                String totalamtrate = "";
                int a = 1;Double totaldiscount=0.0,discal=0.0,discaltotal=0.0;
                StringBuffer buffer = new StringBuffer();

                String totalamtrategst = "";
                int a1 = 1, j = 0;
                Double itemtax = 0.0;
                int[] positions = new int[ModelSale.arry_item_name.size()];
                StringBuffer buffer1 = new StringBuffer();
                Double totalwithgst = 0.0, finalgst = 0.0,subtotal=0.0;
                Double oldtax = 0.0;
                int l = 0;
                res2 = db.getAllbillWithID(billno);
                while (res2.moveToNext()) {

                    String itemname = res2.getString(3).toString();
                    String largname = "";
                    String qty = res2.getString(4).toString();
                    String rate = res2.getString(5).toString();
                    String discount = res2.getString(7).toString();
                    String tax = res2.getString(8).toString();
                    String Basicrate=res2.getString(9);
                    String amount = res2.getString(6).toString();
                    if (amount.length() > 6) {
                        amount = amount.substring(0, 6);
                    } else {
                        int length = amount.length();
                        int l1 = 0;
                        if (length < 6) {
                            l1 = 6 - length;
                            amount = String.format(amount + "%" + (l1) + "s", "");
                        }
                    }
                    if (qty.length() >=5) {
                        qty = qty.substring(0, 5);

                    } else {
                        int length = qty.length();
                        int l1 = 0;
                        if (length < 5) {
                            l1 = 5 - length;
                            qty = String.format(qty + "%" + (l1) + "s", "");
                        }
                    }
                    if (Basicrate.length() > 5) {
                        Basicrate = Basicrate.substring(0, 5);

                    } else {
                        int length = Basicrate.length();
                        int l1 = 0;
                        if (length < 5) {
                            l1 = 5 - length;
                            Basicrate = String.format(Basicrate + "%" + (l1) + "s", "");
                        }
                    }
                    if (discount.length() > 2) {
                        discount = discount.substring(0, 2);

                    } else {
                        int length = discount.length();
                        int l1 = 0;
                        if (length <2) {
                            l1 = 2 - length;
                            discount = String.format(discount + "%" + (l1) + "s", "");
                        }
                    }
                    if (tax.length() > 2) {
                        tax = tax.substring(0, 2);

                    } else {
                        int length = tax.length();
                        int l1 = 0;
                        if (length < 2) {
                            l1 = 2 - length;
                            tax = String.format(tax + "%" + (l1) + "s", "");
                        }
                    }

                    if (itemname.length() > 15) {
                        if(a>=10) {
                            buffer.append(a + "   " + itemname.substring(0, 15) + "" + qty+"  "+ Basicrate+"   "+ discount +"  "+ tax +" "+ amount + "\n"); //res2.getString(6)+"\n");
                        }
                        else {
                            buffer.append(a + "    " + itemname.substring(0, 15) + "" + qty +"  "+ Basicrate +"   "+ discount +"  "+ tax +" "+ amount + "\n"); //res2.getString(6)+"\n");
                        }
                    }
                    else {
                        int length = itemname.length();
                        int l1 = 0;
                        if (length < 15) {
                            l1 = 15 - length;
                            String itemname1 = itemname.concat(l1 + " ");
                            itemname = String.format(itemname + "%" + (l1) + "s", "");
                        }
                        if(a>=10) {
                            buffer.append(a + "   " + itemname + "" + qty + "  " +Basicrate + "   " + discount + "  " + tax + " " + amount + "\n");//res2.getString(6)+"\n");
                        }
                        else {
                            buffer.append(a+"    "+itemname + "" + qty + "  " + Basicrate +"   "+discount+"  "+tax+ " " + amount + "\n");//res2.getString(6)+"\n");
                        }
                    }
                    a++;
                    discaltotal=discaltotal+Double.parseDouble(Basicrate);
                    discal=discaltotal*(Double.parseDouble(discount)/100);
                    totaldiscount=totaldiscount+discal;
                    discal=0.0;
                    discaltotal=0.0;
                    subtotal=subtotal+Double.parseDouble(Basicrate);
                }
                DecimalFormat decimalFormat = new DecimalFormat("#####.00");
                Double grandgst = 0.0;
                int count1 = 0;
                Double cgst = 0.0, sgst = 0.0;
                List<String> result = new ArrayList<>();

                Double sameitemgst = 0.0, sameitemtotalgst = 0.0;
                List<String> taxlist1 = new ArrayList<>();
                final List<String> itemnamelist = new ArrayList<>();
                List<Double> qtylist = new ArrayList<>();
                List<String> ratelist = new ArrayList<>();
                List<Double> amtlist = new ArrayList<>();
                List<String> indexlist1 = new ArrayList<>();
                List<String> disclist = new ArrayList<>();
                List<String> disclistrate = new ArrayList<>();
                for (int k = 0; k < ModelSale.arry_item_name.size(); k++) {
                    itemtax = Double.parseDouble(taxlistupdated.get(k));
                    Cursor c = db.getdistinct(String.valueOf(itemtax));
                    while (c.moveToNext()) {
                        taxlist1.add(c.getString(0));
                        String stringName = c.getString(0);
                        if(checkArray==null || checkArray.size()==0){
                            Log.v("TAG","NO COMPLEMENT");
                            if (ModelSale.arry_item_name.contains(c.getString(0))) {
                                if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                    indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    itemnamelist.add(c.getString(0));
                                    qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                }
                                if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                } else {
                                    itemnamelist.add(c.getString(0));
                                    indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                }
                            }
                        }
                        else{
                            if (ModelSale.arry_item_name.get(k).equals(stringName)) {
                                Log.v("TAGG",ModelSale.arry_item_name.get(k));
                                if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                    //   for(int q=0;q<checkArray.size();q++) {
                                    if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                        Log.v("TAG","1 IN");
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        //ratelist.add(k,"0.0");
                                        amtlist.add(k, 0.0);
                                        disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));

                                        if (counter<=2) {
                                            double current = Double.parseDouble(tv_total_amt.getText().toString());
                                            double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                            total_val = current - total_val;
                                            if(total_val<=0.0){
                                                tv_total_amt.setText("0.0");
                                            }
                                            else {
                                                tv_total_amt.setText("" + String.format("%.02f", total_val));
                                            }
                                            Log.v("TAG", String.valueOf(total_val));
                                        }
                                        Log.v("TAG","1 OUT");
                                    } else {
                                        Log.v("TAG","2 IN");
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        Log.v("TAG","2 OUT");
                                    }
                                }
                                else if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                    if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                        Log.v("TAG","3 IN");
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));

                                        amtlist.add(k, 0.0);
                                        disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        if (counter<=2) {
                                            double current = Double.parseDouble(tv_total_amt.getText().toString());
                                            double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                            total_val = current - total_val;
                                            if(total_val<=0.0){
                                                tv_total_amt.setText("0.0");
                                            }
                                            else {
                                                tv_total_amt.setText("" + String.format("%.02f", total_val));
                                            }
                                            Log.v("TAG", String.valueOf(total_val));
                                        }
                                        Log.v("TAG","3 OUT");
                                    } else {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        Log.v("TAG","4 OUT");
                                    }
                                }
                                else {
                                    if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                        Log.v("TAG","4 IN");
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, 0.0);
                                        disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        if (counter<=2) {
                                            double current = Double.parseDouble(tv_total_amt.getText().toString());
                                            double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                            total_val = current - total_val;
                                            if(total_val<=0.0){
                                                tv_total_amt.setText("0.0");
                                            }
                                            else {
                                                tv_total_amt.setText("" + String.format("%.02f", total_val));
                                            }
                                            Log.v("TAG", String.valueOf(total_val));
                                        }
                                        Log.v("TAG","4 OUT");
                                    } else {
                                        Log.v("TAGG", "CHECKIN4");
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        Log.v("TAGG", "CHECKOUT4");
                                    }
                                }
                            }
                        }
                    }
                }
                taxlist1.clear();
                for (int k = 0; k < itemnamelist.size(); k++) {
                    DecimalFormat formats = new DecimalFormat("#####.00");
                    taxlist1.clear();
                    int count = itemnamelist.size();
                    String itemname = itemnamelist.get(k);
                    String largname = "";
                    Double qty = qtylist.get(k);
//                        String rate = ratelist.get(k);
                    String rate=disclistrate.get(k);
                    String amount = String.valueOf(qty*Double.parseDouble(rate));
//                        String amount = String.valueOf(amtlist.get(k));
                    itemtax = Double.parseDouble(indexlist1.get(k));
                    Cursor c = db.getdistinct(String.valueOf(itemtax));
                    while (c.moveToNext()) {
                        taxlist1.add(c.getString(0));

                    }
                    cgst = (itemtax / 2);
                    sgst = cgst;
                    if (amount.length() > 4) {
                        amount = amount.substring(0, 4);

                    } else {
                        int length = amount.length();
                        int l1 = 0;
                        if (length < 4) {
                            l1 = 4 - length;
                            amount = String.format(amount + "%" + (l1) + "s", "");
                        }
                    }

                    List<String> list = new ArrayList<>();
                    for (String aObject : taxlist1) {
                        if (itemnamelist.contains(aObject)) {
                            list.add(aObject);
                        }
                    }
                    result = list;

                    i("dataname", String.valueOf(result));
                    int resultcount = result.size();

                    if (itemname.length() > 15) {


                        if (rate.length() == 3) {

                            count1++;
                            if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                int p = result.indexOf(itemnamelist.get(k));
                                int f = taxlist1.size();
                                int h = itemnamelist.size();
                                if (p == (result.size() - 1)) {
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer1.append("    " + itemtax + "%       " + String.format("%.02f",sameitemtotalgst / 2) + "+" + String.format("%.02f",sameitemtotalgst / 2) + "       " + String.format("%.02f",sameitemtotalgst) + "\n");
                                } else if (result.size() == 1) {
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer1.append("    " + itemtax + "%       " + String.format("%.02f",sameitemtotalgst / 2) + "+" + String.format("%.02f",sameitemtotalgst / 2) + "       " + String.format("%.02f",sameitemtotalgst) + "\n");
                                } else {
                                    if (g == f || count == count1) {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("    " + itemtax + "%       " + String.format("%.02f",sameitemtotalgst / 2) + "+" + String.format("%.02f",sameitemtotalgst / 2) + "       " + String.format("%.02f",sameitemtotalgst) + "\n");
                                    } else {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    }
                                }
                            } else {
                                totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                grandgst = grandgst + totalwithgst;
                                buffer1.append("    " + itemtax + "%       " + String.format("%.02f",(totalwithgst / 2)) + "+" + String.format("%.02f",(totalwithgst / 2)) + "       " + String.format("%.02f",totalwithgst) + "\n");
                            }
                        } else {
                            if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                int p = result.indexOf(itemnamelist.get(k));
                                int f = taxlist1.size();
                                int h = itemnamelist.size();
                                if (p == (result.size() - 1)) {
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer1.append("    " + itemtax + "%       " + String.format("%.02f",sameitemtotalgst / 2) + "+" + String.format("%.02f",sameitemtotalgst / 2) + "       " + String.format("%.02f",sameitemtotalgst) + "\n");
                                } else if (result.size() == 1) {
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer1.append("    " + itemtax + "%       " + String.format("%.02f",sameitemtotalgst / 2) + "+" + String.format("%.02f",sameitemtotalgst / 2) + "       " + String.format("%.02f",sameitemtotalgst) + "\n");
                                } else {
                                    if (g == f || count == count1) {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("    " + itemtax + "%       " + String.format("%.02f",sameitemtotalgst / 2) + "+" + String.format("%.02f",sameitemtotalgst / 2) + "       " + String.format("%.02f",sameitemtotalgst) + "\n");
                                    } else {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    }
                                }
                            } else {
                                totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                buffer1.append("    " + itemtax + "%       " + String.format("%.02f",(totalwithgst / 2)) + "+" + String.format("%.02f",(totalwithgst / 2)) + "       " + String.format("%.02f",totalwithgst) + "\n");
                                grandgst = grandgst + totalwithgst;
                            }
                        }
                    } else {
                        int length = itemname.length();
                        int l1 = 0;
                        if (length < 15) {
                            l1 = 15 - length;
                            String itemname1 = itemname.concat(l1 + " ");
                            itemname = String.format(itemname + "%" + (l1) + "s", "");
                        }
                        if (rate.length() == 3) {
                            count1++;
                            if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                int p = result.indexOf(itemnamelist.get(k));
                                int f = taxlist1.size();
                                int h = itemnamelist.size();
                                if (p == (result.size() - 1)) {
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer1.append("    " + itemtax + "%       " + String.format("%.02f",sameitemtotalgst / 2) + "+" + String.format("%.02f",sameitemtotalgst / 2) + "       " + String.format("%.02f",sameitemtotalgst) + "\n");
                                } else if (result.size() == 1) {
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer1.append("    " + itemtax + "%       " + String.format("%.02f",sameitemtotalgst / 2) + "+" + String.format("%.02f",sameitemtotalgst / 2) + "       " + String.format("%.02f",sameitemtotalgst) + "\n");
                                } else {
                                    if (g == f || count == count1) {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("    " + itemtax + "%       " + String.format("%.02f",sameitemtotalgst / 2) + "+" + String.format("%.02f",sameitemtotalgst / 2) + "       " + String.format("%.02f",sameitemtotalgst) + "\n");
                                    } else {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    }
                                }
                            } else {
                                totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                buffer1.append("    " + itemtax + "%       " + String.format("%.02f",(totalwithgst / 2)) + "+" + String.format("%.02f",(totalwithgst / 2)) + "       " + String.format("%.02f",totalwithgst) + "\n");
                                grandgst = grandgst + totalwithgst;
                            }
                        } else {
                            count1++;
                            if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                int p = result.indexOf(itemnamelist.get(k));
                                int f = taxlist1.size();
                                int h = itemnamelist.size();
                                if (p == (result.size() - 1)) {
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer1.append("    " + itemtax + "%       " + String.format("%.02f",sameitemtotalgst / 2) + "+" + String.format("%.02f",sameitemtotalgst / 2) + "       " + String.format("%.02f",sameitemtotalgst) + "\n");
                                } else if (result.size() == 1) {
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer1.append("    " + itemtax + "%       " + String.format("%.02f",sameitemtotalgst / 2) + "+" + String.format("%.02f",sameitemtotalgst / 2) + "       " + String.format("%.02f",sameitemtotalgst) + "\n");
                                } else {
                                    if (g == f || count == count1) {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("    " + itemtax + "%       " + String.format("%.02f",sameitemtotalgst / 2) + "+" + String.format("%.02f",sameitemtotalgst / 2) + "       " + String.format("%.02f",sameitemtotalgst) + "\n");
                                    } else {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    }
                                }
                            } else {
                                totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                buffer1.append("    " + itemtax + "%       " + String.format("%.02f",(totalwithgst / 2)) + "+" + String.format("%.02f",(totalwithgst / 2)) + "       " + String.format("%.02f",totalwithgst) + "\n");
                                grandgst = grandgst + totalwithgst;
                            }
                        }
                    }
                    j++;
                    finalgst = totalwithgst + finalgst;
                    totalwithgst = 0.0;
                    cgst = 0.0;
                    sgst = 0.0;
                }
                Double totalAmount = Double.parseDouble(tv_total_amt.getText().toString()) + finalgst;


                printClass.IntentPrint(String.valueOf(buffer));

                double ddata = Double.parseDouble(tv_total_amt.getText().toString());
                DecimalFormat f = new DecimalFormat("#####.00");
                d = "________________________________________________\n";
                data = "                       Sub TOTAL(Rs): " + String.format("%.02f",subtotal) + "\n";
                printClass.IntentPrint(d);
                printClass.IntentPrint(data);
                if(totaldiscount==0.0){}else {
                    data = "                      -Dis(Rs)      : " + String.format("%.02f",totaldiscount) + "\n";
                    d = "                     __________________________";
                    printClass.IntentPrint(data);
                    printClass.IntentPrint(d);
                    data = "                                      " + String.format("%.02f",(subtotal - totaldiscount));
                    printClass.IntentPrint(data);
                }if(finalgst==0.0){}else{
                    data = "\n                      +GST(Rs)      : " + String.format("%.02f",finalgst) + "\n";
                    printClass.IntentPrint(data);
                    printClass.IntentPrint(d);
                }
                data = "                       Net Total(Rs): " + String.format("%.02f",ddata) + "\n";

                printClass.IntentPrint(data);
                if (f1 == null || f1.length() == 0 || f1 == "") {
                    f1 = null;
                } else {
                    data = "     " + f1 + "\n";
                    printClass.IntentPrint(data);
                }
                if (f2 == null || f2.length() == 0 || f2 == "") {
                    f2 = null;
                } else {
                    data = "     " + f2 + "\n";
                    printClass.IntentPrint(data);
                }
                if (f3 == null || f3.length() == 0 || f3 == "") {
                    f3 = null;
                } else {

                    data = "     " + f3 + "\n";
                    printClass.IntentPrint(data);
                }
                if (f4 == null || f4.length() == 0 || f4 == "") {
                    f4 = null;
                } else {
                    data = "     " + f4 + "\n";
                    printClass.IntentPrint(data);
                }
                if (f5 == null || f5.length() == 0 || f5 == "") {
                    f5 = null;
                } else {
                    data = "     " + f5 + "\n";
                    printClass.IntentPrint(data);
                }
                printClass.IntentPrint("\n\n\n\n");
                clr_all_mtd();
                ids = db.getAllbillID();
                autoinnc();
                Message.message(getContext(), "Bill saved");

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }


    private void gst_data_blueprints_three_inch_gst_disable() {
        String billwithprint=manager.getBillWithPrint();
        String multipleprint=manager.getMultipleOption();
        if(billwithprint.equals("Yes")&&multipleprint.equals("No")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                threeInchGSTDisable();
             }else{
                Toast.makeText(getContext(),"Please Use Android Version Greater Than 6",Toast.LENGTH_SHORT).show();
            }
        }
        if(billwithprint.equals("Yes")&&multipleprint.equals("Yes")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                threeInchGSTDisable();
            }else{
                Toast.makeText(getContext(),"Please Use Android Version Greater Than 6",Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void threeInchGSTEnable(){
        {
            if (print_option.equals("3 inch") && gst_option.equals("GST Enable")) {
                Cursor res1 = db.getAllHeaderFooter();
                String h1 = null, h1size = "10", h2 = null, h2size = "9", h3 = null, h3size = "8", h4 = null, h4size = "7", h5 = null, h5size = "6", f1 = null, f1size = "10", f2 = null, f2size = "10", f3 = null, f3size = "10", f4 = null, f4size = "10", f5 = null, f5size = "10";
                while (res1.moveToNext()) {
                    h1 = res1.getString(1);
                    h1size = res1.getString(2);
                    h2 = res1.getString(3);
                    h2size = res1.getString(4);
                    h3 = res1.getString(5);
                    h3size = res1.getString(6);
                    h4 = res1.getString(7);
                    h4size = res1.getString(8);
                    h5 = res1.getString(9);
                    h5size = res1.getString(10);

                    f1 = res1.getString(11);
                    f1size = res1.getString(12);
                    f2 = res1.getString(13);
                    f2size = res1.getString(14);
                    f3 = res1.getString(15);
                    f3size = res1.getString(16);
                    f4 = res1.getString(17);
                    f4size = res1.getString(18);
                    f5 = res1.getString(19);
                    f5size = res1.getString(20);
                }

                Date today = new Date();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                String dateToStr = format.format(today);
                String data = null;

                data = "                 " + "TechMart Cafe" + "             \n";
                String d = "________________________________\n";
                try {
                    if (h1 == null || h1.length() == 0 || h1 == "") {
                        h1 = null;
                    } else {
                        if (h1.length() < h2.length()) {
                            data = "                " + h1 + "\n";
                        } else {
                            data = "               " + h1 + "\n";
                        }
                        try {
                            printClass.IntentPrint(data);
                        }catch (Exception ex) {
                            Message.message(getContext(),"Printer not connected please connect from main settings");
                        }
                    }

                    if (h2 == null || h2.length() == 0 || h2 == "") {
                        h2 = null;
                    } else {
                        if (h2.length() > 16) {
                            data = "           " + h2 + "\n";
                        } else {
                            data = "                 " + h2 + "\n";
                        }
                        printClass.IntentPrint(data);
                    }
                    if (h3 == null || h3.length() == 0 || h3 == "") {
                        h3 = null;
                    } else {
                        if (h3.length() > 16) {
                            data = "           " + h3 + "\n";
                        } else {
                            data = "                 " + h3 + "\n";
                        }
                        printClass.IntentPrint(data);
                    }
                    if (h4 == null || h4.length() == 0 || h4 == "") {
                        h4 = null;
                    } else {
                        if (h4.length() > 16) {
                            data = "          " + h4 + "\n";
                        } else {
                            data = "                 " + h4 + "\n";
                        }
                        printClass.IntentPrint(data);
                    }
                    if (h5 == null || h5.length() == 0 || h5 == "") {
                        h5 = null;
                    } else {
                        if (h4.length() > 16) {
                            data = "            " + h5 + "\n";
                        } else {
                            data = "                 " + h5 + "\n";
                        }
                        printClass.IntentPrint(data);
                    }

                    data = "\n                 BILL No:" + billno + "\n";
                    try {
                        printClass.IntentPrint(data);
                    }catch (Exception ex) {
                        Message.message(getContext(),"Printer not connected please connect from main settings");
                    }
                    data = "Date:-" + dateToStr + "\n";
                    printClass.IntentPrint(data);
                    if (str_set_cust_name == "" || str_set_cust_name == null) {
                        str_set_cust_name = "";
                        data = "\nNo|   Item Name   | Qty | Rate |Dis|Tax| Amt  \n";
                    } else {
                        data = "Customer Name:- " + str_set_cust_name + "\n";
                        printClass.IntentPrint(data);
                        data = "\nNo|   Item Name   | Qty | Rate |Dis|Tax| Amt  \n";

                    }
                    printClass.IntentPrint(data);
                    String totalamtrate = "";
                    int a = 1;Double totaldiscount=0.0,discal=0.0,discaltotal=0.0;
                    StringBuffer buffer = new StringBuffer();
                    String totalamtrategst = "";
                    int a1 = 1, j = 0;
                    Double itemtax = 0.0;
                    int[] positions = new int[ModelSale.arry_item_name.size()];
                    StringBuffer buffer1 = new StringBuffer();
                    Double totalwithgst = 0.0, finalgst = 0.0,subtotal=0.0;
                    Double oldtax = 0.0;
                    int l = 0;
                    Cursor res2 = db.getAllbillWithID(billno);
                    while (res2.moveToNext()) {
                        String itemname = res2.getString(3).toString();
                        String largname = "";
                        String qty = res2.getString(4).toString();
                        String rate = res2.getString(5).toString();
                        String discount = res2.getString(7).toString();
                        String tax = res2.getString(8).toString();
                        String Basicrate=res2.getString(9);

                        String amount = res2.getString(6).toString();

                        if (amount.length() > 6) {
                            amount = amount.substring(0, 6);

                        } else {
                            int length = amount.length();
                            int l1 = 0;
                            if (length < 6) {
                                l1 = 6 - length;
                                amount = String.format(amount + "%" + (l1) + "s", "");
                            }
                        }
                        if (qty.length() >=5) {
                            qty = qty.substring(0, 5);
                        } else {
                            int length = qty.length();
                            int l1 = 0;
                            if (length < 5) {
                                l1 = 5 - length;
                                qty = String.format(qty + "%" + (l1) + "s", "");
                            }
                        }
                        if (Basicrate.length() > 5) {
                            Basicrate = Basicrate.substring(0, 5);
                        } else {
                            int length = Basicrate.length();
                            int l1 = 0;
                            if (length < 5) {
                                l1 = 5 - length;
                                Basicrate = String.format(Basicrate + "%" + (l1) + "s", "");
                            }
                        }
                        if (discount.length() > 2) {
                            discount = discount.substring(0, 2);
                        } else {
                            int length = discount.length();
                            int l1 = 0;
                            if (length <2) {
                                l1 = 2 - length;
                                discount = String.format(discount + "%" + (l1) + "s", "");
                            }
                        }
                        if (tax.length() > 2) {
                            tax = tax.substring(0, 2);
                        } else {
                            int length = tax.length();
                            int l1 = 0;
                            if (length < 2) {
                                l1 = 2 - length;
                                tax = String.format(tax + "%" + (l1) + "s", "");
                            }
                        }
                        if (itemname.length() > 15) {
                            if(a>=10) {
                                buffer.append(a + "   " + itemname.substring(0, 15) + "" + qty+"  "+ Basicrate+"   "+ discount +"  "+ tax +" "+ amount + "\n"); //res2.getString(6)+"\n");
                            }
                            else {
                                buffer.append(a + "    " + itemname.substring(0, 15) + "" + qty +"  "+ Basicrate +"   "+ discount +"  "+ tax +" "+ amount + "\n"); //res2.getString(6)+"\n");
                            }
                        }
                        else {
                            int length = itemname.length();
                            int l1 = 0;
                            if (length < 15) {
                                l1 = 15 - length;
                                String itemname1 = itemname.concat(l1 + " ");
                                itemname = String.format(itemname + "%" + (l1) + "s", "");
                            }
                            if(a>=10) {
                                buffer.append(a + "   " + itemname + "" + qty + "  " +Basicrate + "   " + discount + "  " + tax + " " + amount + "\n");//res2.getString(6)+"\n");
                            } else {
                                buffer.append(a+"    "+itemname + "" + qty + "  " + Basicrate +"   "+discount+"  "+tax+ " " + amount + "\n");//res2.getString(6)+"\n");
                            }
                        }
                        a++;

                        discaltotal=discaltotal+Double.parseDouble(Basicrate);
                        discal=discaltotal*(Double.parseDouble(discount)/100);
                        totaldiscount=totaldiscount+discal;
                        discal=0.0;
                        discaltotal=0.0;
                        subtotal=subtotal+Double.parseDouble(Basicrate);
                    }
                    DecimalFormat decimalFormat = new DecimalFormat("#####.00");
                    Double grandgst = 0.0;
                    int count1 = 0;
                    Double cgst = 0.0, sgst = 0.0;
                    List<String> result = new ArrayList<>();

                    Double sameitemgst = 0.0, sameitemtotalgst = 0.0;
                    List<String> taxlist1 = new ArrayList<>();
                    final List<String> itemnamelist = new ArrayList<>();
                    List<Double> qtylist = new ArrayList<>();
                    List<String> ratelist = new ArrayList<>();
                    List<Double> amtlist = new ArrayList<>();
                    List<String> indexlist1 = new ArrayList<>();
                    List<String> disclist = new ArrayList<>();
                    List<String> disclistrate = new ArrayList<>();
                    for (int k = 0; k < ModelSale.arry_item_name.size(); k++) {
                        itemtax = Double.parseDouble(taxlistupdated.get(k));
                        Cursor c = db.getdistinct(String.valueOf(itemtax));
                        while (c.moveToNext()) {
                            taxlist1.add(c.getString(0));
                            String stringName = c.getString(0);
                            if(checkArray==null || checkArray.size()==0){

                                Log.v("TAG","NO COMPLEMENT");

                                if (ModelSale.arry_item_name.contains(c.getString(0))) {
                                    if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    }
                                    if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                    } else {
                                        itemnamelist.add(c.getString(0));
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    }
                                }
                            }
                            else{
                                if (ModelSale.arry_item_name.get(k).equals(stringName)) {
                                    Log.v("TAGG",ModelSale.arry_item_name.get(k));
                                    if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                        //   for(int q=0;q<checkArray.size();q++) {
                                        if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                            Log.v("TAG","1 IN");
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            //ratelist.add(k,"0.0");
                                            amtlist.add(k, 0.0);
                                            disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));

                                            if (counter<=2) {
                                                double current = Double.parseDouble(tv_total_amt.getText().toString());
                                                double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                                total_val = current - total_val;
                                                if(total_val<=0.0){
                                                    tv_total_amt.setText("0.0");
                                                }
                                                else {
                                                    tv_total_amt.setText("" + String.format("%.02f", total_val));
                                                }
                                                Log.v("TAG", String.valueOf(total_val));
                                            }
                                        } else {
                                            Log.v("TAG","2 IN");
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            Log.v("TAG","2 OUT");
                                        }
                                    }
                                    else if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                        if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                            Log.v("TAG","3 IN");
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));

                                            amtlist.add(k, 0.0);
                                            disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));

                                            if (counter<=2) {
                                                double current = Double.parseDouble(tv_total_amt.getText().toString());
                                                double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                                total_val = current - total_val;
                                                if(total_val<=0.0){
                                                    tv_total_amt.setText("0.0");
                                                }
                                                else {
                                                    tv_total_amt.setText("" + String.format("%.02f", total_val));
                                                }
                                                Log.v("TAG", String.valueOf(total_val));
                                            }
                                        } else {
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            Log.v("TAG","4 OUT");
                                        }
                                    }
                                    else {
                                        if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                            Log.v("TAG","4 IN");
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));

                                            amtlist.add(k, 0.0);
                                            disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));

                                            if (counter<=2) {
                                                double current = Double.parseDouble(tv_total_amt.getText().toString());
                                                double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                                total_val = current - total_val;
                                                if(total_val<=0.0){
                                                    tv_total_amt.setText("0.0");
                                                }
                                                else {
                                                    tv_total_amt.setText("" + String.format("%.02f", total_val));
                                                }
                                                Log.v("TAG", String.valueOf(total_val));
                                            }
                                        } else {
                                            Log.v("TAGG", "CHECKIN4");
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            Log.v("TAGG", "CHECKOUT4");
                                        }
                                    }
                                }
                            }
                        }
                    }

                    taxlist1.clear();
                    for (int k = 0; k < itemnamelist.size(); k++) {
                        DecimalFormat formats = new DecimalFormat("#####.00");
                        taxlist1.clear();
                        int count = itemnamelist.size();
                        String itemname = itemnamelist.get(k);
                        String largname = "";
                        Double qty = qtylist.get(k);
//                        String rate = ratelist.get(k);
                        String rate=disclistrate.get(k);
                        String amount = String.valueOf(qty*Double.parseDouble(rate));
//                        String amount = String.valueOf(amtlist.get(k));
                        itemtax = Double.parseDouble(indexlist1.get(k));
                        Cursor c = db.getdistinct(String.valueOf(itemtax));
                        while (c.moveToNext()) {
                            taxlist1.add(c.getString(0));
                        }
                        cgst = (itemtax / 2);
                        sgst = cgst;
                        if (amount.length() > 4) {
                            amount = amount.substring(0, 4);
                        } else {
                            int length = amount.length();
                            int l1 = 0;
                            if (length < 4) {
                                l1 = 4 - length;
                                amount = String.format(amount + "%" + (l1) + "s", "");
                            }
                        }

                        List<String> list = new ArrayList<>();
                        for (String aObject : taxlist1) {
                            if (itemnamelist.contains(aObject)) {
                                list.add(aObject);
                            }
                        }
                        result = list;

                        i("dataname", String.valueOf(result));
                        int resultcount = result.size();

                        if (itemname.length() > 15) {


                            if (rate.length() == 3) {

                                count1++;
                                if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                    int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                    int p = result.indexOf(itemnamelist.get(k));
                                    int f = taxlist1.size();
                                    int h = itemnamelist.size();
                                    if (p == (result.size() - 1)) {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("    " + itemtax + "%       " + String.format("%.02f",sameitemtotalgst / 2) + "+" + String.format("%.02f",sameitemtotalgst / 2) + "       " + String.format("%.02f",sameitemtotalgst) + "\n");
                                    } else if (result.size() == 1) {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("    " + itemtax + "%       " + String.format("%.02f",sameitemtotalgst / 2) + "+" + String.format("%.02f",sameitemtotalgst / 2) + "       " + String.format("%.02f",sameitemtotalgst) + "\n");
                                    } else {
                                        if (g == f || count == count1) {
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer1.append("    " + itemtax + "%       " + String.format("%.02f",sameitemtotalgst / 2) + "+" + String.format("%.02f",sameitemtotalgst / 2) + "       " + String.format("%.02f",sameitemtotalgst) + "\n");
                                        } else {
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        }
                                    }
                                } else {
                                    totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                    grandgst = grandgst + totalwithgst;
                                    buffer1.append("    " + itemtax + "%       " + String.format("%.02f",(totalwithgst / 2)) + "+" + String.format("%.02f",(totalwithgst / 2)) + "       " + String.format("%.02f",totalwithgst) + "\n");
                                }
                            } else {
                                if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                    int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                    int p = result.indexOf(itemnamelist.get(k));
                                    int f = taxlist1.size();
                                    int h = itemnamelist.size();
                                    if (p == (result.size() - 1)) {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("    " + itemtax + "%       " + String.format("%.02f",sameitemtotalgst / 2) + "+" + String.format("%.02f",sameitemtotalgst / 2) + "       " + String.format("%.02f",sameitemtotalgst) + "\n");
                                    } else if (result.size() == 1) {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("    " + itemtax + "%       " + String.format("%.02f",sameitemtotalgst / 2) + "+" + String.format("%.02f",sameitemtotalgst / 2) + "       " + String.format("%.02f",sameitemtotalgst) + "\n");
                                    } else {
                                        if (g == f || count == count1) {
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer1.append("    " + itemtax + "%       " + String.format("%.02f",sameitemtotalgst / 2) + "+" + String.format("%.02f",sameitemtotalgst / 2) + "       " + String.format("%.02f",sameitemtotalgst) + "\n");
                                        } else {
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        }
                                    }
                                } else {
                                    totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                    buffer1.append("    " + itemtax + "%       " + String.format("%.02f",(totalwithgst / 2)) + "+" + String.format("%.02f",(totalwithgst / 2)) + "       " + String.format("%.02f",totalwithgst) + "\n");
                                    grandgst = grandgst + totalwithgst;
                                }
                            }
                        } else {
                            int length = itemname.length();
                            int l1 = 0;
                            if (length < 15) {
                                l1 = 15 - length;
                                String itemname1 = itemname.concat(l1 + " ");
                                itemname = String.format(itemname + "%" + (l1) + "s", "");
                            }
                            if (rate.length() == 3) {
                                count1++;

                                if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                    int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                    int p = result.indexOf(itemnamelist.get(k));
                                    int f = taxlist1.size();
                                    int h = itemnamelist.size();
                                    if (p == (result.size() - 1)) {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("    " + itemtax + "%       " + String.format("%.02f",sameitemtotalgst / 2) + "+" + String.format("%.02f",sameitemtotalgst / 2) + "       " + String.format("%.02f",sameitemtotalgst) + "\n");
                                    } else if (result.size() == 1) {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("    " + itemtax + "%       " + String.format("%.02f",sameitemtotalgst / 2) + "+" + String.format("%.02f",sameitemtotalgst / 2) + "       " + String.format("%.02f",sameitemtotalgst) + "\n");
                                    } else {
                                        if (g == f || count == count1) {
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer1.append("    " + itemtax + "%       " + String.format("%.02f",sameitemtotalgst / 2) + "+" + String.format("%.02f",sameitemtotalgst / 2) + "       " + String.format("%.02f",sameitemtotalgst) + "\n");
                                        } else {
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        }
                                    }
                                } else {
                                    totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                    buffer1.append("    " + itemtax + "%       " + String.format("%.02f",(totalwithgst / 2)) + "+" + String.format("%.02f",(totalwithgst / 2)) + "       " + String.format("%.02f",totalwithgst) + "\n");
                                    grandgst = grandgst + totalwithgst;
                                }
                            } else {
                                count1++;
                                if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                    int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                    int p = result.indexOf(itemnamelist.get(k));
                                    int f = taxlist1.size();
                                    int h = itemnamelist.size();
                                    if (p == (result.size() - 1)) {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("    " + itemtax + "%       " + String.format("%.02f",sameitemtotalgst / 2) + "+" + String.format("%.02f",sameitemtotalgst / 2) + "       " + String.format("%.02f",sameitemtotalgst) + "\n");
                                    } else if (result.size() == 1) {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("    " + itemtax + "%       " + String.format("%.02f",sameitemtotalgst / 2) + "+" + String.format("%.02f",sameitemtotalgst / 2) + "       " + String.format("%.02f",sameitemtotalgst) + "\n");
                                    } else {
                                        if (g == f || count == count1) {
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer1.append("    " + itemtax + "%       " + String.format("%.02f",sameitemtotalgst / 2) + "+" + String.format("%.02f",sameitemtotalgst / 2) + "       " + String.format("%.02f",sameitemtotalgst) + "\n");
                                        } else {
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        }
                                    }
                                } else {
                                    totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                    buffer1.append("    " + itemtax + "%       " + String.format("%.02f",(totalwithgst / 2)) + "+" + String.format("%.02f",(totalwithgst / 2)) + "       " + String.format("%.02f",totalwithgst) + "\n");
                                    grandgst = grandgst + totalwithgst;
                                }
                            }
                        }
                        j++;
                        finalgst = totalwithgst + finalgst;
                        totalwithgst = 0.0;
                        cgst = 0.0;
                        sgst = 0.0;
                    }



                    Double totalAmount = Double.parseDouble(tv_total_amt.getText().toString()) + finalgst;

                    printClass.IntentPrint(String.valueOf(buffer));

                    double ddata = Double.parseDouble(tv_total_amt.getText().toString());
                    DecimalFormat f = new DecimalFormat("#####.00");
                    d = "________________________________________________\n";
                    data = "                       Sub TOTAL(Rs): " + String.format("%.02f",subtotal) + "\n";
                    printClass.IntentPrint(d);
                    printClass.IntentPrint(data);
                    if(totaldiscount==0.0){}else {
                        data = "                      -Dis(Rs)      : " + String.format("%.02f",totaldiscount) + "\n";
                        d = "                     __________________________";

                        printClass.IntentPrint(data);
                        printClass.IntentPrint(d);

                        data = "                                      " + String.format("%.02f",(subtotal - totaldiscount));
                        printClass.IntentPrint(data);
                    }if(finalgst==0.0){}else{
                        data = "\n                      +GST(Rs)      : " + String.format("%.02f",finalgst) + "\n";
                        printClass.IntentPrint(data);
                        printClass.IntentPrint(d);
                    }
                    data = "                       Net Total(Rs): " + String.format("%.02f",ddata) + "\n";

                    printClass.IntentPrint(data);
                    if (f1 == null || f1.length() == 0 || f1 == "") {
                        f1 = null;
                    } else {
                        data = "           " + f1 + "\n";
                        printClass.IntentPrint(data);
                    }
                    if (f2 == null || f2.length() == 0 || f2 == "") {
                        f2 = null;
                    } else {
                        data = "           " + f2 + "\n";
                        printClass.IntentPrint(data);
                    }
                    if (f3 == null || f3.length() == 0 || f3 == "") {
                        f3 = null;
                    } else {

                        data = "            " + f3 + "\n";
                        printClass.IntentPrint(data);
                    }
                    if (f4 == null || f4.length() == 0 || f4 == "") {
                        f4 = null;
                    } else {
                        data = "            " + f4 + "\n";
                        printClass.IntentPrint(data);
                    }
                    if (f5 == null || f5.length() == 0 || f5 == "") {
                        f5 = null;
                    } else {
                        data = "              " + f5 + "\n";
                        printClass.IntentPrint(data);
                    }

                    printClass.IntentPrint("\n\n\n\n");
                    clr_all_mtd();
                    ids = db.getAllbillID();
                    autoinnc();
                    Message.message(getContext(), "Bill saved");
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
    private void gst_data_blueprints_three_inch_gst_enable() {
        String billwithprint=manager.getBillWithPrint();
        String multipleprint=manager.getMultipleOption();
        if(billwithprint.equals("Yes")&&multipleprint.equals("No")) {
            threeInchGSTEnable();
        }
        if(billwithprint.equals("Yes")&&multipleprint.equals("Yes")) {
            threeInchGSTEnable();
        }
    }

    private void twoInchGSTEnable(){
        {
            Cursor res1 = db.getAllHeaderFooter();
            String h1 = null, h1size = "10", h2 = null, h2size = "9", h3 = null, h3size = "8", h4 = null, h4size = "7", h5 = null, h5size = "6", f1 = null, f1size = "10", f2 = null, f2size = "10", f3 = null, f3size = "10", f4 = null, f4size = "10", f5 = null, f5size = "10";
            while (res1.moveToNext()) {
                h1 = res1.getString(1);
                h1size = res1.getString(2);
                h2 = res1.getString(3);
                h2size = res1.getString(4);
                h3 = res1.getString(5);
                h3size = res1.getString(6);
                h4 = res1.getString(7);
                h4size = res1.getString(8);
                h5 = res1.getString(9);
                h5size = res1.getString(10);

                f1 = res1.getString(11);
                f1size = res1.getString(12);
                f2 = res1.getString(13);
                f2size = res1.getString(14);
                f3 = res1.getString(15);
                f3size = res1.getString(16);
                f4 = res1.getString(17);
                f4size = res1.getString(18);
                f5 = res1.getString(19);
                f5size = res1.getString(20);
            }
            Date today = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
            String dateToStr = format.format(today);
            String data = null;
            data = "                 " + "TechMart Cafe" + "             \n";
            String d = "________________________________\n";
            try {
                if (h1 == null || h1.length() == 0 || h1 == "") {
                    h1 = null;
                } else {
                    if (h1.length() < h2.length()) {
                        data = "          " + h1 + "\n";
                    } else {
                        data = "         " + h1 + "\n";
                    }
                    try {
                        printClass.IntentPrint(data);
                    }catch (Exception e1){
                        Message.message(getContext(),"Printer not connected please connect from main settings");
                    }
                }
                if (h2 == null || h2.length() == 0 || h2 == "") {
                    h2 = null;
                } else {
                    if (h2.length() > 16) {
                        data = "     " + h2 + "\n";
                    } else {
                        data = "           " + h2 + "\n";
                    }
                    printClass.IntentPrint(data);
                }
                if (h3 == null || h3.length() == 0 || h3 == "") {
                    h3 = null;
                } else {
                    if (h3.length() > 16) {
                        data = "     " + h3 + "\n";
                    } else {
                        data = "           " + h3 + "\n";
                    }
                    printClass.IntentPrint(data);
                }
                if (h4 == null || h4.length() == 0 || h4 == "") {
                    h4 = null;
                } else {
                    if (h4.length() > 16) {
                        data = "     " + h4 + "\n";
                    } else {
                        data = "           " + h4 + "\n";
                    }
                    printClass.IntentPrint(data);
                }
                if (h5 == null || h5.length() == 0 || h5 == "") {
                    h5 = null;
                } else {
                    if (h4.length() > 16) {
                        data = "     " + h5 + "\n";
                    } else {
                        data = "           " + h5 + "\n";
                    }
                    printClass.IntentPrint(data);
                }

                data = "\nCash Memo    bill no:" + emp_code + "\n";
                try {
                    printClass.IntentPrint(data);
                }catch (Exception ex) {
                    Message.message(getContext(),"Printer not connected please connect from main settings");
                }
                data = "Date:-" + dateToStr + "\n";
                printClass.IntentPrint(data);
                if (str_set_cust_name == "" || str_set_cust_name == null) {
                    str_set_cust_name = "";
                    data = "\nItem Name         |Qty|Rate|Amt\n";
                } else {
                    data = "Customer Name:- " + str_set_cust_name + "\n";
                    printClass.IntentPrint(data);
                    data = "Item Name        |Qty|Rate|Amt\n";
                }
                printClass.IntentPrint(data);
                String totalamtrate = "";
                int a = 1, j = 0;
                Double itemtax = 0.0;
                int[] positions = new int[ModelSale.arry_item_name.size()];
                StringBuffer buffer = new StringBuffer();
                Double totalwithgst = 0.0, finalgst = 0.0;
                Double oldtax = 0.0;
                int l = 0;

                DecimalFormat decimalFormat = new DecimalFormat("#####.00");
                Double grandgst = 0.0;
                int count1 = 0;
                Double cgst = 0.0, sgst = 0.0;
                List<String> result = new ArrayList<>();

                Double sameitemgst = 0.0, sameitemtotalgst = 0.0;
                List<String> taxlist1 = new ArrayList<>();
                final List<String> itemnamelist = new ArrayList<>();
                List<Double> qtylist = new ArrayList<>();
                List<String> ratelist = new ArrayList<>();
                List<Double> amtlist = new ArrayList<>();
                List<String> indexlist1 = new ArrayList<>();

                for (int k = 0; k < ModelSale.arry_item_name.size(); k++) {
                    itemtax = Double.parseDouble(taxlistupdated.get(k));
                    Cursor c = db.getdistinct(String.valueOf(itemtax));
                    Log.v("TAG","HELLOUPPER");
                    Log.v("TAG", String.valueOf(c.getCount()));
                    int i=0;
                    while (c.moveToNext()) {
                        Log.v("TAG", String.valueOf(i));
                        i=i+1;
                        //Log.v("TAGGG",ModelSale.arry_item_name.get(k));
                        taxlist1.add(c.getString(0));
                        //Log.v("TAGGGGG",c.getString(0));
                        String stringName = c.getString(0);
                        if(checkArray==null || checkArray.size()==0){
                            if (ModelSale.arry_item_name.contains(c.getString(0))) {
                                if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                    indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    itemnamelist.add(c.getString(0));
                                    qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                }
                                if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                } else {
                                    itemnamelist.add(c.getString(0));
                                    indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                }
                            }
                        }
                        else{
                            if (ModelSale.arry_item_name.get(k).equals(stringName)) {
                                Log.v("TAGG",ModelSale.arry_item_name.get(k));
                                if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                    //   for(int q=0;q<checkArray.size();q++) {
                                    if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, 0.0);
                                        if (counter<=2) {
                                            double current = Double.parseDouble(tv_total_amt.getText().toString());
                                            double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                            total_val = current - total_val;
                                            if(total_val<=0.0){
                                                tv_total_amt.setText("0.0");
                                            }
                                            else {
                                                tv_total_amt.setText("" + String.format("%.02f", total_val));
                                            }
                                            Log.v("TAG", String.valueOf(total_val));
                                        }
                                    } else {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    }
                                }
                                else if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                    if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, 0.0);
                                        if (counter<=2) {
                                            double current = Double.parseDouble(tv_total_amt.getText().toString());
                                            double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                            total_val = current - total_val;
                                            if(total_val<=0.0){
                                                tv_total_amt.setText("0.0");
                                            }
                                            else {
                                                tv_total_amt.setText("" + String.format("%.02f", total_val));
                                            }
                                            Log.v("TAG", String.valueOf(total_val));
                                        }
                                    } else {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    }
                                }
                                else {

                                    if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, 0.0);
                                        if (counter<=2) {
                                            double current = Double.parseDouble(tv_total_amt.getText().toString());
                                            double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                            total_val = current - total_val;
                                            if(total_val<=0.0){
                                                tv_total_amt.setText("0.0");
                                            }
                                            else {
                                                tv_total_amt.setText("" + String.format("%.02f", total_val));
                                            }
                                            Log.v("TAG", String.valueOf(total_val));
                                        }
                                    } else {
                                        Log.v("TAGG", "CHECKIN4");
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        Log.v("TAGG", "CHECKOUT4");
                                    }
                                }
                            }
                        }
                    }
                }

                for (int k = 0; k < itemnamelist.size(); k++) {
                    taxlist1.clear();
                    int count = itemnamelist.size();
                    String itemname = itemnamelist.get(k);
                    String largname = "";
                    Double qty = qtylist.get(k);
                    String rate = ratelist.get(k);
                    String amount = String.valueOf(amtlist.get(k));
                    itemtax = Double.parseDouble(indexlist1.get(k));
                    Cursor c = db.getdistinct(String.valueOf(itemtax));
                    while (c.moveToNext()) {
                        taxlist1.add(c.getString(0));

                    }
                    cgst = (itemtax / 2);
                    sgst = cgst;
                    if (amount.length() > 4) {
                        amount = amount.substring(0, 4);

                    } else {
                        int length = amount.length();
                        int l1 = 0;
                        if (length < 4) {
                            l1 = 4 - length;
                            amount = String.format(amount + "%" + (l1) + "s", "");
                        }
                    }

                    List<String> list = new ArrayList<>();
                    for (String aObject : taxlist1) {
                        if (itemnamelist.contains(aObject)) {
                            list.add(aObject);
                        }
                    }
                    result = list;

                    i("dataname", String.valueOf(result));
                    int resultcount = result.size();

                    if (itemname.length() > 15) {

                        if (rate.length() == 3) {

                            count1++;
                            if(itemtax==0.0)
                            {
                                buffer.append(itemname.substring(0, 15) + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.parseDouble(amount)) + "\n"); //res2.getString(6)+"\n");}
                            }
                            else{
                                if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                    int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                    int p = result.indexOf(itemnamelist.get(k));
                                    int f = taxlist1.size();
                                    int h = itemnamelist.size();
                                    if (p == (result.size() - 1)) {
                                        buffer.append(itemname.substring(0, 15) + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.parseDouble(amount)) + "\n"); //res2.getString(6)+"\n");
                                        if (amount.equals("0.0   ")) {
                                        } else {
                                            if (amount.equals("0.0   ")) {
                                            } else {
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
                                                buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                            }
                                        }
                                    } else if (result.size() == 1) {

                                        buffer.append(itemname.substring(0, 15) + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.parseDouble(amount)) + "\n"); //res2.getString(6)+"\n");
                                        if (amount.equals("0.0   ")) {
                                        } else {
                                            if (amount.equals("0.0   ")) {
                                            } else {
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
                                                buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                            }
                                        }
                                    } else {
                                        if (g == f || count == count1) {
                                            buffer.append(itemname.substring(0, 15) + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.parseDouble(amount)) + "\n"); //res2.getString(6)+"\n");
                                            if (amount.equals("0.0   ")) {
                                            } else {
                                                if (amount.equals("0.0   ")) {
                                                } else {
                                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                    finalgst = finalgst + sameitemtotalgst;
                                                    grandgst = grandgst + sameitemtotalgst;
                                                    buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                                }

                                            }
                                        } else {
                                            buffer.append(itemname.substring(0, 15) + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.parseDouble(amount)) + "\n"); //res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);

                                        }
                                    }
                                } else {
                                    buffer.append(itemname.substring(0, 15) + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.parseDouble(amount)) + "\n"); //res2.getString(6)+"\n");
                                    if (amount.equals("0.0   ")) {
                                    } else {
                                        totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                        buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.02f", totalwithgst) + "\n");
                                        grandgst = grandgst + totalwithgst;
                                    }
                                }
                            }
                        }
                        else {
                            if (itemtax == 0.0) {
                                buffer.append(itemname.substring(0, 15) + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.parseDouble(amount)) + "\n"); //res2.getString(6)+"\n");}
                            } else {
                                if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                    int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                    int p = result.indexOf(itemnamelist.get(k));
                                    int f = taxlist1.size();
                                    int h = itemnamelist.size();
                                    if (p == (result.size() - 1)) {
                                        buffer.append(itemname.substring(0, 15) + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.parseDouble(amount)) + "\n"); //res2.getString(6)+"\n");
                                        if (amount.equals("0.0   ")) {
                                        } else {
                                            if (amount.equals("0.0   ")) {
                                            } else {
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
                                                buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                            }
                                        }
                                    } else if (result.size() == 1) {
                                        buffer.append(itemname.substring(0, 15) + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.parseDouble(amount)) + "\n"); //res2.getString(6)+"\n");
                                        if (amount.equals("0.0   ")) {
                                        } else {
                                            if (amount.equals("0.0   ")) {
                                            } else {
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
                                                buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                            }
                                        }
                                    } else {
                                        if (g == f || count == count1) {
                                            buffer.append(itemname.substring(0, 15) + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.parseDouble(amount)) + "\n"); //res2.getString(6)+"\n");
                                            if (amount.equals("0.0   ")) {
                                            } else {
                                                if (amount.equals("0.0   ")) {
                                                } else {
                                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                    finalgst = finalgst + sameitemtotalgst;
                                                    grandgst = grandgst + sameitemtotalgst;
                                                    buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                                }
                                            }
                                        } else {
                                            buffer.append(itemname.substring(0, 15) + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.parseDouble(amount)) + "\n"); //res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        }
                                    }
                                } else {
                                    buffer.append(itemname.substring(0, 15) + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.parseDouble(amount)) + "\n"); //res2.getString(6)+"\n");
                                    if (amount.equals("0.0   ")) {
                                    } else {
                                        totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                        buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.02f", totalwithgst) + "\n");
                                        grandgst = grandgst + totalwithgst;
                                    }
                                }
                            }
                        }
                    } else {
                        int length = itemname.length();
                        int l1 = 0;
                        if (length < 15) {
                            l1 = 15 - length;
                            String itemname1 = itemname.concat(l1 + " ");
                            itemname = String.format(itemname + "%" + (l1) + "s", "");
                        }
                        if (rate.length() == 3) {
                            count1++;
                            if(itemtax==0.0) {
                                buffer.append(itemname.substring(0, 15) + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.parseDouble(amount)) + "\n"); //res2.getString(6)+"\n");}
                            }
                            else {
                                if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                    int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                    int p = result.indexOf(itemnamelist.get(k));
                                    int f = taxlist1.size();
                                    int h = itemnamelist.size();
                                    if (p == (result.size() - 1)) {
                                        buffer.append(itemname + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.parseDouble(amount)) + "\n");
                                        if (amount.equals("0.0   ")) {
                                        } else {
                                            if (amount.equals("0.0   ")) {
                                            } else {
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
                                                buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                            }
                                        }
                                    } else if (result.size() == 1) {
                                        buffer.append(itemname + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.parseDouble(amount)) + "\n");
                                        if (amount.equals("0.0   ")) {
                                        } else {
                                            if (amount.equals("0.0   ")) {
                                            } else {
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
                                                buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                            }
                                        }
                                    } else {
                                        if (g == f || count == count1) {
                                            buffer.append(itemname + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.parseDouble(amount)) + "\n");//res2.getString(6)+"\n");
                                            if (amount.equals("0.0   ")) {
                                            } else {
                                                if (amount.equals("0.0   ")) {
                                                } else {
                                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                    finalgst = finalgst + sameitemtotalgst;
                                                    grandgst = grandgst + sameitemtotalgst;
                                                    buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                                }
                                            }
                                        } else {
                                            buffer.append(itemname + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.parseDouble(amount)) + "\n");//res2.getString(6)+"\n");
                                            if (amount.equals("0.0   ")) {
                                            } else {
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            }
                                        }
                                    }
                                } else {
                                    buffer.append(itemname + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.parseDouble(amount)) + "\n");//res2.getString(6)+"\n");
                                    if (amount.equals("0.0   ")) {
                                    } else {
                                        totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                        buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.02f", totalwithgst) + "\n");
                                        grandgst = grandgst + totalwithgst;
                                    }
                                }
                            }
                        } else {
                            count1++;
                            if(itemtax==0.0)
                            {
                                buffer.append(itemname.substring(0, 15) + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.parseDouble(amount)) + "\n"); //res2.getString(6)+"\n");}
                            }
                            else {
                                if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                    int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                    int p = result.indexOf(itemnamelist.get(k));
                                    int f = taxlist1.size();
                                    int h = itemnamelist.size();
                                    if (p == (result.size() - 1)) {
                                        buffer.append(itemname + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.parseDouble(amount)) + "\n");
                                        if (amount.equals("0.0   ")) {
                                        } else {
                                            if (amount.equals("0.0   ")) {
                                            } else {
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
                                                buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                            }
                                        }
                                    } else if (result.size() == 1) {
                                        buffer.append(itemname + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.parseDouble(amount)) + "\n");//res2.getString(6)+"\n");
                                        if (amount.equals("0.0   ")) {
                                        } else {
                                            if (amount.equals("0.0   ")) {
                                            } else {
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
                                                buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                            }
                                        }
                                    } else {
                                        if (g == f || count == count1) {
                                            buffer.append(itemname + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.parseDouble(amount)) + "\n");//res2.getString(6)+"\n");
                                            if (amount.equals("0.0   ")) {
                                            } else {
                                                if (amount.equals("0.0   ")) {
                                                } else {
                                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                    finalgst = finalgst + sameitemtotalgst;
                                                    grandgst = grandgst + sameitemtotalgst;
                                                    buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                                }
                                            }
                                        } else {
                                            buffer.append(itemname + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.parseDouble(amount)) + "\n");//res2.getString(6)+"\n");
                                            if (amount.equals("0.0   ")) {
                                            } else {
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            }
                                        }
                                    }
                                } else {
                                    buffer.append(itemname + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.parseDouble(amount)) + "\n");//res2.getString(6)+"\n");
                                    if (amount.equals("0.0   ")) {
                                    } else {
                                        totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                        buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.02f", totalwithgst) + "\n");
                                        grandgst = grandgst + totalwithgst;
                                    }
                                }
                            }
                        }
                    }
                    j++;
                    finalgst = totalwithgst + finalgst;
                    totalwithgst = 0.0;
                    cgst = 0.0;
                    sgst = 0.0;
                }
                Double totalAmount = Double.parseDouble(tv_total_amt.getText().toString())- finalgst;
                printClass.IntentPrint(String.valueOf(buffer));

                double ddata = Double.parseDouble(tv_total_amt.getText().toString());
                DecimalFormat f = new DecimalFormat("#####.00");
                if(totalAmount<=0.0){
                    totalAmount=0.0;
                }
                else{
                    totalAmount=totalAmount;
                }
                if(grandgst<=0.0){
                    grandgst=0.0;
                }
                else{
                    grandgst=grandgst;
                }
                Double d12= Double.parseDouble(tv_total_amt.getText().toString());
                if(d12<=0.0){
                    tv_total_amt.setText("0.0");
                }
                else{
                    tv_total_amt.setText(""+d12);
                }

                data = "         NET TOTAL (Rs): " + String.format("%.02f",totalAmount) + "\n";
                printClass.IntentPrint(d);
                printClass.IntentPrint(data);

                data = "         Total GST (Rs): " + String.format("%.02f",grandgst) + "\n";
                printClass.IntentPrint(d);
                printClass.IntentPrint(data);


                data = "     TOTAL With GST(Rs): " + String.format("%.02f",Double.parseDouble(tv_total_amt.getText().toString())) + "\n";
                printClass.IntentPrint(d);
                printClass.IntentPrint(data);
                if (f1 == null || f1.length() == 0 || f1 == "") {
                    f1 = null;
                } else {
                    data = "     " + f1 + "\n";
                    printClass.IntentPrint(data);
                }
                if (f2 == null || f2.length() == 0 || f2 == "") {
                    f2 = null;
                } else {
                    data = "     " + f2 + "\n";
                    printClass.IntentPrint(data);
                }
                if (f3 == null || f3.length() == 0 || f3 == "") {
                    f3 = null;
                } else {
                    data = "     " + f3 + "\n";
                    printClass.IntentPrint(data);
                }
                if (f4 == null || f4.length() == 0 || f4 == "") {
                    f4 = null;
                } else {
                    data = "     " + f4 + "\n";
                    printClass.IntentPrint(data);
                }
                if (f5 == null || f5.length() == 0 || f5 == "") {
                    f5 = null;
                } else {
                    data = "     " + f5 + "\n";
                    printClass.IntentPrint(data);
                }

                printClass.IntentPrint("\n\n\n\n");
                clr_all_mtd();
                ids = db.getAllbillID();
                autoinnc();
                Message.message(getContext(), "Bill saved");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void gst_data_blueprints_two_inch_gst_enable() {
        String billwithprint=manager.getBillWithPrint();
        String multipleprint=manager.getMultipleOption();
        if(billwithprint.equals("Yes")&&multipleprint.equals("No")) {
            if (print_option.equals("2 inch")  && gst_option.equals("GST Enable")){
                twoInchGSTEnable();
            }
        }
        if(billwithprint.equals("Yes")&&multipleprint.equals("Yes")) {
            if (print_option.equals("2 inch")  && gst_option.equals("GST Enable")){
                twoInchGSTEnable();
            }
        }
    }

    private void twoInchGSTDisable(){
            Cursor res2 = db.getAllbillWithID(billno);
            Cursor res1 = db.getAllHeaderFooter();
            Log.v("TAGG", String.valueOf(res2.getCount()));
            Log.v("TAGG", String.valueOf(res1.getCount()));
            String h1 = null, h1size = "10", h2 = null, h2size = "9", h3 = null, h3size = "8", h4 = null, h4size = "7", h5 = null, h5size = "6", f1 = null, f1size = "10", f2 = null, f2size = "10", f3 = null, f3size = "10", f4 = null, f4size = "10", f5 = null, f5size = "10";
            if (res1.getCount()!=0) {
                while (res1.moveToNext()) {
                    h1 = res1.getString(1);
                    h1size = res1.getString(2);
                    h2 = res1.getString(3);
                    h2size = res1.getString(4);
                    h3 = res1.getString(5);
                    h3size = res1.getString(6);
                    h4 = res1.getString(7);
                    h4size = res1.getString(8);
                    h5 = res1.getString(9);
                    h5size = res1.getString(10);

                    f1 = res1.getString(11);
                    f1size = res1.getString(12);
                    f2 = res1.getString(13);
                    f2size = res1.getString(14);
                    f3 = res1.getString(15);
                    f3size = res1.getString(16);
                    f4 = res1.getString(17);
                    f4size = res1.getString(18);
                    f5 = res1.getString(19);
                    f5size = res1.getString(20);
                }
            }
            try {
                Date today = new Date();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                String dateToStr = format.format(today);
                String data = null;
                data = "                 " + "TechMart Cafe" + "             \n";
                String d = "________________________________\n";

                if (h1 == null || h1.length() == 0 || h1 == "") {
                    h1 = null;
                } else {
                    if (h1.length() < h2.length()) {
                        data = "          " + h1 + "\n";
                    } else {
                        data = "         " + h1 + "\n";
                    }
                    try {
                        printClass.IntentPrint(data);
                    }catch (Exception ex) {
                        Message.message(getContext(), "Printer not connected please connect from main settings");
                    }
                }
                if (h2 == null || h2.length() == 0 || h2 == "") {
                    h2 = null;
                } else {
                    if (h2.length() > 16) {
                        data = "     " + h2 + "\n";
                    } else {
                        data = "           " + h2 + "\n";
                    }
                    printClass.IntentPrint(data);
                }
                if (h3 == null || h3.length() == 0 || h3 == "") {
                    h3 = null;
                } else {
                    if (h3.length() > 16) {
                        data = "     " + h3 + "\n";
                    } else {
                        data = "           " + h3 + "\n";
                    }
                    printClass.IntentPrint(data);
                }
                if (h4 == null || h4.length() == 0 || h4 == "") {
                    h4 = null;
                } else {
                    if (h4.length() > 16) {
                        data = "     " + h4 + "\n";
                    } else {
                        data = "           " + h4 + "\n";
                    }
                    printClass.IntentPrint(data);
                }
                if (h5 == null || h5.length() == 0 || h5 == "") {
                    h5 = null;
                } else {
                    if (h4.length() > 16) {
                        data = "     " + h5 + "\n";
                    } else {
                        data = "           " + h5 + "\n";
                    }
                    printClass.IntentPrint(data);
                }
                data = "\nCash Memo    bill no:" + emp_code + "\n";
                try {
                    printClass.IntentPrint(data);
                }catch (Exception ex) {
                    Message.message(getContext(),"Printer not connected please connect from main settings");
                }
                data = "Date:-" + dateToStr + "\n";
                printClass.IntentPrint(data);
                if (str_set_cust_name == "" || str_set_cust_name == null) {
                    str_set_cust_name = "";
                    data = "\nItem Name         |Qty|Rate|Amt\n";
                } else {
                    data = "Customer Name:- " + str_set_cust_name + "\n";
                    printClass.IntentPrint(data);
                    data = "Item Name        |Qty|Rate|Amt\n";
                }
                printClass.IntentPrint(data);
                String totalamtrate = "";
                int a = 1;
                StringBuffer buffer = new StringBuffer();
                while (res2.moveToNext()) {
                    String itemname = res2.getString(3).toString();
                    String largname = "";
                    String qty = res2.getString(4).toString();
                    String rate = res2.getString(5).toString();
                    String amount = res2.getString(6).toString();
                    if (amount.length() > 4) {
                        amount = amount.substring(0, 4);
                    } else {
                        int length = amount.length();
                        int l1 = 0;
                        if (length < 4) {
                            l1 = 4 - length;
                            amount = String.format(amount + "%" + (l1) + "s", "");
                        }
                    }
                    if (itemname.length() > 15) {
                        if (res2.getString(5).length() >= 3) {
                            buffer.append(itemname.substring(0, 15) + " " + res2.getString(4) + " " + res2.getString(5) + " " + amount + "\n"); //res2.getString(6)+"\n");
                        } else {
                            buffer.append(itemname.substring(0, 15) + " " + res2.getString(4) + "  " + res2.getString(5) + "   " + amount + "\n"); //res2.getString(6)+"\n");
                        }
                    } else {
                        int length = itemname.length();
                        int l1 = 0;
                        if (length < 15) {
                            l1 = 15 - length;
                            String itemname1 = itemname.concat(l1 + " ");
                            itemname = String.format(itemname + "%" + (l1) + "s", "");
                        }
                        if (res2.getString(5).length() >= 3) {
                            buffer.append(itemname + " " + res2.getString(4) + "  " + res2.getString(5) + " " + amount + "\n");//res2.getString(6)+"\n");
                        } else {
                            buffer.append(itemname + " " + res2.getString(4) + "  " + res2.getString(5) + "   " + amount + "\n");//res2.getString(6)+"\n");
                        }
                    }
                }
                String totalamtrategst = "";
                int a1 = 1, j = 0;
                Double itemtax = 0.0;
                int[] positions = new int[ModelSale.arry_item_name.size()];
                StringBuffer buffer1 = new StringBuffer();
                Double totalwithgst = 0.0, finalgst = 0.0;
                Double oldtax = 0.0;
                int l = 0;

                DecimalFormat decimalFormat = new DecimalFormat("#####.00");
                Double grandgst = 0.0;
                int count1 = 0;
                Double cgst = 0.0, sgst = 0.0;
                List<String> result = new ArrayList<>();

                Double sameitemgst = 0.0, sameitemtotalgst = 0.0;
                List<String> taxlist1 = new ArrayList<>();
                final List<String> itemnamelist = new ArrayList<>();
                List<Double> qtylist = new ArrayList<>();
                List<String> ratelist = new ArrayList<>();
                List<Double> amtlist = new ArrayList<>();
                List<String> indexlist1 = new ArrayList<>();
                for (int k = 0; k < ModelSale.arry_item_name.size(); k++) {
                    itemtax = Double.parseDouble(taxlistupdated.get(k));
                    Cursor c = db.getdistinct(String.valueOf(itemtax));
                    while (c.moveToNext()) {
                        taxlist1.add(c.getString(0));
                        String stringName = c.getString(0);
                        if(checkArray==null || checkArray.size()==0){
                            Log.v("TAG","NO COMPLEMENT");
                            if (ModelSale.arry_item_name.contains(c.getString(0))) {
                                if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                    indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    itemnamelist.add(c.getString(0));
                                    qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                }
                                if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                } else {
                                    itemnamelist.add(c.getString(0));
                                    indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                }
                            }
                        }
                        else{
                            if (ModelSale.arry_item_name.get(k).equals(stringName)) {
                                Log.v("TAGG",ModelSale.arry_item_name.get(k));
                                if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                    //   for(int q=0;q<checkArray.size();q++) {
                                    if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                        Log.v("TAG","1 IN");
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        //ratelist.add(k,"0.0");
                                        amtlist.add(k, 111.0);

                                        if (counter<=2) {
                                            double current = Double.parseDouble(tv_total_amt.getText().toString());
                                            double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                            total_val = current - total_val;
                                            if(total_val<=0.0){
                                                tv_total_amt.setText("0.0");
                                            }
                                            else {
                                                tv_total_amt.setText("" + String.format("%.02f", total_val));
                                            }
                                            Log.v("TAG", String.valueOf(total_val));
                                        }
                                    } else {
                                        Log.v("TAG","2 IN");
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        Log.v("TAG","2 OUT");
                                    }
                                }
                                else if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                    if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                        Log.v("TAG","3 IN");
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));

                                        amtlist.add(k, 0.0);

                                        if (counter<=2) {
                                            double current = Double.parseDouble(tv_total_amt.getText().toString());
                                            double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                            total_val = current - total_val;
                                            if(total_val<=0.0){
                                                tv_total_amt.setText("0.0");
                                            }
                                            else {
                                                tv_total_amt.setText("" + String.format("%.02f", total_val));
                                            }
                                            Log.v("TAG", String.valueOf(total_val));
                                        }
                                    } else {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        Log.v("TAG","4 OUT");
                                    }
                                }
                                else {
                                    if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                        Log.v("TAG","4 IN");
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));

                                        amtlist.add(k, 0.0);

                                        if (counter<=2) {
                                            double current = Double.parseDouble(tv_total_amt.getText().toString());
                                            double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                            total_val = current - total_val;
                                            if(total_val<=0.0){
                                                tv_total_amt.setText("0.0");
                                            }
                                            else {
                                                tv_total_amt.setText("" + String.format("%.02f", total_val));
                                            }
                                            Log.v("TAG", String.valueOf(total_val));
                                        }
                                    } else {
                                        Log.v("TAGG", "CHECKIN4");
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        Log.v("TAGG", "CHECKOUT4");
                                    }
                                }
                            }
                        }
                    }
                }
                taxlist1.clear();
                for (int k = 0; k < itemnamelist.size(); k++) {
                    DecimalFormat formats = new DecimalFormat("#####.00");
                    taxlist1.clear();
                    int count = itemnamelist.size();
                    String itemname = itemnamelist.get(k);
                    String largname = "";
                    Double qty = qtylist.get(k);
                    String rate = ratelist.get(k);

                    String amount = String.valueOf(qty*Double.parseDouble(rate));
                    itemtax = Double.parseDouble(indexlist1.get(k));
                    Cursor c = db.getdistinct(String.valueOf(itemtax));
                    while (c.moveToNext()) {
                        taxlist1.add(c.getString(0));

                    }
                    cgst = (itemtax / 2);
                    sgst = cgst;
                    if (amount.length() > 4) {
                        amount = amount.substring(0, 4);
                    } else {
                        int length = amount.length();
                        int l1 = 0;
                        if (length < 4) {
                            l1 = 4 - length;
                            amount = String.format(amount + "%" + (l1) + "s", "");
                        }
                    }
                    List<String> list = new ArrayList<>();
                    for (String aObject : taxlist1) {
                        if (itemnamelist.contains(aObject)) {
                            list.add(aObject);
                        }
                    }
                    result = list;
                    i("dataname", String.valueOf(result));
                    int resultcount = result.size();

                    if (itemname.length() > 15) {
                        if (rate.length() == 3) {
                            if(itemtax==0.0){}
                            else {
                                count1++;
                                if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                    int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                    int p = result.indexOf(itemnamelist.get(k));
                                    int f = taxlist1.size();
                                    int h = itemnamelist.size();
                                    if (p == (result.size() - 1)) {
                                        if (amount.equals("0.0   ")) {
                                        } else {
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer1.append("  " + itemtax + "%    " + String.format("%.02f", sameitemtotalgst / 2) + "+" + String.format("%.02f", sameitemtotalgst / 2) + "     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                        }
                                    } else if (result.size() == 1) {
                                        if (amount.equals("0.0   ")) {
                                        } else {
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                         buffer1.append("  " + itemtax + "%    " + String.format("%.02f", sameitemtotalgst / 2) + "+" + String.format("%.02f", sameitemtotalgst / 2) + "     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                        }
                                    } else {
                                        if (g == f || count == count1) {
                                            if (amount.equals("0.0   ")) {
                                            } else {
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
                                                buffer1.append("  " + itemtax + "%    " + String.format("%.02f", sameitemtotalgst / 2) + "+" + String.format("%.02f", sameitemtotalgst / 2) + "     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                            }
                                        } else {

                                            if (amount.equals("0.0   ")) {
                                            } else {
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            }
                                        }
                                    }
                                } else {
                                    if (amount.equals("0.0   ")) {
                                    } else {
                                        totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));

                                        grandgst = grandgst + totalwithgst;
                                        buffer1.append("  " + itemtax + "%    " + String.format("%.02f", (totalwithgst / 2)) + "+" + String.format("%.02f", (totalwithgst / 2)) + "     " + String.format("%.02f", totalwithgst) + "\n");
                                    }
                                }
                            }
                        } else {
                            if (itemtax == 0.0) {
                            } else {
                                if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                    int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                    int p = result.indexOf(itemnamelist.get(k));
                                    int f = taxlist1.size();
                                    int h = itemnamelist.size();
                                    if (p == (result.size() - 1)) {
                                        if (amount.equals("0.0   ")) {
                                        } else {
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer1.append("  " + itemtax + "%    " + String.format("%.02f", sameitemtotalgst / 2) + "+" + String.format("%.02f", sameitemtotalgst / 2) + "     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                        }
                                    } else if (result.size() == 1) {
                                        if (amount.equals("0.0   ")) {
                                        } else {
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer1.append("  " + itemtax + "%    " + String.format("%.02f", sameitemtotalgst / 2) + "+" + String.format("%.02f", sameitemtotalgst / 2) + "     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                        }
                                    } else {
                                        if (g == f || count == count1) {
                                            if (amount.equals("0.0   ")) {
                                            } else {
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
                                                buffer1.append("  " + itemtax + "%    " + String.format("%.02f", sameitemtotalgst / 2) + "+" + String.format("%.02f", sameitemtotalgst / 2) + "     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                            }
                                        } else {
                                            if (amount.equals("0.0   ")) {
                                            } else {
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            }
                                        }
                                    }
                                } else {
                                    if (amount.equals("0.0   ")) {
                                    } else {
                                        totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                        buffer1.append("  " + itemtax + "%    " + String.format("%.02f", (totalwithgst / 2)) + "+" + String.format("%.02f", (totalwithgst / 2)) + "     " + String.format("%.02f", totalwithgst) + "\n");
                                        grandgst = grandgst + totalwithgst;
                                    }
                                }
                            }
                        }
                    } else {
                        int length = itemname.length();
                        int l1 = 0;
                        if (length < 15) {
                            l1 = 15 - length;
                            String itemname1 = itemname.concat(l1 + " ");
                            itemname = String.format(itemname + "%" + (l1) + "s", "");
                        }
                        if (rate.length() == 3) {
                            count1++;
                            if(itemtax==0.0){}
                            else {
                                if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                    int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                    int p = result.indexOf(itemnamelist.get(k));
                                    int f = taxlist1.size();
                                    int h = itemnamelist.size();
                                    if (p == (result.size() - 1)) {
                                        if (amount.equals("0.0   ")) {
                                        } else {
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer1.append("  " + itemtax + "%    " + String.format("%.02f", sameitemtotalgst / 2) + "+" + String.format("%.02f", sameitemtotalgst / 2) + "     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                        }
                                    } else if (result.size() == 1) {
                                        if (amount.equals("0.0   ")) {
                                        } else {
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer1.append("  " + itemtax + "%    " + String.format("%.02f", sameitemtotalgst / 2) + "+" + String.format("%.02f", sameitemtotalgst / 2) + "     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                        }
                                    } else {
                                        if (g == f || count == count1) {
                                            if (amount.equals("0.0   ")) {
                                            } else {
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
                                                buffer1.append("  " + itemtax + "%    " + String.format("%.02f", sameitemtotalgst / 2) + "+" + String.format("%.02f", sameitemtotalgst / 2) + "     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                            }
                                        } else {
                                            if (amount.equals("0.0   ")) {
                                            } else {
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            }
                                        }
                                    }
                                } else {
                                    if (amount.equals("0.0   ")) {
                                    } else {
                                        totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                        buffer1.append("  " + itemtax + "%    " + String.format("%.02f", (totalwithgst / 2)) + "+" + String.format("%.02f", (totalwithgst / 2)) + "     " + String.format("%.02f", totalwithgst) + "\n");
                                        grandgst = grandgst + totalwithgst;
                                    }
                                }
                            }
                        } else {
                            count1++;
                            if (itemtax == 0.0) {
                            } else {
                                if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                    int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                    int p = result.indexOf(itemnamelist.get(k));
                                    int f = taxlist1.size();
                                    int h = itemnamelist.size();
                                    if (p == (result.size() - 1)) {
                                        if (amount.equals("0.0   ")) {
                                        } else {
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer1.append("  " + itemtax + "%    " + String.format("%.02f", sameitemtotalgst / 2) + "+" + String.format("%.02f", sameitemtotalgst / 2) + "     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                        }
                                    } else if (result.size() == 1) {
                                        if (amount.equals("0.0   ")) {
                                        } else {
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer1.append("  " + itemtax + "%    " + String.format("%.02f", sameitemtotalgst / 2) + "+" + String.format("%.02f", sameitemtotalgst / 2) + "     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                        }
                                    } else {
                                        if (g == f || count == count1) {
                                            if (amount.equals("0.0   ")) {
                                            } else {
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
                                                buffer1.append("  " + itemtax + "%    " + String.format("%.02f", sameitemtotalgst / 2) + "+" + String.format("%.02f", sameitemtotalgst / 2) + "     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                            }
                                        } else {
                                            if (amount.equals("0.0   ")) {
                                            } else {
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            }
                                        }
                                    }
                                } else {
                                    if (amount.equals("0.0   ")) {
                                    } else {
                                        totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                        buffer1.append("  " + itemtax + "%    " + String.format("%.02f", (totalwithgst / 2)) + "+" + String.format("%.02f", (totalwithgst / 2)) + "     " + String.format("%.02f", totalwithgst) + "\n");
                                        grandgst = grandgst + totalwithgst;
                                    }
                                }
                            }
                        }
                    }
                    j++;
                    finalgst = totalwithgst + finalgst;
                    totalwithgst = 0.0;
                    cgst = 0.0;
                    sgst = 0.0;
                }

                Double totalAmount = Double.parseDouble(tv_total_amt.getText().toString()) + finalgst;
                printClass.IntentPrint(String.valueOf(buffer));

                double ddata = Double.parseDouble(tv_total_amt.getText().toString());
                DecimalFormat f = new DecimalFormat("#####.00");
                if(ddata<=0.0){
                    ddata=0.0;
                }
                else{
                    ddata=ddata;
                }
                data = "              TOTAL(Rs): " + String.format("%.02f",ddata) + "\n";
                printClass.IntentPrint(d);
                printClass.IntentPrint(data);
                if(finalgst==0.0){}
                else {
                    data = "\n          GST Details \n";
                    printClass.IntentPrint(data);
                    data = "  GST%    CGST+SGST     Total \n";
                    printClass.IntentPrint(data);
                    printClass.IntentPrint(String.valueOf(buffer1));
                }
                if (f1 == null || f1.length() == 0 || f1 == "") {
                    f1 = null;
                } else {
                    data = "     " + f1 + "\n";
                    printClass.IntentPrint(data);
                }
                if (f2 == null || f2.length() == 0 || f2 == "") {
                    f2 = null;
                } else {
                    data = "     " + f2 + "\n";
                    printClass.IntentPrint(data);
                }
                if (f3 == null || f3.length() == 0 || f3 == "") {
                    f3 = null;
                } else {

                    data = "     " + f3 + "\n";
                    printClass.IntentPrint(data);
                }
                if (f4 == null || f4.length() == 0 || f4 == "") {
                    f4 = null;
                } else {
                    data = "     " + f4 + "\n";
                    printClass.IntentPrint(data);
                }
                if (f5 == null || f5.length() == 0 || f5 == "") {
                    f5 = null;
                } else {
                    data = "     " + f5 + "\n";
                    printClass.IntentPrint(data);
                }
                printClass.IntentPrint("\n\n\n\n");
                clr_all_mtd();
                ids = db.getAllbillID();
                autoinnc();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    private void gst_data_blueprints_two_inch_gst_disable() {
        String billwithprint=manager.getBillWithPrint();
        String multipleprint=manager.getMultipleOption();
        if(billwithprint.equals("Yes")&&multipleprint.equals("No")){
            Log.v("TAGGG","NO");
            twoInchGSTDisable();
        }
        if(billwithprint.equals("Yes")&&multipleprint.equals("Yes")){
            Log.v("TAGGG","YES");
            twoInchGSTDisable();
        }
    }

    private void autoinnc() {
        String resetbill=manager.getResetBill();
        try {
            ids= db.getAllbillID();
            int r=1;
            Date today = new Date(); String d="";
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            String dateToday = format.format(today);
            String yesterday="";
            if(resetbill.equals("Yes")) {
                while (ids.moveToNext()) {
                    d = ids.getString(0);
                    yesterday = ids.getString(1);
                }
                if (yesterday == null || d == null || d == "") {
                    billno = r;
                } else if (!dateToday.equals(yesterday)) {

                    billno = r;
                } else if (dateToday.equals(yesterday)) {
                    r = Integer.parseInt(d);
                    r = r + 1;
                    billno = r;
                }
                else {
                    r = Integer.parseInt(d);
                    r = r + 1;
                    billno = r;
                }
            }
            else if(resetbill.equals("No")) {
                while (ids.moveToNext()) {
                    d = ids.getString(0);
                    if (d == ""||d==null) {
                        billno=1;
                    } else {
                        r = Integer.parseInt(ids.getString(0));
                        r = r + 1;
                        billno= r;
                    }
                }
            }
        }
        catch (Exception ex) {
            Message.message(getContext(),""+ex.getMessage());
        }
    }

    class CodeWiseRecycle extends RecyclerView.Adapter<CodewiseHolder>{

        @NonNull
        @Override
        public CodewiseHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View itemView = inflater.inflate(R.layout.layout_codewise,parent,false);
            return new CodewiseHolder(itemView);

        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull final CodewiseHolder holder, final int position) {
            ClassCodeWiseList codewise=itemArrayList.get(position);
            holder.tv_sr_no.setText(codewise.getSr_no() + "");
            holder.tv_item_name1.setText(codewise.getItem_name() + "");
            holder.tv_rate.setText(codewise.getRate() + "");
            holder.tv_qty1.setText(codewise.getQty() + "");
            holder.tv_amount.setText(codewise.getAmount() + "");
            holder.tv_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        total_bill_amount = total_bill_amount - (ModelSale.arr_item_price.get(position));
                        if(total_bill_amount<=0){
                            tv_total_amt.setText("0.0");
                            total_bill_amount=0;
                        }else {
                            tv_total_amt.setText("" + String.format("%.02f", total_bill_amount));
                            if(tv_total_amt.getText().toString().equals("-0.00")){
                                tv_total_amt.setText("0.00");
                            }
                        }
                        String name= ModelSale.arry_item_name.get(position);
                        ModelSale.arr_item_rate.remove(position);
                        ModelSale.arry_item_name.remove(position);
                        ModelSale.arr_item_qty.remove(position);
                        ModelSale.arr_item_price.remove(position);
                        ModelSale.arr_item_basicrate.remove(position);
                        ModelSale.arr_item_dis_rate.remove(position);
                        ModelSale.arr_item_dis.remove(position);
                        ModelSale.arr_item_tax.remove(position);
                        if (checkArray.size()!=0){
                            for(int i=0;i<checkArray.size();i++){
                                if(checkArray.get(i).equals(name)) {
                                    checkArray.remove(i);
                                    break;
                                }
                            }
                        }
                        if(position==(itemArrayList.size()-1)){
                            cnt=position+1;
                        }

                        itemArrayList.remove(position);
                      if(itemArrayList.size()==0){
                            cnt=1;
                        }
                        for(int i = (position);i<itemArrayList.size();i++){
                            itemArrayList.set(i,new ClassCodeWiseList(""+(i+1) , "" + ModelSale.arry_item_name.get(i),  "" +Double.parseDouble(ModelSale.arr_item_basicrate.get(i)), "" + ModelSale.arr_item_qty.get(i),"" + String.format("%.02f",ModelSale.arr_item_price.get(i)), ""));
                            cnt=i+2;
                        }
                        notifyDataSetChanged();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });

            holder.chkIos.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    int model_pos = Integer.parseInt(holder.tv_sr_no.getText().toString()) - 1;
                    if (b) {
                        String item = itemArrayList.get(model_pos).getItem_name();
                        if (!checkArray.contains(item))
                            checkArray.add(itemArrayList.get(model_pos).getItem_name());
                    }else{
                        checkArray.remove(itemArrayList.get(model_pos).getItem_name());
                        Log.v("TAGGGG",itemArrayList.get(model_pos).getItem_name());
                    }
                    Log.v("TAG",String.valueOf(checkArray.size()));
                }
            });

            for(int i=0;i<checkArray.size();i++) {
                if(itemArrayList.get(position).getItem_name().equals(checkArray.get(i).toString())) {
                    holder.chkIos.setChecked(true);
                }
            }
        }

        @Override
        public int getItemCount() {
            return itemArrayList.size();
        }
    }

    @Override
    public int getMenuLayout() {
        return R.menu.codewise_menu;
    }

    @Override
    public int getFragmentLayout() {
        return R.layout.codewise_fragment;
    }
}
