package com.iping.airbillonline.fragments;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.iping.airbillonline.R;
import com.iping.airbillonline.utils.SyncData;

import java.util.Objects;

public class AdminDashboardFragment extends AbstractFragment {

    private TextView tv_today_sale;
    private TextView tv_today_bills;
    private TextView tv_total_sale;
    private TextView tv_total_bills;
    private TextView tv_today_deleted_bills;
    private TextView tv_total_deleted_bills;

    private int total_bill=0;
    private int todays_bill=0;
    private int total_deleted_bill=0;
    private int today_deleted_bill=0;
    private double total_sale=0;
    private double todays_sale=0;

    @Override
    public void onStart() {
        super.onStart();
        View view=getView();

        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        SyncData syncData=new SyncData(getContext());
                        syncData.getcount();
                    }
                });
            }
        };

        executor.execute(runnable);

        tv_today_sale= Objects.requireNonNull(view).findViewById(R.id.tv_today_sale);
        tv_today_bills= view.findViewById(R.id.tv_today_bills);
        tv_total_sale= view.findViewById(R.id.tv_total_sale);
        tv_total_bills= view.findViewById(R.id.tv_total_bills);
        tv_today_deleted_bills= view.findViewById(R.id.tv_today_deleted_bills);
        tv_total_deleted_bills= view.findViewById(R.id.tv_total_deleted_bills);
        try {
            total_bill = db.get_total_bills();
            tv_total_bills.setText("" + total_bill);
            total_sale = db.get_total_sale();
            tv_total_sale.setText("" + total_sale);
            total_deleted_bill = db.get_total_deleted_bills();
            tv_total_deleted_bills.setText("" + total_deleted_bill);
            todays_bill = db.get_todays_bill();
            tv_today_bills.setText("" + todays_bill);
            todays_sale = db.get_todays_sales();
            tv_today_sale.setText("" + todays_sale);
            today_deleted_bill = db.get_todays_deleted_bill();
            tv_today_deleted_bills.setText("" + today_deleted_bill);
        }catch(Exception e){
            e.printStackTrace();
            Toast.makeText(getContext(),"Error=",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public int getMenuLayout() {
        return R.menu.default_menu;
    }

    @Override
    public int getFragmentLayout() {
        return R.layout.admindash_board_fragment;
    }
}