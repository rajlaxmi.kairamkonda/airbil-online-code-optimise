package com.iping.airbillonline.fragments;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.iping.airbillonline.R;
import com.iping.airbillonline.pojo.DeletedSalesBillReportPOJO;
import com.iping.airbillonline.pojo.SalesBillReportPOJO;
import com.iping.airbillonline.recycle.DeletedSalesBillReportRecycle;
import com.iping.airbillonline.recycle.SalesBillReportRecycle;
import com.iping.airbillonline.viewmodel.DeletedSalesBillReportViewModel;
import com.iping.airbillonline.viewmodel.SalesBillReportViewModel;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class DeletedSalesBillReportFragment extends AbstractFragment {
    private Date today = new Date();
    private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

    private DeletedSalesBillReportViewModel deletedSalesBillReportViewModel;

    private RecyclerView deletedSalesBillReportRecycle;

    private List<DeletedSalesBillReportPOJO> deletedSalesBillReportList;

    private String fromdate=format.format(today);
    private String todate=format.format(today);
    private String startdate;

    private long miliSecsDate;

    private TextView et_from_date;
    private TextView tvamt;
    private TextView et_to_date;
    private Button btn_submit;

    private DeletedSalesBillReportFragment fragment=this;

    @Override
    public void onStart() {
        super.onStart();
        View view=getView();
        deletedSalesBillReportViewModel= new ViewModelProvider(Objects.requireNonNull(getActivity())).get(DeletedSalesBillReportViewModel.class);
        deletedSalesBillReportViewModel.init(getActivity(),fromdate,todate);

        deletedSalesBillReportRecycle= Objects.requireNonNull(view).findViewById(R.id.deleted_sales_bill_report_recycle);
        et_from_date= Objects.requireNonNull(view).findViewById(R.id.et_from_date);
        et_to_date= view.findViewById(R.id.et_to_date);
        btn_submit= view.findViewById(R.id.btn_submit);
        tvamt= view.findViewById(R.id.tvamt);

        final LinearLayoutManager manager = new LinearLayoutManager(getContext());
        deletedSalesBillReportRecycle.setLayoutManager(manager);
        deletedSalesBillReportRecycle.setHasFixedSize(true);
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getContext(),R.anim.slide_from_right_layout);
        deletedSalesBillReportRecycle.setLayoutAnimation(animation);

        et_from_date.setOnClickListener(this);
        et_to_date.setOnClickListener(this);
        btn_submit.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        deletedSalesBillReportViewModel.getDeletedReport().observe(Objects.requireNonNull(getActivity()), new Observer<List<DeletedSalesBillReportPOJO>>() {
            @Override
            public void onChanged(List<DeletedSalesBillReportPOJO> salesBillReportPOJOS) {
                if (salesBillReportPOJOS.size()>0) {
                    DeletedSalesBillReportRecycle adapter = new DeletedSalesBillReportRecycle(salesBillReportPOJOS);
                    deletedSalesBillReportList=salesBillReportPOJOS;
                    deletedSalesBillReportRecycle.setAdapter(adapter);
                    tvamt.setText(deletedSalesBillReportViewModel.getTotalAmt());
                }else{
                    TextView view= Objects.requireNonNull(getView()).findViewById(R.id.no_found);
                    deletedSalesBillReportRecycle.setVisibility(View.INVISIBLE);
                    view.setVisibility(View.VISIBLE);
                    tvamt.setText("0.0");
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        if (v.getId()==et_from_date.getId())
            datePicker1();
        if(v.getId()==et_to_date.getId())
            datePicker2();
        if (v.getId()==btn_submit.getId()){
            if(et_from_date.getText().toString().equals("") || et_to_date.getText().toString().equals("")){
                Toast.makeText(getContext(),"Please select the From and To Date",Toast.LENGTH_SHORT).show();
            }else {
                deletedSalesBillReportViewModel.init(getActivity(),et_from_date.getText().toString(),et_to_date.getText().toString());
            }
        }
    }

    private void datePicker1(){
        final Calendar c1 = Calendar.getInstance();
        int mYear1 = c1.get(Calendar.YEAR);
        int mMonth1 = c1.get(Calendar.MONTH);
        int mDay1 = c1.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        et_from_date.setText(dayOfMonth+"-"+(monthOfYear+1)+"-"+year);
                        if ((monthOfYear+1)<10 && dayOfMonth<10 ) {
                            fromdate = year + "-0" + (monthOfYear + 1) + "-0" + dayOfMonth;
                        }
                        else if((monthOfYear+1)<10 && dayOfMonth>10) {
                            fromdate = year + "-0" + (monthOfYear + 1) + "-" + dayOfMonth;
                        }
                        else if((monthOfYear+1)>=10 && dayOfMonth<10) {
                            fromdate = year + "-" + (monthOfYear + 1) + "-0" + dayOfMonth;
                        }
                        else if((monthOfYear+1)>=10 && dayOfMonth==10) {
                            fromdate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        }
                        else if((monthOfYear+1)>=10 && dayOfMonth>10) {
                            fromdate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        }
                        startdate=year+"-"+(monthOfYear+1)+"-"+(dayOfMonth);
                        miliSecsDate = milliseconds(startdate);
                    }
                }, mYear1, mMonth1, mDay1);

        datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
            }
        });
        datePickerDialog.show();
    }

    private void datePicker2(){
        final Calendar c2 = Calendar.getInstance();
        int mYear2 = c2.get(Calendar.YEAR);
        int mMonth2 = c2.get(Calendar.MONTH);
        int mDay2 = c2.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        et_to_date.setText(dayOfMonth+"-"+(monthOfYear+1)+"-"+year);
                        if ((monthOfYear+1)<10 && dayOfMonth<10 ) {
                            todate = year + "-0" + (monthOfYear + 1) + "-0" + dayOfMonth;
                        }
                        else if((monthOfYear+1)<10 && dayOfMonth>=10) {
                            todate = year + "-0" + (monthOfYear + 1) + "-" + dayOfMonth;
                        }
                        else if((monthOfYear+1)>=10 && dayOfMonth<10) {
                            todate = year + "-" + (monthOfYear + 1) + "-0" + dayOfMonth;
                        }
                        else if((monthOfYear+1)>=10 && dayOfMonth==10) {
                            todate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        }
                        else if((monthOfYear+1)>=10 && dayOfMonth>10) {
                            todate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        }
                    }
                }, mYear2, mMonth2, mDay2);

        datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {

            }
        });
        if(startdate.equals("")){
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        }else {
            datePickerDialog.getDatePicker().setMinDate(miliSecsDate);
        }
        datePickerDialog.show();
    }

    @Override
    public int getMenuLayout() {
        return R.menu.print_menu;
    }

    @Override
    public int getFragmentLayout() {
        return R.layout.deleted_sales_bill_report_fragment;
    }
}
