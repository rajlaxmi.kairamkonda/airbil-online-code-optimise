package com.iping.airbillonline.fragments;

import android.database.Cursor;
import android.os.Handler;
import android.os.Looper;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.iping.airbillonline.R;
import com.iping.airbillonline.pojo.ItemListPOJO;
import com.iping.airbillonline.recycle.ItemListRecycle;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ItemListFragment extends AbstractFragment {

    private RecyclerView itemListRecycle;

    private List<ItemListPOJO> itemList;

    @Override
    public void onStart() {
        super.onStart();
        View view=getView();
        itemListRecycle= Objects.requireNonNull(view).findViewById(R.id.item_list_recycle);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        itemListRecycle.setLayoutManager(linearLayoutManager);
        itemListRecycle.setHasFixedSize(true);
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getContext(),R.anim.slide_from_bottom_layou);
        itemListRecycle.setLayoutAnimation(animation);
    }

    @Override
    public void onResume() {
        super.onResume();
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                getdata();
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if (itemList.size()>0) {
                            ItemListRecycle adapter = new ItemListRecycle(itemList);
                            itemListRecycle.setAdapter(adapter);
                        }else{
                            TextView view= Objects.requireNonNull(getView()).findViewById(R.id.no_found);
                            itemListRecycle.setVisibility(View.INVISIBLE);
                            view.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }
        };
        executor.execute(runnable);
    }

    private void getdata() {
        Cursor c = db.getAllItems();
        itemList=new ArrayList<>();
        try {
            if (c.getCount() != 0) {
                while (c.moveToNext()) {
                    itemList.add(new ItemListPOJO("" + c.getString(22), "" + c.getString(1), "" + c.getString(7)));
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId()==R.id.action_print){
            onPrintBill();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void onPrintBill() {
        if(print_option.equals("3 inch")) {
            try {
                String data = null;
                data = "                 " + "AirBill App" + "             \n";
                String d = "_____________________________________________\n";
                data = "No.| Item Name                       | Rate  \n";
                printClass.IntentPrint(data);
                int a=1;
                if (itemList.size()>0){
                    StringBuffer buffer = new StringBuffer();
                    for (ItemListPOJO item:itemList){
                        String itemname=item.getName();
                        if (itemname.length() > 22) {
                            buffer.append(a+"  "+ itemname.substring(0, 22)+"              "+item.getRate()+"\n");
                        } else {
                            int length = itemname.length();
                            int l1 = 0;
                            if (length < 15) {
                                l1 = 15 - length;
                                String itemname1 = itemname.concat(l1 + " ");
                                itemname = String.format(itemname + "%" + (l1 - 2) + "s", "");
                            }
                            buffer.append(a+"  "+itemname+"                       "+item.getRate()+"\n");
                        }
                        a++;
                    }
                    printClass.IntentPrint(String.valueOf(buffer));
                    printClass.IntentPrint("\n\n\n\n");
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        if(print_option.equals("2 inch")) {
            try {
                try {
                    String data = null;
                    data = "                 " + "AirBill App" + "             \n";
                    String d = "\n_________________________________\n";
                    data = "No.| Item Name         |Rate  \n";
                    printClass.IntentPrint(data);
                    int a=1;
                    if (itemList.size()>0){
                        StringBuffer buffer = new StringBuffer();
                        for (ItemListPOJO item:itemList){
                            String itemname=item.getName();
                            if (itemname.length() > 15) {
                                buffer.append(a+"  "+ itemname.substring(0, 15)+"      "+item.getRate()+"\n");
                            } else {
                                int length = itemname.length();
                                int l1 = 0;
                                if (length < 15) {
                                    l1 = 15 - length;
                                    String itemname1 = itemname.concat(l1 + " ");
                                    itemname = String.format(itemname + "%" + (l1) + "s", "");
                                }
                                buffer.append(a+"  "+itemname+"        "+item.getRate()+"\n");
                            }
                            a++;
                        }
                        printClass.IntentPrint(String.valueOf(buffer));
                        printClass.IntentPrint("\n\n\n\n");
                    }
                 } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public int getMenuLayout() {
        return R.menu.print_menu;
    }

    @Override
    public int getFragmentLayout() {
        return R.layout.item_list_fragment;
    }
}
