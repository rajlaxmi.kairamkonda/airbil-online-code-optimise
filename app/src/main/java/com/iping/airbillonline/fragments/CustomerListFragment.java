package com.iping.airbillonline.fragments;

import android.database.Cursor;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.iping.airbillonline.R;
import com.iping.airbillonline.pojo.ItemListPOJO;
import com.iping.airbillonline.recycle.CustomerListRecycle;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CustomerListFragment extends AbstractFragment {

    private RecyclerView customerListRecycle;

    private List<ItemListPOJO> customerList;
    public CustomerListFragment(){

    }

    @Override
    public void onStart() {
        super.onStart();
        View view=getView();
        customerListRecycle= Objects.requireNonNull(view).findViewById(R.id.customer_list_recycle);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        customerListRecycle.setLayoutManager(linearLayoutManager);
        customerListRecycle.setHasFixedSize(true);
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getContext(),R.anim.slide_from_bottom_layou);
        customerListRecycle.setLayoutAnimation(animation);
    }

    @Override
    public void onResume() {
        super.onResume();
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                getData();
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if (customerList.size()>0) {
                            CustomerListRecycle adapter = new CustomerListRecycle(customerList);
                            customerListRecycle.setAdapter(adapter);
                        } else{
                            TextView view= Objects.requireNonNull(getView()).findViewById(R.id.no_found);
                            customerListRecycle.setVisibility(View.INVISIBLE);
                            view.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }
        };
        executor.execute(runnable);
    }

    private void getData(){
        customerList=new ArrayList<>();
        Cursor c=db.getCustomerDetails();
        if(c.getCount() != 0) {
            while (c.moveToNext()) {
                customerList.add(new ItemListPOJO(""+c.getString(0),""+c.getString(1),""+c.getString(2)));
            }
        }
    }


    @Override
    public int getMenuLayout() {
        return R.menu.default_menu;
    }

    @Override
    public int getFragmentLayout() {
        return R.layout.customer_list_fragment;
    }
}
