package com.iping.airbillonline.fragments;

import android.database.Cursor;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.iping.airbillonline.R;
import com.iping.airbillonline.pojo.ItemListPOJO;
import com.iping.airbillonline.recycle.CustomerListRecycle;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ListCategoryFragment extends AbstractFragment {

    private RecyclerView categoryRecycle;

    private List<ItemListPOJO> categoryList;

    public ListCategoryFragment(){

    }

    @Override
    public void onStart() {
        super.onStart();
        View view=getView();
        categoryRecycle= Objects.requireNonNull(view).findViewById(R.id.category_recycle);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        categoryRecycle.setLayoutManager(linearLayoutManager);
        categoryRecycle.setHasFixedSize(true);
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getContext(),R.anim.slide_from_bottom_layou);
        categoryRecycle.setLayoutAnimation(animation);
    }

    @Override
    public void onResume() {
        super.onResume();
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                getData();
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if (categoryList.size()>0) {
                            CustomerListRecycle adapter = new CustomerListRecycle(categoryList);
                            categoryRecycle.setAdapter(adapter);
                        }else{
                            TextView view= Objects.requireNonNull(getView()).findViewById(R.id.no_found);
                            categoryRecycle.setVisibility(View.INVISIBLE);
                            view.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }
        };
        executor.execute(runnable);
    }

    private void getData() {
        categoryList =new ArrayList<>();
        Cursor c=db.getCategoryDetails();
        if(c.getCount() != 0) {
            while (c.moveToNext()) {
                categoryList.add(new ItemListPOJO("",""+c.getString(1),""));
            }
        }
    }

    @Override
    public int getMenuLayout() {
        return R.menu.default_menu;
    }

    @Override
    public int getFragmentLayout() {
        return R.layout.list_category_fragment;
    }
}
