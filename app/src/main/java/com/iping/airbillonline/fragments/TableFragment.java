package com.iping.airbillonline.fragments;

import com.iping.airbillonline.R;

public class TableFragment extends AbstractFragment  {

    @Override
    public int getMenuLayout() {
        return R.menu.default_menu;
    }

    @Override
    public int getFragmentLayout() {
        return R.layout.table_fragment;
    }
}
