package com.iping.airbillonline.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.iping.airbillonline.R;
import com.iping.airbillonline.holder.CodewiseHolder;
import com.iping.airbillonline.pojo.ClassCodeWiseList;
import com.iping.airbillonline.utils.Message;
import com.iping.airbillonline.utils.ModelSale;
import com.iping.airbillonline.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import static android.util.Log.i;

public class ThumbnailFragment extends AbstractFragment {

    private ListView catgridvew;
    private GridView itemgridview1;
    private TextView tv_total_amt;
    private TextView tvbill;

    private RecyclerView thumbnailRecycle;

    private Cursor ids;

    private List<String> basicratelist= new ArrayList<>();
    private List<String> taxlist= new ArrayList<>();
    private List<String> taxlistupdated= new ArrayList<>();
    private List<String> samevalulist = new ArrayList<>();
    private List<String> ITEM_LIST= new ArrayList<>();
    private List<String> Category_LIST=new ArrayList<>();
    private List<String> price_list=new ArrayList<>();
    private List<String> checkArray;
    private List<String> Categorylist;
    private List<String> CategoryIdlist;
    private List<String> itemlist= new ArrayList<>();
    private List<String> printerList=new ArrayList<>();
    private List<String> printlist= new ArrayList<>();
    private List<String> tempbilling= new ArrayList<>();
    private List<String> disratelist= new ArrayList<>();
    private List<String> dis_list= new ArrayList<>();
    private List<ClassCodeWiseList> itemArrayList;

    private double totalrate1=0.0;
    private double totalamount=0.0;
    private double newqty=0.0;
    private double str_qty=0.0;

    private String str_item="";
    private String billwithprint;
    private String multipleprint;
    private String gst_option;
    private String rate="";
    private String itemname;
    private String str_set_cust_name="";
    private String str_set_id_cust="";
    private String str_set_payment="";
    private String str_set_pointofsale="";
    private String payment_mode_id="";
    private String order_details="";
    private String payment_deatils="";
    private String empcode="";
    private String emp_code="";
    private String Name="";

    private int bno=1;
    private int counter = 1;
    private int cnt=1;
    private double cost=0;

    LinearLayout ll_all_items;
    private ThumbnailRecycle adapter;
    public ThumbnailFragment(){}

    @Override
    public void onStart() {
        super.onStart();
        View view=getView();
        checkArray=new ArrayList<>();
        catgridvew= Objects.requireNonNull(view).findViewById(R.id.gd_category);
        itemgridview1=view.findViewById(R.id.gd_items);
        tv_total_amt=view.findViewById(R.id.tv_total_amt);
        tvbill=view.findViewById(R.id.billno);
        ll_all_items=view.findViewById(R.id.ll_all_items);
        tv_total_amt.setOnClickListener(this);

        autoinnc();
        billwithprint=manager.getBillWithPrint();
        multipleprint=manager.getMultipleOption();
        gst_option=manager.getGstOption();

        categoryRecycleList();
    }

    private void categoryRecycleList() {
        Categorylist = db.getAllCategory();
        CategoryIdlist = db.getAllCategoryId();

        Category_LIST = new ArrayList<>(Categorylist);
        ITEM_LIST = new ArrayList<>();
        price_list = new ArrayList<>();
        itemArrayList = new ArrayList<ClassCodeWiseList>();

        catgridvew.setAdapter(new ArrayAdapter<String>(
                getContext(),android.R.layout.simple_list_item_1, Category_LIST){
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            public View getView(int position, View convertView, ViewGroup parent) {

                View view = super.getView(position,convertView,parent);
                TextView tv = (TextView) view;
                RelativeLayout.LayoutParams lp =  new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                tv.setLayoutParams(lp);
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)tv.getLayoutParams();
                params.width = getPixelsFromDPs(Objects.requireNonNull(getActivity()),90);
                params.height = getPixelsFromDPs(getActivity(),50);
                tv.setLayoutParams(new GridView.LayoutParams(GridView.AUTO_FIT, 120));
                tv.setGravity(Gravity.CENTER);
                tv.setGravity(Gravity.CENTER);
                tv.setTypeface(Typeface.defaultFromStyle(Typeface.ITALIC), Typeface.NORMAL);
                tv.setTextColor(Color.WHITE);
                tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);
                String getname=Category_LIST.get(position);
                tv.setText(getname);
                tv.setTypeface(tv.getTypeface(), Typeface.BOLD);
                return tv;
            }
        });
        catgridvew.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {

                AlertDialog.Builder alert = new AlertDialog.Builder(
                        getContext());
                String catname=Category_LIST.get(i);
                alert.setMessage(""+catname);
                alert.show();
                return true;
            }
        });

        itemgridview1.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {

                AlertDialog.Builder alert = new AlertDialog.Builder(
                        getContext());
                String itemname=ITEM_LIST.get(i);
                alert.setMessage(""+itemname);
                alert.show();
                return true;
            }
        });

        catgridvew.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                itemlist.clear();
                price_list.clear();
                basicratelist.clear();
                taxlist.clear();
                ITEM_LIST= new ArrayList<>(itemlist);
                price_list= new ArrayList<>(price_list);

                String selectedItem= adapterView.getItemAtPosition(i).toString();
                String name=Categorylist.get(i);
                String catid=CategoryIdlist.get(i);
                Cursor c =db.getAllCategorywiseItem(catid);
                try {
                    if (c.getCount() == 0) {
                        Message.message(getContext(), "Item not exists");
                    } else {
                        while (c.moveToNext()) {
                            if (gst_option.equals("GST Disable")) {
                                itemlist.add(c.getString(1));
                                price_list.add(c.getString(7));
                                taxlist.add(c.getString(5));
                                basicratelist.add(c.getString(2));
                                disratelist.add(c.getString(4));

                                dis_list.add(c.getString(3));

                            } else if (gst_option.equals("GST Enable")) {
                                itemlist.add(c.getString(1));
                                basicratelist.add(c.getString(2));
                                disratelist.add(c.getString(4));

                                taxlist.add(c.getString(5));
                                price_list.add(c.getString(7));
                                dis_list.add(c.getString(3));
                            }
                        }
                    }
                }catch(Exception ex) {
                    Message.message(getContext(),"Error "+ex.getMessage());
                }

                ITEM_LIST= new ArrayList<>(itemlist);
                itemgridview1.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, ITEM_LIST) {
                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                    public View getView(int position1, View convertView1, ViewGroup parent1) {

                        View view1 = super.getView(position1,convertView1,parent1);
                        TextView tv1 = (TextView) view1;
                        RelativeLayout.LayoutParams lp1 =  new RelativeLayout.LayoutParams(
                                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT
                        );
                        tv1.setLayoutParams(lp1);
                        RelativeLayout.LayoutParams params1 = (RelativeLayout.LayoutParams)tv1.getLayoutParams();
                        params1.width = getPixelsFromDPs(getActivity(),110);
                        tv1.setLayoutParams(new GridView.LayoutParams(GridView.AUTO_FIT, 120));
                        tv1.setGravity(Gravity.CENTER);
                        tv1.setGravity(Gravity.CENTER);
                        tv1.setTypeface(Typeface.SANS_SERIF, Typeface.NORMAL);
                        tv1.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                        String itemname=ITEM_LIST.get(position1);
                        tv1.setText(itemname);
                        ShapeDrawable sd = new ShapeDrawable();
                        sd.setShape(new RectShape());
                        sd.getPaint().setColor(Color.parseColor("#000000"));
                        sd.getPaint().setStrokeWidth(10f);
                        sd.getPaint().setStyle(Paint.Style.STROKE);
                        tv1.setBackground(sd);
                        return tv1;
                    }
                });
            }
        });

        itemgridview1.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        try {
                            if (gst_option.equals("GST Disable")) {
                                    if (ModelSale.arry_item_name.contains(itemlist.get(i))) {
                                        int index1 = ModelSale.arry_item_name.indexOf(itemlist.get(i));

                                        Double value = ModelSale.arr_item_qty.get(index1);ModelSale.arr_item_qty.set(index1, value + 1.0);
                                        totalrate1 = Double.valueOf(ModelSale.arr_item_rate.get(index1)) * Double.valueOf(ModelSale.arr_item_qty.get(index1));
                                        totalamount =Double.valueOf(String.format("%.02f",totalrate1)) + Double.valueOf(String.format("%.02f",totalamount));
                                        ModelSale.arr_item_price.set(index1, Double.valueOf(String.format("%.02f",totalrate1)));
                                        itemArrayList.set(index1, new ClassCodeWiseList("" + (index1 + 1), "" + ModelSale.arry_item_name.get(index1), "" + ModelSale.arr_item_qty.set(index1, value + 1.0), "" + Double.valueOf(ModelSale.arr_item_rate.get(index1)), "" + String.format("%.02f",Double.parseDouble(String.valueOf(totalrate1))), ""));
                                    }
                                    else {
                                        newqty = 1.0;
                                        totalrate1 = 1.0 * Double.parseDouble(price_list.get(i));
                                        totalamount = Double.valueOf(String.format("%.02f",totalrate1)) + Double.valueOf(String.format("%.02f",totalamount));
                                        ModelSale.arry_item_name.add(itemlist.get(i));
                                        ModelSale.arr_item_qty.add(newqty);
                                        ModelSale.arr_item_rate.add(price_list.get(i));
                                        ModelSale.arr_item_price.add(Double.valueOf(String.format("%.02f",totalrate1)));
                                        ModelSale.array_item_amt.add(Double.valueOf(String.format("%.02f",totalamount)));
                                        ModelSale.arr_item_tax.add(taxlist.get(i));
                                        ModelSale.arr_item_basicrate.add(basicratelist.get(i));
                                        ModelSale.arr_item_dis.add(dis_list.get(i));
                                        ModelSale.arr_item_dis_rate.add(disratelist.get(i));
                                        itemArrayList.add(new ClassCodeWiseList("" + cnt, "" + itemlist.get(i), "" + newqty, "" + price_list.get(i), "" + String.format("%.02f",Double.parseDouble(String.valueOf(totalrate1))), ""));
                                        cnt++;
                                    }

                                    cost = cost + Double.parseDouble(price_list.get(i));
                                    tv_total_amt.setText(String.format("%.02f",cost));
                                    itemname = itemlist.get(i);
                                    printlist.add(itemlist.get(i) + " " + price_list.get(i) + " " + i + "\n");
                                    int listSize = printlist.size();
                                    for (int j = 0; j < listSize; j++) {
                                        i("Member name: ", printlist.get(j));
                                    }
                                    bno = 1;
                                    bno++;
                                    i("data=:", str_item + "  " + "  " + str_qty + "  " + rate + "  " + "  " + totalrate1 + "  " + totalamount);

                            }
                            else if(gst_option.equals("GST Enable")){
                                if (ModelSale.arry_item_name.contains(itemlist.get(i))) {
                                    int index1 =ModelSale.arry_item_name.indexOf(itemlist.get(i));
                                    Double value=ModelSale.arr_item_qty.get(index1);
                                    ModelSale.arr_item_qty.set(index1, value + 1.0);
                                    totalrate1 = Double.valueOf(ModelSale.arr_item_rate.get(index1)) * Double.valueOf(ModelSale.arr_item_qty.get(index1));
                                    totalamount = Double.valueOf(String.format("%.02f",totalrate1)) + Double.valueOf(String.format("%.02f",totalamount));
                                    ModelSale.arr_item_price.set(index1, Double.valueOf(String.format("%.02f",totalrate1)));
                                    itemArrayList.set(index1,new ClassCodeWiseList(""+(index1+1) , "" + ModelSale.arry_item_name.get(index1), "" + ModelSale.arr_item_qty.set(index1, value + 1.0), "" +Double.valueOf(ModelSale.arr_item_rate.get(index1)), "" + String.format("%.02f",Double.parseDouble(String.valueOf(totalrate1))), ""));
                                } else {
                                    newqty = 1.0;
                                    totalrate1 = 1.0 * Double.parseDouble(price_list.get(i));
                                    totalamount = Double.valueOf(String.format("%.02f",totalrate1)) + Double.valueOf(String.format("%.02f",totalamount));
                                    ModelSale.arry_item_name.add(itemlist.get(i));
                                    ModelSale.arr_item_qty.add(newqty);
                                    ModelSale.arr_item_rate.add(price_list.get(i));
                                    ModelSale.arr_item_basicrate.add(basicratelist.get(i));
                                    ModelSale.arr_item_tax.add(taxlist.get(i));
                                    ModelSale.arr_item_price.add(Double.valueOf(String.format("%.02f",totalrate1)));
                                    ModelSale.array_item_amt.add(Double.valueOf(String.format("%.02f",totalamount)));
                                    ModelSale.arr_item_dis.add(dis_list.get(i));
                                    ModelSale.arr_item_dis_rate.add(disratelist.get(i));
                                    itemArrayList.add(new ClassCodeWiseList("" + cnt, "" +itemlist.get(i), "" +newqty, "" + price_list.get(i), "" + String.format("%.02f",Double.parseDouble(String.valueOf(totalrate1))), ""));
                                    cnt++;
                                }
                                cost = cost + Double.parseDouble(price_list.get(i));
                                tv_total_amt.setText(""+String.format("%.02f",cost));
                                itemname = itemlist.get(i);
                                printlist.add(itemlist.get(i) + " " + basicratelist.get(i) + " " + i + "\n");
                                int listSize = printlist.size();
                                for (int j = 0; j < listSize; j++) {
                                    i("Member name: ", printlist.get(j));
                                }
                                bno = 1;
                                bno++;
                                i("data=:", str_item + "  " + "  " + str_qty + "  " + rate + "  " + "  " + totalrate1 + "  " + totalamount);
                            }
                        }
                        catch (Exception ex) {
                            Message.message(getContext(),""+ex.getMessage());
                        }
                    }
                });

        ll_all_items.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemlist.clear();
                price_list.clear();
                basicratelist.clear();
                taxlist.clear();
                ITEM_LIST= new ArrayList<>(itemlist);
                price_list= new ArrayList<>(price_list);

                Cursor c =db.getAllItems();
                try {
                    if (c.getCount() == 0) {
                        Message.message(getContext(), "Item not exists");
                    } else {
                        while (c.moveToNext()) {
                            if (gst_option.equals("GST Disable")) {
                                itemlist.add(c.getString(1));
                                price_list.add(c.getString(7));
                                taxlist.add(c.getString(5));
                                basicratelist.add(c.getString(2));
                                disratelist.add(c.getString(4));

                                dis_list.add(c.getString(3));

                            } else if (gst_option.equals("GST Enable")) {
                                itemlist.add(c.getString(1));
                                basicratelist.add(c.getString(2));
                                disratelist.add(c.getString(4));

                                taxlist.add(c.getString(5));
                                price_list.add(c.getString(7));
                                dis_list.add(c.getString(3));
                            }
                        }
                    }
                }catch(Exception ex) {
                    Message.message(getContext(),"Error "+ex.getMessage());
                }

                ITEM_LIST= new ArrayList<>(itemlist);
                itemgridview1.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, ITEM_LIST) {
                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                    public View getView(int position1, View convertView1, ViewGroup parent1) {

                        View view1 = super.getView(position1,convertView1,parent1);
                        TextView tv1 = (TextView) view1;
                        RelativeLayout.LayoutParams lp1 =  new RelativeLayout.LayoutParams(
                                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT
                        );
                        tv1.setLayoutParams(lp1);
                        RelativeLayout.LayoutParams params1 = (RelativeLayout.LayoutParams)tv1.getLayoutParams();
                        params1.width = getPixelsFromDPs(getActivity(),110);
                        tv1.setLayoutParams(new GridView.LayoutParams(GridView.AUTO_FIT, 120));
                        tv1.setGravity(Gravity.CENTER);
                        tv1.setGravity(Gravity.CENTER);
                        tv1.setTypeface(Typeface.SANS_SERIF, Typeface.NORMAL);
                        tv1.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                        String itemname=ITEM_LIST.get(position1);
                        tv1.setText(itemname);
                        ShapeDrawable sd = new ShapeDrawable();
                        sd.setShape(new RectShape());
                        sd.getPaint().setColor(Color.parseColor("#000000"));
                        sd.getPaint().setStrokeWidth(10f);
                        sd.getPaint().setStyle(Paint.Style.STROKE);
                        tv1.setBackground(sd);
                        return tv1;
                    }
                });
            }
        });
    }

    public void clr_all_mtd(){
        ModelSale.arr_item_rate.clear();
        ModelSale.arry_item_name.clear();
        ModelSale.arr_item_qty.clear();
        ModelSale.arr_item_price.clear();
        ModelSale.arr_item_basicrate.clear();
        ModelSale.arr_item_dis_rate.clear();
        ModelSale.arr_item_dis.clear();
        ModelSale.arr_item_tax.clear();
        ModelSale.array_item_amt.clear();
        checkArray.clear();

        payment_deatils="";
        order_details="";
        str_set_cust_name="";
        str_set_payment="";
        payment_mode_id="";
        Name="";
        str_set_pointofsale="";
        cost = 0;
        tv_total_amt.setText("0");
        cnt = 1;
        itemArrayList.clear();

        if(billwithprint.equals("Yes"))
            adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        if (tv_total_amt.getId()==v.getId())
            showRecycle();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_customer:{
                if(tv_total_amt.getText().toString().equals("0.00") || tv_total_amt.getText().toString().equals("0.0") || tv_total_amt.getText().toString().equals("") || tv_total_amt.getText().toString().equals("0")) {

                }else{
                    customMessageBox();
                }
                return true;
        }
            case R.id.action_clear_all:{
                clr_all_mtd();
                return true;
            }
            case R.id.action_print:{
                onPrintBill();
                return true;
            }
            default:
            return super.onOptionsItemSelected(item);
        }
    }

    private void customMessageBox() {
        Button btn_save_cashcredit,payment_details;
        final Dialog dialog=new Dialog(getContext());
        dialog.setContentView(R.layout.layout_cash_credit);
        dialog.setTitle("Payment Details");
        btn_save_cashcredit= dialog.findViewById(R.id.btn_save_cashcredit);
        payment_details= dialog.findViewById(R.id.btn_payment_details);
        final AutoCompleteTextView at_customer_name= dialog.findViewById(R.id.at_customer_name);
        final Spinner sp_payment= dialog.findViewById(R.id.sp_cash_credit);
        final Spinner sppoint_of_contact= dialog.findViewById(R.id.sp_modeofpayment);

        List<String> users = new ArrayList<>();
        List<String> users1 = new ArrayList<>();
        final List<String> users2 = new ArrayList<>();

        final List<String> NameList= new ArrayList<>();
        List<String> IdList= new ArrayList<>();

        users.add(0, "Select Payment Type");
        Cursor cpm=db.payment_details();
        if(cpm.getCount() != 0) {
            while (cpm.moveToNext()) {
                users.add(""+cpm.getString(2));
            }
        }
        users1.add(0, "Select Point Of Contact");
        users2.add("0");

        Cursor cpc=db.point_contact_details();
        if(cpc.getCount() != 0) {
            while (cpc.moveToNext()) {
                users2.add(""+cpc.getString(1));
                users1.add(""+cpc.getString(2));
            }
        }

        ArrayAdapter adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, users);
        sp_payment.setAdapter(adapter);

        if (!str_set_payment.equals("")) {
            int spinnerPosition = adapter.getPosition(str_set_payment);
            sp_payment.setSelection(spinnerPosition);
        }

        ArrayAdapter adapter1 = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, users1);
        sppoint_of_contact.setAdapter(adapter1);

        if (!str_set_pointofsale.equals("")) {
            int spinnerPosition = adapter1.getPosition(str_set_pointofsale);
            sppoint_of_contact.setSelection(spinnerPosition);
        }

        payment_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customMessageBox1();
            }
        });


        Cursor c =db.getCustomerDetails();
        if(c.getCount() == 0) {
            Message.message(getContext(),"Nothing found");
        }else {
            while (c.moveToNext()) {
                IdList.add(c.getString(0));
                NameList.add(c.getString(2));
            }
        }

        ArrayAdapter<String> adapter2=new ArrayAdapter<String>(getContext(),android.R.layout.select_dialog_item,NameList);
        at_customer_name.setThreshold(1);
        at_customer_name.setAdapter(adapter2);

        if(!Name.equals("")){
            at_customer_name.setText(Name);
        }

        at_customer_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Name=at_customer_name.getText().toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
                Name=at_customer_name.getText().toString();
            }
        });


        btn_save_cashcredit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Name=Name.trim();

                if(!Name.matches("[a-zA-Z ]*") || Name.equals("")){
                    at_customer_name.setError("Enter Characters only");
                }
                else {

                    if(sp_payment.getSelectedItem().equals("Select Payment Type")){
                        str_set_payment="Cash";
                    }else{
                        str_set_payment = "" + sp_payment.getSelectedItem();
                    }

                    if(sppoint_of_contact.getSelectedItem().equals("Select Point Of Contact")){
                        str_set_pointofsale="Counter";
                        payment_mode_id = users2.get(0);
                    }else{
                        str_set_pointofsale="" +sppoint_of_contact.getSelectedItem();
                        int point_of_saleid= (int) sppoint_of_contact.getSelectedItemId();
                        payment_mode_id = users2.get(point_of_saleid);
                    }

                    str_set_cust_name=Name;

                    if(NameList.contains(Name)){
                        int name_index=NameList.indexOf(Name);
                        Toast.makeText(getActivity(),""+name_index,Toast.LENGTH_SHORT).show();
                        str_set_cust_name=Name;
                        dialog.dismiss();
                    }else {
                        //Toast.makeText(getApplicationContext(),"New Customer added!!!",Toast.LENGTH_SHORT).show();
                        str_set_cust_name=Name;
                        //db.AddCutomer("",str_set_cust_name,"","","","");
                        dialog.dismiss();
                    }
                }
            }
        });

        dialog.setCancelable(true);
        dialog.show();
    }

    private void customMessageBox1() {
        Button btn_save_cashcredit;
        final Dialog dialog1=new Dialog(Objects.requireNonNull(getContext()));
        dialog1.setContentView(R.layout.layout_payment_details);
        dialog1.setTitle("Payment Details");
        dialog1.setTitle("Payment Details");

        List<String> users = new ArrayList<>();
        final Spinner stransaction= dialog1.findViewById(R.id.TransactionStatus);

        users.add(0, "Transaction status");
        users.add(1, "Success");
        users.add(2, "Fail");
        users.add(3, "Processing");

        ArrayAdapter adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, users);
        stransaction.setAdapter(adapter);
        btn_save_cashcredit= dialog1.findViewById(R.id.btn_save_cashcredit1);
        final AutoCompleteTextView tid= dialog1.findViewById(R.id.TransactionID);
        final AutoCompleteTextView tdetails= dialog1.findViewById(R.id.TransactionDetails);
        AutoCompleteTextView todetails= dialog1.findViewById(R.id.OrderDetails);
        btn_save_cashcredit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    JSONArray array = new JSONArray();
                    JSONObject obj = new JSONObject();
                    obj.put("t_id", tid.getText().toString());
                    obj.put("t_details", tdetails.getText().toString());
                    obj.put("t_status", stransaction.getSelectedItem());
                    array.put(obj);
                    payment_deatils=""+array;

                    JSONArray array1 = new JSONArray();
                    JSONObject obj1 = new JSONObject();
                    obj1.put("o_details", tdetails.getText().toString());
                    array1.put(obj1);
                    order_details=""+array1;
                    dialog1.dismiss();

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        dialog1.setCancelable(true);
        dialog1.show();

    }

    private void addDBGSTEnable(){
        String cid= manager.getCid();
        String lid=manager.getLid();
        String empid=manager.getEmpId();
        emp_code= empcode+tvbill.getText().toString();
        taxlistupdated.clear();
        Double totalPrice = 0.0;//Double.parseDouble(tv_total_amt.getText().toString());
        if (checkArray != null && checkArray.size() != 0) {
            for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                // totalPrice = Double.parseDouble(tv_total_amt.getText().toString());
                for (int j = 0; j < checkArray.size(); j++) {
                    if (checkArray.get(j).equals(ModelSale.arry_item_name.get(i))) {
                        Double value = ModelSale.arr_item_price.get(i);
                        totalPrice = totalPrice + value;
                    }
                }
            }
            totalPrice = Double.parseDouble(tv_total_amt.getText().toString()) - totalPrice;
            for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                if (checkArray.contains(ModelSale.arry_item_name.get(i))) {
                    db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                            ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                            Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                            0.0, totalPrice, cid, lid,
                            empid, "" + Integer.parseInt(tvbill.getText().toString()),
                            payment_mode_id, payment_deatils, order_details, gst_option, emp_code,
                            ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                            Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                    taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                    db.Update_inventory(ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i).toString());
                } else {
                    db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                            ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                            Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                            Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                            totalPrice, cid, lid,
                            empid, "" + Integer.parseInt(tvbill.getText().toString()),
                            payment_mode_id, payment_deatils, order_details, gst_option, emp_code,
                            ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                            Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                    taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                    db.Update_inventory(ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i).toString());

                }
            }
        }
        else {
            for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                        ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                        Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                        Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                        Double.valueOf(tv_total_amt.getText().toString()), cid, lid,
                        empid, "" + Integer.parseInt(tvbill.getText().toString()),
                        payment_mode_id, payment_deatils, order_details, gst_option, emp_code,
                        ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                        Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                db.Update_inventory(ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i).toString());
            }
        }

    }

    private void addDBGSTDisable(){
        String cid= manager.getCid();
        String lid=manager.getLid();
        String empid=manager.getEmpId();
        emp_code= empcode+tvbill.getText().toString();

        taxlistupdated.clear();
        Double totalPrice = 0.0;
        if (checkArray != null && checkArray.size() != 0) {
            for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                // totalPrice = Double.parseDouble(tv_total_amt.getText().toString());
                for (int j = 0; j < checkArray.size(); j++) {
                    if (checkArray.get(j).equals(ModelSale.arry_item_name.get(i))) {
                        Double value = ModelSale.arr_item_price.get(i);
                        totalPrice = totalPrice + value;
                    }
                }
            }
            totalPrice = Double.parseDouble(tv_total_amt.getText().toString()) - totalPrice;
            for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                if (checkArray.contains(ModelSale.arry_item_name.get(i))) {
                    db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                            ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                            Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                            0.0, totalPrice, cid, lid,
                            empid, "" + Integer.parseInt(tvbill.getText().toString()), payment_mode_id,
                            payment_deatils, order_details, gst_option, emp_code, ModelSale.arr_item_dis.get(i),
                            ModelSale.arr_item_tax.get(i), Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                    taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                    db.Update_inventory(ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i).toString());
                } else {
                    db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                            ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                            Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                            Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                            totalPrice, cid, lid,
                            empid, "" + Integer.parseInt(tvbill.getText().toString()), payment_mode_id,
                            payment_deatils, order_details, gst_option, emp_code, ModelSale.arr_item_dis.get(i),
                            ModelSale.arr_item_tax.get(i), Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                    taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                    db.Update_inventory(ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i).toString());
                }
            }
        }
        else {
            for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                Log.v("TAG",tvbill.getText().toString());
                db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                        ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                        Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                        Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                        Double.valueOf(tv_total_amt.getText().toString()), cid, lid,
                        empid, "" + Integer.parseInt(tvbill.getText().toString()), payment_mode_id,
                        payment_deatils, order_details, gst_option, emp_code, ModelSale.arr_item_dis.get(i),
                        ModelSale.arr_item_tax.get(i), Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                db.Update_inventory(ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i).toString());
            }
        }
    }

    private void onPrintBill() {
        String cid= manager.getCid();
        String lid=manager.getLid();
        String empid=manager.getEmpId();
        emp_code= empcode+tvbill.getText().toString();

        if (print_option.equals("3 inch")) {
            if (gst_option.equals("GST Disable") && ModelSale.arry_item_name.size() != 0) {
                if (billwithprint.equals("No") && multipleprint.equals("No")) {
                    addDBGSTDisable();
                    clr_all_mtd();
                    ids = db.getAllbillID();
                    autoinnc();

                    Message.message(getContext(), "Bill saved");
                }
                else if (billwithprint.equals("Yes") && multipleprint.equals("No")) {
                    addDBGSTDisable();
                    if (ModelSale.arry_item_name.size() == 0) {
                        Message.message(getContext(), "Please select items");
                    } else {
                        threeInchPrintGSTDisable();
                    }
                }
                else if (billwithprint.equals("Yes") && multipleprint.equals("Yes")) {
                    if (counter <= 1) {
                       addDBGSTDisable();
                    }
                    if (counter == 1) {
                        counter++;
                        if (ModelSale.arry_item_name.size() == 0) {
                            Message.message(getContext(), "Please select items");
                        } else {
                            threeInchPrintGSTDisable();
                        }
                    }
                    if (counter >= 2) {
                        if (ModelSale.arry_item_name.size() == 0) {
                            Message.message(getContext(), "Please select items");
                        } else {
                            RemoveMessageBox("gst_data_blueprints_three_inch_gst_disable", "Reprinting", "Do you want bill again ?", tvbill.getText().toString() + "?");
                        }
                    }
                }
            }
            if (gst_option.equals("GST Enable") && ModelSale.arry_item_name.size() != 0) {
                if (billwithprint.equals("No") && multipleprint.equals("No")) {
                    addDBGSTEnable();

                    clr_all_mtd();
                    ids = db.getAllbillID();
                    autoinnc();

                    Message.message(getContext(), "Bill saved");
                }
                else if (billwithprint.equals("Yes") && multipleprint.equals("No")) {
                    addDBGSTEnable();
                    if (ModelSale.arry_item_name.size() == 0) {
                        Message.message(getContext(), "Please select items");
                    } else {
                        threeInchPrintGSTEnable();
                    }
                }
                else if (billwithprint.equals("Yes") && multipleprint.equals("Yes")) {
                    if (counter <= 1) {
                      addDBGSTEnable();
                    }
                    if (counter == 1) {
                        counter++;
                        if (ModelSale.arry_item_name.size() == 0) {
                            Message.message(getContext(), "Please select items");
                        } else {
                            threeInchPrintGSTEnable();
                        }
                    }
                    if (counter >= 2) {
                        if (ModelSale.arry_item_name.size() == 0) {
                            Message.message(getContext(), "Please select items");
                        } else {
                            RemoveMessageBox("gst_data_blueprints_three_inch_gst_enable", "Reprinting", "Do you want bill again ?", tvbill.getText().toString() + "?");
                        }
                    }
                }
            }
        }
        if (print_option.equals("2 inch")) {
            if (gst_option.equals("GST Disable") && ModelSale.arry_item_name.size() != 0) {


                if(billwithprint.equals("No")){

                    addDBGSTDisable();

                    clr_all_mtd();
                    ids = db.getAllbillID();
                    autoinnc();
                    Message.message(getContext(), "Bill saved");
                } else if (billwithprint.equals("Yes") && multipleprint.equals("No")) {
                   addDBGSTDisable();
                    if (ModelSale.arry_item_name.size() == 0) {
                        Message.message(getContext(), "Please select items");
                    } else {
                        twoInchPrintGSTDisable();
                    }
                } else if (billwithprint.equals("Yes") && multipleprint.equals("Yes")) {
                    if (counter <= 1) {
                      addDBGSTDisable();
                    }
                    if (counter == 1) {
                        counter++;
                        if (ModelSale.arry_item_name.size() == 0) {
                            Message.message(getContext(), "Please select items");
                        } else {
                            twoInchPrintGSTDisable();
                        }
                    }
                    if (counter >= 2) {
                        if (ModelSale.arry_item_name.size() == 0) {
                            Message.message(getContext(), "Please select items");
                        } else {
                            RemoveMessageBox("gst_data_blueprints_two_inch_gst_disable", "Reprinting", "Do you want bill again ?", tvbill.getText().toString() + "?");
                        }
                    }
                }
            }
            if (gst_option.equals("GST Enable") && ModelSale.arry_item_name.size() != 0) {
                if (billwithprint.equals("No") && multipleprint.equals("No")) {
                   addDBGSTEnable();

                    clr_all_mtd();
                    ids = db.getAllbillID();
                    autoinnc();

                    Message.message(getContext(), "Bill saved");
                }
                else if (billwithprint.equals("Yes") && multipleprint.equals("No")) {
                  addDBGSTEnable();
                    if (ModelSale.arry_item_name.size() == 0) {
                        Message.message(getContext(), "Please select items");
                    } else {
                        twoInchPrintGSTEnable();
                    }
                }
                else if (billwithprint.equals("Yes") && multipleprint.equals("Yes")) {
                    Log.v("TAG", "GST ENABLE AIR BILL");
                    if (counter <= 1) {
                       addDBGSTEnable();
                    }
                    if (counter == 1) {
                        counter++;
                        if (ModelSale.arry_item_name.size() == 0) {
                            Message.message(getContext(), "Please select items");
                        } else {
                            twoInchPrintGSTEnable();
                        }
                    }
                    if (counter >= 2) {
                        if (ModelSale.arry_item_name.size() == 0) {
                            Message.message(getContext(), "Please select items");
                        } else {
                            RemoveMessageBox("gst_data_blueprints_two_inch_gst_enable", "Reprinting", "Do you want bill again ?", tvbill.getText().toString() + "?");
                        }
                    }
                }
            }
        }
    }

    private void threeInchPrintGSTEnable() {
            Cursor res1 = db.getAllHeaderFooter();
            String h1 = null, h1size = "10", h2 = null, h2size = "9", h3 = null, h3size = "8", h4 = null, h4size = "7", h5 = null, h5size = "6", f1 = null, f1size = "10", f2 = null, f2size = "10", f3 = null, f3size = "10", f4 = null, f4size = "10", f5 = null, f5size = "10";
            while (res1.moveToNext()) {
                h1 = res1.getString(1);
                h1size = res1.getString(2);
                h2 = res1.getString(3);
                h2size = res1.getString(4);
                h3 = res1.getString(5);
                h3size = res1.getString(6);
                h4 = res1.getString(7);
                h4size = res1.getString(8);
                h5 = res1.getString(9);
                h5size = res1.getString(10);

                f1 = res1.getString(11);
                f1size = res1.getString(12);
                f2 = res1.getString(13);
                f2size = res1.getString(14);
                f3 = res1.getString(15);
                f3size = res1.getString(16);
                f4 = res1.getString(17);
                f4size = res1.getString(18);
                f5 = res1.getString(19);
                f5size = res1.getString(20);
            }

            Date today = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
            String dateToStr = format.format(today);
            String data = null;
            data = "                 " + "TechMart Cafe" + "             \n";
            String d = "________________________________\n";
            try {
                if (h1 == null || h1.length() == 0 || h1 == "") {
                    h1 = null;
                } else {
                    if (h1.length() < h2.length()) {
                        data = "                " + h1 + "\n";
                    } else {
                        data = "               " + h1 + "\n";
                    }
                    try {
                        printClass.IntentPrint(data);
                    } catch (Exception ex) {
                        Message.message(getContext(), "Printer not connected please connect from main settings");
                    }
                }
                if (h2 == null || h2.length() == 0 || h2 == "") {
                    h2 = null;
                } else {
                    if (h2.length() > 16) {
                        data = "           " + h2 + "\n";
                    } else {
                        data = "                 " + h2 + "\n";
                    }
                    printClass.IntentPrint(data);
                }
                if (h3 == null || h3.length() == 0 || h3 == "") {
                    h3 = null;
                } else {
                    if (h3.length() > 16) {
                        data = "           " + h3 + "\n";
                    } else {
                        data = "                " + h3 + "\n";
                    }
                    printClass.IntentPrint(data);
                }
                if (h4 == null || h4.length() == 0 || h4 == "") {
                    h4 = null;
                } else {
                    if (h4.length() > 16) {
                        data = "           " + h4 + "\n";
                    } else {
                        data = "                " + h4 + "\n";
                    }
                    printClass.IntentPrint(data);
                }
                if (h5 == null || h5.length() == 0 || h5 == "") {
                    h5 = null;
                } else {
                    if (h4.length() > 16) {
                        data = "          " + h5 + "\n";
                    } else {
                        data = "               " + h5 + "\n";
                    }
                    printClass.IntentPrint(data);
                }

                data = "\n                 BILL No:" + tvbill.getText().toString() + "\n";
                try {
                    printClass.IntentPrint(data);
                } catch (Exception ex) {
                    Message.message(getContext(), "Printer not connected please connect from main settings");
                }
                data = "Date:-" + dateToStr + "\n";
                printClass.IntentPrint(data);
                if (str_set_cust_name == "" || str_set_cust_name == null) {
                    str_set_cust_name = "";
                    data = "\nNo|   Item Name   | Qty | Rate |Dis|Tax| Amt  \n";
                } else {
                    data = "Customer Name:- " + str_set_cust_name + "\n";
                    printClass.IntentPrint(data);
                    data = "\nNo|   Item Name   | Qty | Rate |Dis|Tax| Amt  \n";

                }
                printClass.IntentPrint(data);

                String totalamtrate = "";
                int a = 1;Double totaldiscount=0.0,discal=0.0,discaltotal=0.0;
                StringBuffer buffer = new StringBuffer();

                String totalamtrategst = "";
                int a1 = 1, j = 0;
                Double itemtax = 0.0;
                int[] positions = new int[ModelSale.arry_item_name.size()];
                StringBuffer buffer1 = new StringBuffer();
                Double totalwithgst = 0.0, finalgst = 0.0,subtotal=0.0;
                Double oldtax = 0.0;
                int l = 0;
                Cursor res2 = db.getAllbillWithID(Integer.parseInt(tvbill.getText().toString()));
                while (res2.moveToNext()) {
                    String itemname = res2.getString(3);
                    String largname = "";
                    String qty = res2.getString(4);
                    String rate = res2.getString(5);
                    String discount = res2.getString(7);
                    String tax = res2.getString(8);
                    String Basicrate=res2.getString(9);

                    String amount = res2.getString(6);

                    if (amount.length() > 6) {
                        amount = amount.substring(0, 6);

                    } else {
                        int length = amount.length();
                        int l1 = 0;
                        if (length < 6) {
                            l1 = 6 - length;
                            amount = String.format(amount + "%" + (l1) + "s", "");
                        }
                    }
                    if (qty.length() >=5) {
                        qty = qty.substring(0, 5);
                    } else {
                        int length = qty.length();
                        int l1 = 0;
                        if (length < 5) {
                            l1 = 5 - length;
                            qty = String.format(qty + "%" + (l1) + "s", "");
                        }
                    }
                    if (Basicrate.length() > 5) {
                        Basicrate = Basicrate.substring(0, 5);
                    } else {
                        int length = Basicrate.length();
                        int l1 = 0;
                        if (length < 5) {
                            l1 = 5 - length;
                            Basicrate = String.format(Basicrate + "%" + (l1) + "s", "");
                        }
                    }
                    if (discount.length() > 2) {
                        discount = discount.substring(0, 2);

                    } else {
                        int length = discount.length();
                        int l1 = 0;
                        if (length <2) {
                            l1 = 2 - length;
                            discount = String.format(discount + "%" + (l1) + "s", "");
                        }
                    }
                    if (tax.length() > 2) {
                        tax = tax.substring(0, 2);

                    } else {
                        int length = tax.length();
                        int l1 = 0;
                        if (length < 2) {
                            l1 = 2 - length;
                            tax = String.format(tax + "%" + (l1) + "s", "");
                        }
                    }

                    if (itemname.length() > 15) {
                        if(a>=10) {
                            buffer.append(a + "   " + itemname.substring(0, 15) + "" + qty+"  "+ Basicrate+"   "+ discount +"  "+ tax +" "+ amount + "\n"); //res2.getString(6)+"\n");
                        }
                        else {
                            buffer.append(a + "    " + itemname.substring(0, 15) + "" + qty +"  "+ Basicrate +"   "+ discount +"  "+ tax +" "+ amount + "\n"); //res2.getString(6)+"\n");
                        }
                    }
                    else {
                        int length = itemname.length();
                        int l1 = 0;
                        if (length < 15) {
                            l1 = 15 - length;
                            String itemname1 = itemname.concat(l1 + " ");
                            itemname = String.format(itemname + "%" + (l1) + "s", "");
                        }
                        if(a>=10) {
                            buffer.append(a + "   " + itemname + "" + qty + "  " +Basicrate + "   " + discount + "  " + tax + " " + amount + "\n");//res2.getString(6)+"\n");
                        }
                        else {
                            buffer.append(a+"    "+itemname + "" + qty + "  " + Basicrate +"   "+discount+"  "+tax+ " " + amount + "\n");//res2.getString(6)+"\n");
                        }
                    }

                    a++;
                    discaltotal=discaltotal+Double.parseDouble(Basicrate);
                    discal=discaltotal*(Double.parseDouble(discount)/100);
                    totaldiscount=totaldiscount+discal;
                    discal=0.0;
                    discaltotal=0.0;
                    subtotal=subtotal+Double.parseDouble(Basicrate);
                }
                Double grandgst = 0.0;
                int count1 = 0;
                Double cgst = 0.0, sgst = 0.0;
                List<String> result = new ArrayList<>();

                Double sameitemgst = 0.0, sameitemtotalgst = 0.0;
                List<String> taxlist1 = new ArrayList<>();
                final List<String> itemnamelist = new ArrayList<>();
                List<Double> qtylist = new ArrayList<>();
                List<String> ratelist = new ArrayList<>();
                List<Double> amtlist = new ArrayList<>();
                List<String> indexlist1 = new ArrayList<>();
                List<String> disclist = new ArrayList<>();
                List<String> disclistrate = new ArrayList<>();
                for (int k = 0; k < ModelSale.arry_item_name.size(); k++) {
                    itemtax = Double.parseDouble(taxlistupdated.get(k));
                    Cursor c = db.getdistinct(String.valueOf(itemtax));
                    while (c.moveToNext()) {
                        taxlist1.add(c.getString(0));
                        String stringName = c.getString(0);
                        if(checkArray==null || checkArray.size()==0){
                            Log.v("TAG","NO COMPLEMENT");
                            if (ModelSale.arry_item_name.contains(c.getString(0))) {
                                if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                    indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    itemnamelist.add(c.getString(0));
                                    qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                }
                                if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                } else {
                                    itemnamelist.add(c.getString(0));
                                    indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                }
                            }
                        }
                        else{
                            if (ModelSale.arry_item_name.get(k).equals(stringName)) {
                                Log.v("TAGG",ModelSale.arry_item_name.get(k));
                                if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                    //   for(int q=0;q<checkArray.size();q++) {
                                    if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                        Log.v("TAG","1 IN");
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        //ratelist.add(k,"0.0");
                                        amtlist.add(k, 0.0);
                                        disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        if (counter<=1) {
                                            double current = Double.parseDouble(tv_total_amt.getText().toString());
                                            double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                            total_val = current - total_val;
                                            if(total_val<=0.0){
                                                tv_total_amt.setText("0.0");
                                            }
                                            else {
                                                tv_total_amt.setText("" + String.format("%.02f", total_val));
                                            }
                                            Log.v("TAG", String.valueOf(total_val));
                                        }
                                        Log.v("TAG","1 OUT");
                                    } else {
                                        Log.v("TAG","2 IN");
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        Log.v("TAG","2 OUT");
                                    }
                                }
                                else if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                    if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                        Log.v("TAG","3 IN");
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));

                                        amtlist.add(k, 0.0);
                                        disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        if (counter<=1) {
                                            double current = Double.parseDouble(tv_total_amt.getText().toString());
                                            double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                            total_val = current - total_val;
                                            if(total_val<=0.0){
                                                tv_total_amt.setText("0.0");
                                            }
                                            else {
                                                tv_total_amt.setText("" + String.format("%.02f", total_val));
                                            }
                                            Log.v("TAG", String.valueOf(total_val));
                                        }
                                        Log.v("TAG","3 OUT");
                                    } else {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        Log.v("TAG","4 OUT");
                                    }
                                }
                                else {
                                    if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                        Log.v("TAG","4 IN");
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, 0.0);
                                        disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        if (counter<=1) {
                                            double current = Double.parseDouble(tv_total_amt.getText().toString());
                                            double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                            total_val = current - total_val;
                                            if(total_val<=0.0){
                                                tv_total_amt.setText("0.0");
                                            }
                                            else {
                                                tv_total_amt.setText("" + String.format("%.02f", total_val));
                                            }
                                            Log.v("TAG", String.valueOf(total_val));
                                        }
                                        Log.v("TAG","4 OUT");
                                    } else {
                                        Log.v("TAGG", "CHECKIN4");
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        Log.v("TAGG", "CHECKOUT4");
                                    }
                                }
                            }
                        }
                    }
                }
                taxlist1.clear();
                for (int k = 0; k < itemnamelist.size(); k++) {
                    DecimalFormat formats = new DecimalFormat("#####.00");
                    taxlist1.clear();
                    int count = itemnamelist.size();
                    String itemname = itemnamelist.get(k);
                    String largname = "";
                    Double qty = qtylist.get(k);
                    String rate=disclistrate.get(k);
                    String amount = String.valueOf(qty*Double.parseDouble(rate));
                    itemtax = Double.valueOf(indexlist1.get(k));
                    Cursor c = db.getdistinct(String.valueOf(itemtax));
                    while (c.moveToNext()) {
                        taxlist1.add(c.getString(0));
                    }
                    cgst = (itemtax / 2);
                    sgst = cgst;
                    if (amount.length() > 4) {
                        amount = amount.substring(0, 4);
                    } else {
                        int length = amount.length();
                        int l1 = 0;
                        if (length < 4) {
                            l1 = 4 - length;
                            amount = String.format(amount + "%" + (l1) + "s", "");
                        }
                    }

                    List<String> list = new ArrayList<>();
                    for (String aObject : taxlist1) {
                        if (itemnamelist.contains(aObject)) {
                            list.add(aObject);
                        }
                    }
                    result = list;

                    i("dataname", String.valueOf(result));
                    int resultcount = result.size();

                    if (itemname.length() > 15) {


                        if (rate.length() == 3) {

                            count1++;
                            if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                int p = result.indexOf(itemnamelist.get(k));
                                int f = taxlist1.size();
                                int h = itemnamelist.size();
                                if (p == (result.size() - 1)) {
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");

                                } else if (result.size() == 1) {
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                } else {
                                    if (g == f || count == count1) {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                    } else {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    }
                                }
                            } else {
                                totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                grandgst = grandgst + totalwithgst;
                                buffer1.append("    " + itemtax + "%       " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "       " + formats.format(totalwithgst) + "\n");
                            }
                        } else {
                            if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                int p = result.indexOf(itemnamelist.get(k));
                                int f = taxlist1.size();
                                int h = itemnamelist.size();
                                if (p == (result.size() - 1)) {
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                } else if (result.size() == 1) {
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                } else {
                                    if (g == f || count == count1) {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                    } else {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    }
                                }
                            } else {
                                totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                buffer1.append("    " + itemtax + "%       " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "       " + formats.format(totalwithgst) + "\n");
                                grandgst = grandgst + totalwithgst;
                            }
                        }
                    } else {
                        int length = itemname.length();
                        int l1 = 0;
                        if (length < 15) {
                            l1 = 15 - length;
                            String itemname1 = itemname.concat(l1 + " ");
                            itemname = String.format(itemname + "%" + (l1) + "s", "");
                        }
                        if (rate.length() == 3) {
                            count1++;

                            if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                int p = result.indexOf(itemnamelist.get(k));
                                int f = taxlist1.size();
                                int h = itemnamelist.size();
                                if (p == (result.size() - 1)) {
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                } else if (result.size() == 1) {
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                } else {
                                    if (g == f || count == count1) {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                    } else {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    }
                                }
                            } else {
                                totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                buffer1.append("    " + itemtax + "%       " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "       " + formats.format(totalwithgst) + "\n");
                                grandgst = grandgst + totalwithgst;
                            }
                        } else {
                            count1++;
                            if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                int p = result.indexOf(itemnamelist.get(k));
                                int f = taxlist1.size();
                                int h = itemnamelist.size();
                                if (p == (result.size() - 1)) {
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                } else if (result.size() == 1) {
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                } else {
                                    if (g == f || count == count1) {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                    } else {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    }
                                }
                            } else {
                                totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                buffer1.append("    " + itemtax + "%       " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "       " + formats.format(totalwithgst) + "\n");
                                grandgst = grandgst + totalwithgst;
                            }
                        }
                    }
                    j++;
                    finalgst = totalwithgst + finalgst;
                    totalwithgst = 0.0;
                    cgst = 0.0;
                    sgst = 0.0;
                }
                printClass.IntentPrint(String.valueOf(buffer));

                double ddata = Double.parseDouble(tv_total_amt.getText().toString());
                DecimalFormat f = new DecimalFormat("#####.00");
                d = "________________________________________________\n";
                data = "                       Sub TOTAL(Rs): " + String.format("%.02f",subtotal) + "\n";
                printClass.IntentPrint(d);

                printClass.IntentPrint(data);
                if(totaldiscount==0.0){}else {
                    data = "                      -Dis(Rs)      : " + String.format("%.02f",totaldiscount) + "\n";
                    d = "                     __________________________";
                    printClass.IntentPrint(data);
                    printClass.IntentPrint(d);
                    data = "                                      " + String.format("%.02f",(subtotal - totaldiscount));
                    printClass.IntentPrint(data);
                }if(finalgst==0.0){}else{
                    data = "\n                      +GST(Rs)      : " + String.format("%.02f",finalgst) + "\n";
                    printClass.IntentPrint(data);
                    printClass.IntentPrint(d);
                }
                data = "                       Net Total(Rs): " + String.format("%.02f",ddata) + "\n";


                printClass.IntentPrint(data);
                if (f1 == null || f1.length() == 0 || f1 == "") {
                    f1 = null;
                } else {

                    data = "           " + f1 + "\n";
                    printClass.IntentPrint(data);

                }
                if (f2 == null || f2.length() == 0 || f2 == "") {
                    f2 = null;
                } else {
                    data = "           " + f2 + "\n";
                    printClass.IntentPrint(data);
                }
                if (f3 == null || f3.length() == 0 || f3 == "") {
                    f3 = null;
                } else {

                    data = "            " + f3 + "\n";
                    printClass.IntentPrint(data);
                }
                if (f4 == null || f4.length() == 0 || f4 == "") {
                    f4 = null;
                } else {
                    data = "           " + f4 + "\n";
                    printClass.IntentPrint(data);
                }
                if (f5 == null || f5.length() == 0 || f5 == "") {
                    f5 = null;
                } else {
                    data = "           " + f5 + "\n";
                    printClass.IntentPrint(data);
                }


                printClass.IntentPrint("\n\n\n\n");

                clr_all_mtd();
                ids = db.getAllbillID();
                autoinnc();

                Message.message(getContext(), "Bill saved");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    private void twoInchPrintGSTDisable() {
            Cursor res2 = db.getAllbillWithID(Integer.parseInt(tvbill.getText().toString()));
            Cursor res1 = db.getAllHeaderFooter();
            String h1 = null, h1size = "10", h2 = null, h2size = "9", h3 = null, h3size = "8", h4 = null, h4size = "7", h5 = null, h5size = "6", f1 = null, f1size = "10", f2 = null, f2size = "10", f3 = null, f3size = "10", f4 = null, f4size = "10", f5 = null, f5size = "10";
            while (res1.moveToNext()) {
                h1 = res1.getString(1);
                h1size = res1.getString(2);
                h2 = res1.getString(3);
                h2size = res1.getString(4);
                h3 = res1.getString(5);
                h3size = res1.getString(6);
                h4 = res1.getString(7);
                h4size = res1.getString(8);
                h5 = res1.getString(9);
                h5size = res1.getString(10);
                f1 = res1.getString(11);
                f1size = res1.getString(12);
                f2 = res1.getString(13);
                f2size = res1.getString(14);
                f3 = res1.getString(15);
                f3size = res1.getString(16);
                f4 = res1.getString(17);
                f4size = res1.getString(18);
                f5 = res1.getString(19);
                f5size = res1.getString(20);
            }

            try {
                Date today = new Date();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                String dateToStr = format.format(today);
                String data = null;
                data = "                 " + "TechMart Cafe" + "             \n";
                String d = "________________________________\n";
                if (h1 == null || h1.length() == 0 || h1 == "") {
                    h1 = null;
                } else {
                    if (h1.length() < h2.length()) {
                        data = "          " + h1 + "\n";
                    } else {
                        data = "         " + h1 + "\n";
                    }
                    try {
                        printClass.IntentPrint(data);
                    }catch (Exception ex) {
                        Message.message(getContext(),"Printer not connected please connect from main settings");
                    }
                }

                if (h2 == null || h2.length() == 0 || h2 == "") {
                    h2 = null;
                } else {
                    if (h2.length() > 16) {
                        data = "     " + h2 + "\n";
                    } else {
                        data = "           " + h2 + "\n";
                    }
                    printClass.IntentPrint(data);
                }
                if (h3 == null || h3.length() == 0 || h3 == "") {
                    h3 = null;
                } else {
                    if (h3.length() > 16) {
                        data = "     " + h3 + "\n";
                    } else {
                        data = "           " + h3 + "\n";
                    }
                    printClass.IntentPrint(data);
                }
                if (h4 == null || h4.length() == 0 || h4 == "") {
                    h4 = null;
                } else {
                    if (h4.length() > 16) {
                        data = "     " + h4 + "\n";
                    } else {
                        data = "           " + h4 + "\n";
                    }
                    printClass.IntentPrint(data);
                }
                if (h5 == null || h5.length() == 0 || h5 == "") {
                    h5 = null;
                } else {
                    if (h4.length() > 16) {
                        data = "     " + h5 + "\n";
                    } else {
                        data = "           " + h5 + "\n";
                    }
                    printClass.IntentPrint(data);
                }

                data = "\nCash Memo    bill no:" + emp_code + "\n";
                try {
                    printClass.IntentPrint(data);
                }catch (Exception ex) {
                    Message.message(getContext(),"Printer not connected please connect from main settings");
                }
                data = "Date:-" + dateToStr + "\n";
                printClass.IntentPrint(data);
                if (str_set_cust_name == "" || str_set_cust_name == null) {
                    str_set_cust_name = "";
                    data = "\nItem Name         |Qty|Rate|Amt\n";
                } else {
                    data = "Customer Name:- " + str_set_cust_name + "\n";
                    printClass.IntentPrint(data);
                    data = "Item Name        |Qty|Rate|Amt\n";
                }
                printClass.IntentPrint(data);
                String totalamtrate = "";
                int a = 1;
                StringBuffer buffer = new StringBuffer();
                while (res2.moveToNext()) {
                    String itemname = res2.getString(3);
                    String largname = "";
                    String qty = res2.getString(4);
                    String rate = res2.getString(5);
                    String amount = res2.getString(6);
                    if (amount.length() > 4) {
                        amount = amount.substring(0, 4);

                    } else {
                        int length = amount.length();
                        int l1 = 0;
                        if (length < 4) {
                            l1 = 4 - length;
                            amount = String.format(amount + "%" + (l1) + "s", "");
                        }
                    }
                    if (itemname.length() > 15) {
                        if (res2.getString(5).length() >= 3) {
                            buffer.append(itemname.substring(0, 15) + " " + res2.getString(4) + " " + res2.getString(5) + " " + amount + "\n"); //res2.getString(6)+"\n");
                        } else {
                            buffer.append(itemname.substring(0, 15) + " " + res2.getString(4) + "  " + res2.getString(5) + "  " + amount + "\n"); //res2.getString(6)+"\n");
                        }
                    } else {
                        int length = itemname.length();
                        int l1 = 0;
                        if (length < 15) {
                            l1 = 15 - length;
                            String itemname1 = itemname.concat(l1 + " ");
                            itemname = String.format(itemname + "%" + (l1) + "s", "");
                        }
                        if (res2.getString(5).length() >= 3) {
                            buffer.append(itemname.substring(0, 15) + " " + res2.getString(4) + "  " + res2.getString(5) + " " + amount + "\n");//res2.getString(6)+"\n");
                        } else {
                            buffer.append(itemname.substring(0, 15) + " " + res2.getString(4) + "  " + res2.getString(5) + "  " + amount + "\n");//res2.getString(6)+"\n");
                        }
                    }
                }
                String totalamtrategst = "";
                int a1 = 1, j = 0;
                Double itemtax = 0.0;
                int[] positions = new int[ModelSale.arry_item_name.size()];
                StringBuffer buffer1 = new StringBuffer();
                Double totalwithgst = 0.0, finalgst = 0.0;
                Double oldtax = 0.0;
                int l = 0;
                DecimalFormat decimalFormat = new DecimalFormat("#####.00");

                Double grandgst = 0.0;
                int count1 = 0;
                Double cgst = 0.0, sgst = 0.0;

                List<String> result = new ArrayList<>();
                Double sameitemgst = 0.0, sameitemtotalgst = 0.0;
                List<String> taxlist1 = new ArrayList<>();
                final List<String> itemnamelist = new ArrayList<>();
                List<Double> qtylist = new ArrayList<>();
                List<String> ratelist = new ArrayList<>();
                List<Double> amtlist = new ArrayList<>();
                List<String> indexlist1 = new ArrayList<>();

                for (int k = 0; k < ModelSale.arry_item_name.size(); k++) {
                    itemtax = Double.parseDouble(taxlistupdated.get(k));
                    Cursor c = db.getdistinct(String.valueOf(itemtax));
                    while (c.moveToNext()) {
                        taxlist1.add(c.getString(0));
                        String stringName = c.getString(0);
                        if(checkArray==null || checkArray.size()==0){
                            if (ModelSale.arry_item_name.contains(c.getString(0))) {
                                if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                    indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    itemnamelist.add(c.getString(0));
                                    qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                }
                                if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                } else {
                                    itemnamelist.add(c.getString(0));
                                    indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                }
                            }
                        }
                        else {
                            if (ModelSale.arry_item_name.get(k).equals(stringName)) {
                                Log.v("TAGG", ModelSale.arry_item_name.get(k));
                                if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                    //   for(int q=0;q<checkArray.size();q++) {
                                    if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, 0.0);
//                                        if (counter<=1) {
                                        double current = Double.parseDouble(tv_total_amt.getText().toString());
                                        double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                        total_val = current - total_val;
                                        if(total_val<=0.0){
                                            tv_total_amt.setText("0.0");
                                        }
                                        else {
                                            tv_total_amt.setText("" + String.format("%.02f", total_val));
                                        }
                                        Log.v("TAG", String.valueOf(total_val));
//                                        }
                                        //checkArray.remove(q);
                                        // break;
                                        //Log.v("TAGG", "CHECKOUT");
                                    } else {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    }
                                }
                                else if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                    if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, 0.0);
//                                        if (counter<=1) {
                                        double current = Double.parseDouble(tv_total_amt.getText().toString());
                                        double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                        total_val = current - total_val;
                                        if(total_val<=0.0){
                                            tv_total_amt.setText("0.0");
                                        }
                                        else {
                                            tv_total_amt.setText("" + String.format("%.02f", total_val));
                                        }
                                        Log.v("TAG", String.valueOf(total_val));
//                                        }
                                    } else {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    }
                                }
                                else {

                                    if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, 0.0);
//                                        if (counter<=1) {
                                        double current = Double.parseDouble(tv_total_amt.getText().toString());
                                        double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                        total_val = current - total_val;
                                        if(total_val<=0.0){
                                            tv_total_amt.setText("0.0");
                                        }
                                        else {
                                            tv_total_amt.setText("" + String.format("%.02f", total_val));
                                        }
                                        Log.v("TAG", String.valueOf(total_val));
//                                        }
                                    } else {
                                        Log.v("TAGG", "CHECKIN4");
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        Log.v("TAGG", "CHECKOUT4");
                                    }
                                }
                            }
                        }
                    }
                }
                taxlist1.clear();
                for (int k = 0; k < itemnamelist.size(); k++) {
                    DecimalFormat formats = new DecimalFormat("#####.00");
                    taxlist1.clear();
                    int count = itemnamelist.size();
                    String itemname = itemnamelist.get(k);
                    String largname = "";
                    Double qty = qtylist.get(k);
                    String rate = ratelist.get(k);
                    String amount = String.valueOf(qty*Double.parseDouble(rate));
                    itemtax = Double.valueOf(indexlist1.get(k));
                    Cursor c = db.getdistinct(String.valueOf(itemtax));
                    while (c.moveToNext()) {
                        taxlist1.add(c.getString(0));
                    }
                    cgst = (itemtax / 2);
                    sgst = cgst;
                    if (amount.length() > 4) {
                        amount = amount.substring(0, 4);
                    } else {
                        int length = amount.length();
                        int l1 = 0;
                        if (length < 4) {
                            l1 = 4 - length;
                            amount = String.format(amount + "%" + (l1) + "s", "");
                        }
                    }

                    List<String> list = new ArrayList<>();
                    for (String aObject : taxlist1) {
                        if (itemnamelist.contains(aObject)) {
                            list.add(aObject);
                        }
                    }
                    result = list;

                    i("dataname", String.valueOf(result));
                    int resultcount = result.size();
                    if (itemname.length() > 15) {
                        if (rate.length() == 3) {
                            count1++;
                            if(itemtax==0.0){}else {
                                if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                    int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                    int p = result.indexOf(itemnamelist.get(k));
                                    int f = taxlist1.size();
                                    int h = itemnamelist.size();
                                    if (p == (result.size() - 1)) {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                    } else if (result.size() == 1) {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                    } else {
                                        if (g == f || count == count1) {
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                        } else {
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        }
                                    }
                                } else {
                                    totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                    grandgst = grandgst + totalwithgst;
                                    buffer1.append("  " + itemtax + "%    " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "     " + formats.format(totalwithgst) + "\n");
                                }
                            }
                        } else {
                            if(itemtax==0.0){}
                            else {
                                if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                    int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                    int p = result.indexOf(itemnamelist.get(k));
                                    int f = taxlist1.size();
                                    int h = itemnamelist.size();
                                    if (p == (result.size() - 1)) {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                    } else if (result.size() == 1) {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                    } else {
                                        if (g == f || count == count1) {
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                        } else {
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        }
                                    }
                                } else {
                                    totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                    buffer1.append("  " + itemtax + "%    " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "     " + formats.format(totalwithgst) + "\n");
                                    grandgst = grandgst + totalwithgst;
                                }
                            }
                        }
                    } else {
                        int length = itemname.length();
                        int l1 = 0;
                        if (length < 15) {
                            l1 = 15 - length;
                            String itemname1 = itemname.concat(l1 + " ");
                            itemname = String.format(itemname + "%" + (l1) + "s", "");
                        }
                        if (rate.length() == 3) {
                            count1++;
                            if(itemtax==0.0){}
                            else {
                                if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                    int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                    int p = result.indexOf(itemnamelist.get(k));
                                    int f = taxlist1.size();
                                    int h = itemnamelist.size();
                                    if (p == (result.size() - 1)) {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                    } else if (result.size() == 1) {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                    } else {
                                        if (g == f || count == count1) {
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                        } else {
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        }
                                    }
                                } else {
                                    totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                    buffer1.append("  " + itemtax + "%    " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "     " + formats.format(totalwithgst) + "\n");
                                    grandgst = grandgst + totalwithgst;
                                }
                            }
                        } else {
                            count1++;
                            if(itemtax==0.0){}
                            else {
                                if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                    int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                    int p = result.indexOf(itemnamelist.get(k));
                                    int f = taxlist1.size();
                                    int h = itemnamelist.size();
                                    if (p == (result.size() - 1)) {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                    } else if (result.size() == 1) {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                    } else {
                                        if (g == f || count == count1) {
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                        } else {
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        }
                                    }
                                } else {
                                    totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                    buffer1.append("  " + itemtax + "%    " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "     " + formats.format(totalwithgst) + "\n");
                                    grandgst = grandgst + totalwithgst;
                                }
                            }
                        }
                    }
                    j++;
                    finalgst = totalwithgst + finalgst;
                    totalwithgst = 0.0;
                    cgst = 0.0;
                    sgst = 0.0;
                }
                Double totalAmount = Double.parseDouble(tv_total_amt.getText().toString()) + finalgst;
                printClass.IntentPrint(String.valueOf(buffer));

                double ddata = Double.parseDouble(tv_total_amt.getText().toString());
                DecimalFormat f = new DecimalFormat("#####.00");
                if(ddata<=0.0){
                    ddata=0.0;
                }
                else{
                    ddata=ddata;
                }
                data = "              TOTAL(Rs): " + String.format("%.02f",ddata) + "\n";
                printClass.IntentPrint(d);
                printClass.IntentPrint(data);
                if(finalgst==0.0){}
                else {
                    data = "\n          GST Details \n";
                    printClass.IntentPrint(data);
                    data = "  GST%    CGST+SGST     Total \n";
                    printClass.IntentPrint(data);
                    printClass.IntentPrint(String.valueOf(buffer1));
                }
                if (f1 == null || f1.length() == 0 || f1 == "") {
                    f1 = null;
                } else {

                    data = "     " + f1 + "\n";
                    printClass.IntentPrint(data);
                }
                if (f2 == null || f2.length() == 0 || f2 == "") {
                    f2 = null;
                } else {
                    data = "     " + f2 + "\n";
                    printClass.IntentPrint(data);
                }
                if (f3 == null || f3.length() == 0 || f3 == "") {
                    f3 = null;
                } else {

                    data = "     " + f3 + "\n";
                    printClass.IntentPrint(data);
                }
                if (f4 == null || f4.length() == 0 || f4 == "") {
                    f4 = null;
                } else {
                    data = "     " + f4 + "\n";
                    printClass.IntentPrint(data);
                }
                if (f5 == null || f5.length() == 0 || f5 == "") {
                    f5 = null;
                } else {
                    data = "     " + f5 + "\n";
                    printClass.IntentPrint(data);
                }
                printClass.IntentPrint("\n\n\n\n");

                clr_all_mtd();
                ids = db.getAllbillID();
                autoinnc();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        private void threeInchPrintGSTDisable() {
            Cursor res1 = db.getAllHeaderFooter();
            String h1 = null, h1size = "10", h2 = null, h2size = "9", h3 = null, h3size = "8", h4 = null, h4size = "7", h5 = null, h5size = "6", f1 = null, f1size = "10", f2 = null, f2size = "10", f3 = null, f3size = "10", f4 = null, f4size = "10", f5 = null, f5size = "10";
            while (res1.moveToNext()) {
                h1 = res1.getString(1);
                h1size = res1.getString(2);
                h2 = res1.getString(3);
                h2size = res1.getString(4);
                h3 = res1.getString(5);
                h3size = res1.getString(6);
                h4 = res1.getString(7);
                h4size = res1.getString(8);
                h5 = res1.getString(9);
                h5size = res1.getString(10);

                f1 = res1.getString(11);
                f1size = res1.getString(12);
                f2 = res1.getString(13);
                f2size = res1.getString(14);
                f3 = res1.getString(15);
                f3size = res1.getString(16);
                f4 = res1.getString(17);
                f4size = res1.getString(18);
                f5 = res1.getString(19);
                f5size = res1.getString(20);
            }
            try {
                Date today = new Date();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                String dateToStr = format.format(today);
                String data = null;
                data = "                 " + "TechMart Cafe" + "             \n";
                String d = "________________________________\n";

                if (h1 == null || h1.length() == 0 || h1 == "") {
                    h1 = null;
                } else {
                    if (h1.length() < h2.length()) {
                        data = "                 " + h1 + "\n";
                    } else {
                        data = "                " + h1 + "\n";
                    }
                    try {
                        printClass.IntentPrint(data);
                    } catch (Exception ex) {
                        Message.message(getContext(), "Printer not connected please connect from main settings");
                    }
                }
                if (h2 == null || h2.length() == 0 || h2 == "") {
                    h2 = null;
                } else {
                    if (h2.length() > 16) {
                        data = "             " + h2 + "\n";
                    } else {
                        data = "                 " + h2 + "\n";
                    }
                    printClass.IntentPrint(data);
                }
                if (h3 == null || h3.length() == 0 || h3 == "") {
                    h3 = null;
                } else {
                    if (h3.length() > 16) {
                        data = "            " + h3 + "\n";
                    } else {
                        data = "                 " + h3 + "\n";
                    }
                    printClass.IntentPrint(data);
                }
                if (h4 == null || h4.length() == 0 || h4 == "") {
                    h4 = null;
                } else {
                    if (h4.length() > 16) {
                        data = "          " + h4 + "\n";
                    } else {
                        data = "                " + h4 + "\n";
                    }
                    printClass.IntentPrint(data);
                }
                if (h5 == null || h5.length() == 0 || h5 == "") {
                    h5 = null;
                } else {
                    if (h4.length() > 16) {
                        data = "           " + h5 + "\n";
                    } else {
                        data = "                  " + h5 + "\n";
                    }
                    printClass.IntentPrint(data);
                }
                data = "\nCash Memo    BILL No:" + tvbill.getText().toString() + "\n";
                try {
                    printClass.IntentPrint(data);
                } catch (Exception ex) {
                    Message.message(getContext(), "Printer not connected please connect from main settings");
                }
                data = "Date:-" + dateToStr + "\n";
                printClass.IntentPrint(data);
                if (str_set_cust_name == "" || str_set_cust_name == null) {
                    str_set_cust_name = "";
                    data = "\nNo|   Item Name   | Qty | Rate |Dis|Tax| Amt  \n";
                } else {
                    data = "Customer Name:- " + str_set_cust_name + "\n";
                    printClass.IntentPrint(data);
                    data = "\nNo|   Item Name   | Qty | Rate |Dis|Tax| Amt  \n";

                }
                printClass.IntentPrint(data);
                int a = 1;
                Double totaldiscount = 0.0, discal = 0.0, discaltotal = 0.0;
                StringBuffer buffer = new StringBuffer();
                int a1 = 1, j = 0;
                Double itemtax = 0.0;
                int[] positions = new int[ModelSale.arry_item_name.size()];
                StringBuffer buffer1 = new StringBuffer();
                Double totalwithgst = 0.0, finalgst = 0.0, subtotal = 0.0;
                Cursor res2 = db.getAllbillWithID(Integer.parseInt(tvbill.getText().toString()));
                while (res2.moveToNext()) {
                    String itemname = res2.getString(3);
                    String largname = "";
                    String qty = res2.getString(4);
                    String rate = res2.getString(5);
                    String discount = res2.getString(7);
                    String tax = res2.getString(8);
                    String Basicrate = res2.getString(9);

                    String amount = res2.getString(6);

                    if (amount.length() > 6) {
                        amount = amount.substring(0, 6);
                    } else {
                        int length = amount.length();
                        int l1 = 0;
                        if (length < 6) {
                            l1 = 6 - length;
                            amount = String.format(amount + "%" + (l1) + "s", "");
                        }
                    }
                    if (qty.length() > 5) {
                        qty = qty.substring(0, 5);

                    } else {
                        int length = qty.length();
                        int l1 = 0;
                        if (length < 5) {
                            l1 = 5 - length;
                            qty = String.format(qty + "%" + (l1) + "s", "");
                        }
                    }
                    if (Basicrate.length() > 5) {
                        Basicrate = Basicrate.substring(0, 5);

                    } else {
                        int length = Basicrate.length();
                        int l1 = 0;
                        if (length < 5) {
                            l1 = 5 - length;
                            Basicrate = String.format(Basicrate + "%" + (l1) + "s", "");
                        }
                    }
                    if (discount.length() > 2) {
                        discount = discount.substring(0, 2);

                    } else {
                        int length = discount.length();
                        int l1 = 0;
                        if (length < 2) {
                            l1 = 2 - length;
                            discount = String.format(discount + "%" + (l1) + "s", "");
                        }
                    }
                    if (tax.length() > 2) {
                        tax = tax.substring(0, 2);

                    } else {
                        int length = tax.length();
                        int l1 = 0;
                        if (length < 2) {
                            l1 = 2 - length;
                            tax = String.format(tax + "%" + (l1) + "s", "");
                        }
                    }

                    if (itemname.length() > 15) {
                        if (a >= 10) {
                            buffer.append(a + "   " + itemname.substring(0, 15) + "" + qty + "  " + Basicrate + "   " + discount + "  " + tax + " " + amount + "\n"); //res2.getString(6)+"\n");
                        } else {
                            buffer.append(a + "    " + itemname.substring(0, 15) + "" + qty + "  " + Basicrate + "   " + discount + "  " + tax + " " + amount + "\n"); //res2.getString(6)+"\n");
                        }
                    } else {
                        int length = itemname.length();
                        int l1 = 0;
                        if (length < 15) {
                            l1 = 15 - length;
                            String itemname1 = itemname.concat(l1 + " ");
                            itemname = String.format(itemname + "%" + (l1) + "s", "");
                        }
                        if (a >= 10) {
                            buffer.append(a + "   " + itemname + "" + qty + "  " + Basicrate + "   " + discount + "  " + tax + " " + amount + "\n");//res2.getString(6)+"\n");
                        } else {
                            buffer.append(a + "    " + itemname + "" + qty + "  " + Basicrate + "   " + discount + "  " + tax + " " + amount + "\n");//res2.getString(6)+"\n");
                        }
                    }

                    a++;

                    discaltotal = discaltotal + Double.parseDouble(Basicrate);
                    discal = discaltotal * (Double.parseDouble(discount) / 100);
                    totaldiscount = totaldiscount + discal;
                    discal = 0.0;
                    discaltotal = 0.0;
                    subtotal = subtotal + Double.parseDouble(Basicrate);
                }
                Double grandgst = 0.0;
                int count1 = 0;
                Double cgst = 0.0, sgst = 0.0;
                List<String> result = new ArrayList<>();

                Double sameitemgst = 0.0, sameitemtotalgst = 0.0;
                List<String> taxlist1 = new ArrayList<>();
                final List<String> itemnamelist = new ArrayList<>();
                List<Double> qtylist = new ArrayList<>();
                List<String> ratelist = new ArrayList<>();
                List<Double> amtlist = new ArrayList<>();
                List<String> indexlist1 = new ArrayList<>();
                List<String> disclist = new ArrayList<>();
                List<String> disclistrate = new ArrayList<>();
                for (int k = 0; k < ModelSale.arry_item_name.size(); k++) {
                    itemtax = Double.parseDouble(taxlistupdated.get(k));
                    Cursor c = db.getdistinct(String.valueOf(itemtax));
                    while (c.moveToNext()) {
                        taxlist1.add(c.getString(0));
                        String stringName = c.getString(0);
                        if (checkArray == null || checkArray.size() == 0) {
                            Log.v("TAG", "NO COMPLEMENT");
                            if (ModelSale.arry_item_name.contains(c.getString(0))) {
                                if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                    indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    itemnamelist.add(c.getString(0));
                                    qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                }
                                if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                } else {
                                    itemnamelist.add(c.getString(0));
                                    indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                }
                            }
                        } else {
                            if (ModelSale.arry_item_name.get(k).equals(stringName)) {
                                Log.v("TAGG", ModelSale.arry_item_name.get(k));
                                if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                    //   for(int q=0;q<checkArray.size();q++) {
                                    if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                        Log.v("TAG", "1 IN");
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        //ratelist.add(k,"0.0");
                                        amtlist.add(k, 0.0);
                                        disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        if (counter <= 2) {
                                            double current = Double.parseDouble(tv_total_amt.getText().toString());
                                            double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                            total_val = current - total_val;
                                            if(total_val<=0.0){
                                                tv_total_amt.setText("0.0");
                                            }
                                            else {
                                                tv_total_amt.setText("" + String.format("%.02f", total_val));
                                            }
                                            Log.v("TAG", String.valueOf(total_val));
                                        }
                                        Log.v("TAG", "1 OUT");
                                    } else {
                                        Log.v("TAG", "2 IN");
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        Log.v("TAG", "2 OUT");
                                    }
                                } else if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                    if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                        Log.v("TAG", "3 IN");
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));

                                        amtlist.add(k, 0.0);
                                        disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        if (counter <= 2) {
                                            double current = Double.parseDouble(tv_total_amt.getText().toString());
                                            double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                            total_val = current - total_val;
                                            if(total_val<=0.0){
                                                tv_total_amt.setText("0.0");
                                            }
                                            else {
                                                tv_total_amt.setText("" + String.format("%.02f", total_val));
                                            }
                                            Log.v("TAG", String.valueOf(total_val));
                                        }
                                        Log.v("TAG", "3 OUT");
                                    } else {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        Log.v("TAG", "4 OUT");
                                    }
                                } else {
                                    if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                        Log.v("TAG", "4 IN");
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, 0.0);
                                        disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        if (counter <= 2) {
                                            double current = Double.parseDouble(tv_total_amt.getText().toString());
                                            double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                            total_val = current - total_val;
                                            if(total_val<=0.0){
                                                tv_total_amt.setText("0.0");
                                            }
                                            else {
                                                tv_total_amt.setText("" + String.format("%.02f", total_val));
                                            }
                                            Log.v("TAG", String.valueOf(total_val));
                                        }
                                        Log.v("TAG", "4 OUT");
                                    } else {
                                        Log.v("TAGG", "CHECKIN4");
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        Log.v("TAGG", "CHECKOUT4");
                                    }
                                }
                            }
                        }
                    }
                }


                taxlist1.clear();
                for (int k = 0; k < itemnamelist.size(); k++) {
                    DecimalFormat formats = new DecimalFormat("#####.00");
                    taxlist1.clear();
                    int count = itemnamelist.size();
                    String itemname = itemnamelist.get(k);
                    String largname = "";
                    Double qty = qtylist.get(k);
                    String rate = disclistrate.get(k);
                    String amount = String.valueOf(qty * Double.parseDouble(rate));
                    itemtax = Double.valueOf(indexlist1.get(k));
                    Cursor c = db.getdistinct(String.valueOf(itemtax));
                    while (c.moveToNext()) {
                        taxlist1.add(c.getString(0));
                    }
                    cgst = (itemtax / 2);
                    sgst = cgst;
                    if (amount.length() > 4) {
                        amount = amount.substring(0, 4);

                    } else {
                        int length = amount.length();
                        int l1 = 0;
                        if (length < 4) {
                            l1 = 4 - length;
                            amount = String.format(amount + "%" + (l1) + "s", "");
                        }
                    }

                    List<String> list = new ArrayList<>();
                    for (String aObject : taxlist1) {
                        if (itemnamelist.contains(aObject)) {
                            list.add(aObject);
                        }
                    }
                    result = list;

                    i("dataname", String.valueOf(result));
                    int resultcount = result.size();

                    if (itemname.length() > 15) {
                        if (rate.length() == 3) {
                            count1++;
                            if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                int p = result.indexOf(itemnamelist.get(k));
                                int f = taxlist1.size();
                                int h = itemnamelist.size();
                                if (p == (result.size() - 1)) {
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");

                                } else if (result.size() == 1) {
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                } else {
                                    if (g == f || count == count1) {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                    } else {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    }
                                }
                            } else {
                                totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                grandgst = grandgst + totalwithgst;
                                buffer1.append("    " + itemtax + "%       " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "       " + formats.format(totalwithgst) + "\n");
                            }
                        } else {
                            if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                int p = result.indexOf(itemnamelist.get(k));
                                int f = taxlist1.size();
                                int h = itemnamelist.size();
                                if (p == (result.size() - 1)) {
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                } else if (result.size() == 1) {
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                } else {
                                    if (g == f || count == count1) {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                    } else {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    }
                                }
                            } else {
                                totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                buffer1.append("    " + itemtax + "%       " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "       " + formats.format(totalwithgst) + "\n");
                                grandgst = grandgst + totalwithgst;
                            }
                        }
                    } else {
                        int length = itemname.length();
                        int l1 = 0;
                        if (length < 15) {
                            l1 = 15 - length;
                            String itemname1 = itemname.concat(l1 + " ");
                            itemname = String.format(itemname + "%" + (l1) + "s", "");
                        }
                        if (rate.length() == 3) {
                            count1++;

                            if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                int p = result.indexOf(itemnamelist.get(k));
                                int f = taxlist1.size();
                                int h = itemnamelist.size();
                                if (p == (result.size() - 1)) {
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                } else if (result.size() == 1) {
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                } else {
                                    if (g == f || count == count1) {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                    } else {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    }
                                }
                            } else {
                                totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                buffer1.append("    " + itemtax + "%       " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "       " + formats.format(totalwithgst) + "\n");
                                grandgst = grandgst + totalwithgst;
                            }
                        } else {
                            count1++;
                            if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                int p = result.indexOf(itemnamelist.get(k));
                                int f = taxlist1.size();
                                int h = itemnamelist.size();
                                if (p == (result.size() - 1)) {
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                } else if (result.size() == 1) {
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                } else {
                                    if (g == f || count == count1) {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                    } else {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    }
                                }
                            } else {
                                totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                buffer1.append("    " + itemtax + "%       " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "       " + formats.format(totalwithgst) + "\n");
                                grandgst = grandgst + totalwithgst;
                            }
                        }
                    }
                    j++;
                    finalgst = totalwithgst + finalgst;
                    totalwithgst = 0.0;
                    cgst = 0.0;
                    sgst = 0.0;
                }
                printClass.IntentPrint(String.valueOf(buffer));

                double ddata = Double.parseDouble(tv_total_amt.getText().toString());
                DecimalFormat f = new DecimalFormat("#####.00");
                d = "________________________________________________\n";
                data = "                       Sub TOTAL(Rs): " + String.format("%.02f",subtotal) + "\n";
                printClass.IntentPrint(d);
                printClass.IntentPrint(data);
                if (totaldiscount == 0.0) {
                } else {
                    data = "                      -Dis(Rs)      : " + String.format("%.02f",totaldiscount) + "\n";
                    d = "                     __________________________";
                    printClass.IntentPrint(data);
                    printClass.IntentPrint(d);


                    data = "                                      " + String.format("%.02f",(subtotal - totaldiscount));
                    printClass.IntentPrint(data);
                }
                if (finalgst == 0.0) {
                } else {
//                    data = "                      +GST(Rs): " + String.format("%.02f",finalgst) + "\n";
                    data = "\n                      +GST(Rs)      : " + String.format("%.02f",finalgst) + "\n";

                    printClass.IntentPrint(data);
                    printClass.IntentPrint(d);
                }


                data = "                       Net Total(Rs): " + String.format("%.02f",ddata) + "\n";

                printClass.IntentPrint(data);
                if (f1 == null || f1.length() == 0 || f1 == "") {
                    f1 = null;
                } else {

                    data = "           " + f1 + "\n";
                    printClass.IntentPrint(data);
                }
                if (f2 == null || f2.length() == 0 || f2 == "") {
                    f2 = null;
                } else {
                    data = "           " + f2 + "\n";
                    printClass.IntentPrint(data);
                }
                if (f3 == null || f3.length() == 0 || f3 == "") {
                    f3 = null;
                } else {

                    data = "           " + f3 + "\n";
                    printClass.IntentPrint(data);
                }
                if (f4 == null || f4.length() == 0 || f4 == "") {
                    f4 = null;
                } else {
                    data = "           " + f4 + "\n";
                    printClass.IntentPrint(data);
                }
                if (f5 == null || f5.length() == 0 || f5 == "") {
                    f5 = null;
                } else {
                    data = "            " + f5 + "\n";
                    printClass.IntentPrint(data);
                }

                printClass.IntentPrint("\n\n\n\n");

                clr_all_mtd();
                ids = db.getAllbillID();
                autoinnc();

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        private void twoInchPrintGSTEnable() {
            if (print_option.equals("2 inch")  && gst_option.equals("GST Enable")) {
                Cursor res1 = db.getAllHeaderFooter();
                String h1 = null, h1size = "10", h2 = null, h2size = "9", h3 = null, h3size = "8", h4 = null, h4size = "7", h5 = null, h5size = "6", f1 = null, f1size = "10", f2 = null, f2size = "10", f3 = null, f3size = "10", f4 = null, f4size = "10", f5 = null, f5size = "10";
                while (res1.moveToNext()) {
                    h1 = res1.getString(1);
                    h1size = res1.getString(2);
                    h2 = res1.getString(3);
                    h2size = res1.getString(4);
                    h3 = res1.getString(5);
                    h3size = res1.getString(6);
                    h4 = res1.getString(7);
                    h4size = res1.getString(8);
                    h5 = res1.getString(9);
                    h5size = res1.getString(10);
                    f1 = res1.getString(11);
                    f1size = res1.getString(12);
                    f2 = res1.getString(13);
                    f2size = res1.getString(14);
                    f3 = res1.getString(15);
                    f3size = res1.getString(16);
                    f4 = res1.getString(17);
                    f4size = res1.getString(18);
                    f5 = res1.getString(19);
                    f5size = res1.getString(20);
                }
                Date today = new Date();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                String dateToStr = format.format(today);
                String data = null;

                data = "                 " + "TechMart Cafe" + "             \n";
                String d = "________________________________\n";
                try {
                    if (h1 == null || h1.length() == 0 || h1 == "") {
                        h1 = null;
                    } else {
                        if (h1.length() < h2.length()) {
                            data = "          " + h1 + "\n";
                        }
                        else {
                            data = "         " + h1 + "\n";
                        }
                        try {
                            printClass.IntentPrint(data);
                        }catch (Exception ex) {
                            Message.message(getContext(),"Printer not connected please connect from main settings");
                        }
                    }

                    if (h2 == null || h2.length() == 0 || h2 == "") {
                        h2 = null;
                    } else {
                        if (h2.length() > 16) {
                            data = "     " + h2 + "\n";
                        } else {
                            data = "           " + h2 + "\n";
                        }
                        printClass.IntentPrint(data);
                    }
                    if (h3 == null || h3.length() == 0 || h3 == "") {
                        h3 = null;
                    } else {
                        if (h3.length() > 16) {
                            data = "     " + h3 + "\n";
                        } else {
                            data = "           " + h3 + "\n";
                        }
                        printClass.IntentPrint(data);
                    }
                    if (h4 == null || h4.length() == 0 || h4 == "") {
                        h4 = null;
                    } else {
                        if (h4.length() > 16) {
                            data = "     " + h4 + "\n";
                        } else {
                            data = "           " + h4 + "\n";
                        }
                        printClass.IntentPrint(data);
                    }
                    if (h5 == null || h5.length() == 0 || h5 == "") {
                        h5 = null;
                    } else {
                        if (h4.length() > 16) {
                            data = "     " + h5 + "\n";
                        } else {
                            data = "           " + h5 + "\n";
                        }
                        printClass.IntentPrint(data);
                    }

                    data = "\nCash Memo    bill no:" + emp_code + "\n";
                    try {
                        printClass.IntentPrint(data);
                    }catch (Exception ex) {
                        Message.message(getContext(),"Printer not connected please connect from main settings");
                    }

                    data = "Date:-" + dateToStr + "\n";
                    printClass.IntentPrint(data);
                    if (str_set_cust_name == "" || str_set_cust_name == null) {
                        str_set_cust_name = "";
                        data = "\nItem Name      |Qty|Rate|Amt\n";

                    } else {
                        data = "Customer Name:- " + str_set_cust_name + "\n";
                        printClass.IntentPrint(data);
                        data = "Item Name        |Qty|Rate|Amt\n";
                    }
                    printClass.IntentPrint(data);
                    String totalamtrate = "";
                    int a = 1, j = 0;
                    Double itemtax = 0.0;
                    int[] positions = new int[ModelSale.arry_item_name.size()];
                    StringBuffer buffer = new StringBuffer();
                    Double totalwithgst = 0.0, finalgst = 0.0;
                    Double oldtax = 0.0;
                    int l = 0;

                    DecimalFormat decimalFormat = new DecimalFormat("#####.00");
                    Double grandgst = 0.0;
                    int count1 = 0;
                    Double cgst = 0.0, sgst = 0.0;
                    List<String> result = new ArrayList<>();

                    Double sameitemgst = 0.0, sameitemtotalgst = 0.0;
                    List<String> taxlist1 = new ArrayList<>();
                    final List<String> itemnamelist = new ArrayList<>();
                    List<Double> qtylist = new ArrayList<>();
                    List<String> ratelist = new ArrayList<>();
                    List<Double> amtlist = new ArrayList<>();
                    List<String> indexlist1 = new ArrayList<>();
                    for (int k = 0; k < ModelSale.arry_item_name.size(); k++) {
                        itemtax = Double.parseDouble(taxlistupdated.get(k));
                        Cursor c = db.getdistinct(String.valueOf(itemtax));
                        while (c.moveToNext()) {
                            taxlist1.add(c.getString(0));
                            String stringName = c.getString(0);
                            if(checkArray==null || checkArray.size()==0){
                                if (ModelSale.arry_item_name.contains(c.getString(0))) {
                                    if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    }
                                    if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                    } else {
                                        itemnamelist.add(c.getString(0));
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    }
                                }
                            }
                            else{
                                if (ModelSale.arry_item_name.get(k).equals(stringName)) {
                                    Log.v("TAGG",ModelSale.arry_item_name.get(k));
                                    if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                        //   for(int q=0;q<checkArray.size();q++) {
                                        if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(k, 0.0);
//                                            if (counter<=1) {
                                            double current = Double.parseDouble(tv_total_amt.getText().toString());
                                            double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                            total_val = current - total_val;
                                            if(total_val<=0.0){
                                                tv_total_amt.setText("0.0");
                                            }
                                            else {
                                                tv_total_amt.setText("" + String.format("%.02f", total_val));
                                            }
                                            Log.v("TAG", String.valueOf(total_val));
//                                            }
                                            //checkArray.remove(q);
                                            // break;
                                            //Log.v("TAGG", "CHECKOUT");
                                        } else {
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        }
                                    }
                                    else if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                        if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(k, 0.0);
//                                            if (counter<=1) {
                                            double current = Double.parseDouble(tv_total_amt.getText().toString());
                                            double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                            total_val = current - total_val;
                                            if(total_val<=0.0){
                                                tv_total_amt.setText("0.0");
                                            }
                                            else {
                                                tv_total_amt.setText("" + String.format("%.02f", total_val));
                                            }
                                            Log.v("TAG", String.valueOf(total_val));
//                                            }
                                        } else {
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        }
                                    }
                                    else {
                                        if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(k, 0.0);

//                                            if (counter<=1) {
                                            double current = Double.parseDouble(tv_total_amt.getText().toString());
                                            double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                            total_val = current - total_val;
                                            if(total_val<=0.0){
                                                tv_total_amt.setText("0.0");
                                            }
                                            else {
                                                tv_total_amt.setText("" + String.format("%.02f", total_val));
                                            }
                                            Log.v("TAG", String.valueOf(total_val));
//                                            }
                                        } else {
                                            Log.v("TAGG", "CHECKIN4");
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            Log.v("TAGG", "CHECKOUT4");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    taxlist1.clear();
                    for (int k = 0; k < itemnamelist.size(); k++) {
                        taxlist1.clear();
                        int count = itemnamelist.size();
                        String itemname = itemnamelist.get(k);
                        String largname = "";
                        Double qty = qtylist.get(k);
                        String rate = ratelist.get(k);
                        String amount = String.valueOf(amtlist.get(k));
                        itemtax = Double.valueOf(indexlist1.get(k));
                        Cursor c = db.getdistinct(String.valueOf(itemtax));
                        while (c.moveToNext()) {
                            taxlist1.add(c.getString(0));
                        }
                        cgst = (itemtax / 2);
                        sgst = cgst;
                        if (amount.length() >6) {
                            amount = amount.substring(0, 6);

                        } else {
                            int length = amount.length();
                            int l1 = 0;
                            if (length < 6) {
                                l1 = 6 - length;
                                amount = String.format(amount + "%" + (l1) + "s", "");
                            }
                        }

                        List<String> list = new ArrayList<>();
                        for (String aObject : taxlist1) {
                            if (itemnamelist.contains(aObject)) {
                                list.add(aObject);
                            }
                        }
                        result = list;

                        i("dataname", String.valueOf(result));
                        int resultcount = result.size();

                        if (itemname.length() > 15) {
                            if (rate.length() == 3) {
                                count1++;
                                if(itemtax==0.0){
                                    buffer.append(itemname.substring(0, 15) +  " " + qty + "  " + rate + "  " + String.format("%.02f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");}
                                }
                                else{
                                    if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                        int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                        int p = result.indexOf(itemnamelist.get(k));
                                        int f = taxlist1.size();
                                        int h = itemnamelist.size();
                                        if (p == (result.size() - 1)) {

                                            buffer.append(itemname.substring(0, 15) + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                            if (amount.equals("0.0   ")) {
                                            } else {
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
                                                buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                            }
                                        } else if (result.size() == 1) {
                                            buffer.append(itemname.substring(0, 15) + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                            if (amount.equals("0.0   ")) {
                                            } else {
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
                                                buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                            }
                                        } else {
                                            if (g == f || count == count1) {
                                                buffer.append(itemname.substring(0, 15) + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                                if (amount.equals("0.0   ")) {
                                                } else {
                                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                    finalgst = finalgst + sameitemtotalgst;
                                                    grandgst = grandgst + sameitemtotalgst;
                                                    buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                                }
                                            } else {
                                                buffer.append(itemname.substring(0, 15) + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            }
                                        }
                                    } else {
                                        buffer.append(itemname.substring(0, 15) + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                        if (amount.equals("0.0   ")) {
                                        } else {
                                            totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                            buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.02f", totalwithgst) + "\n");
                                            grandgst = grandgst + totalwithgst;
                                        }
                                    }
                                }
                            }
                            else {
                                if (itemtax == 0.0) {
                                    buffer.append(itemname.substring(0, 15) + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");}
                                } else {
                                    if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                        int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                        int p = result.indexOf(itemnamelist.get(k));
                                        int f = taxlist1.size();
                                        int h = itemnamelist.size();
                                        if (p == (result.size() - 1)) {
                                            buffer.append(itemname.substring(0, 15) + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                            if (amount.equals("0.0   ")) {
                                            } else {
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
                                                buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                            }
                                        } else if (result.size() == 1) {
                                            buffer.append(itemname.substring(0, 15) + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                            if (amount.equals("0.0   ")) {
                                            } else {
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
                                                buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                            }
                                        } else {
                                            if (g == f || count == count1) {
                                                buffer.append(itemname.substring(0, 15) + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                                if (amount.equals("0.0   ")) {
                                                } else {
                                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                    finalgst = finalgst + sameitemtotalgst;
                                                    grandgst = grandgst + sameitemtotalgst;
                                                    buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                                }
                                            } else {
                                                buffer.append(itemname.substring(0, 15) + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            }
                                        }
                                    } else {
                                        buffer.append(itemname.substring(0, 15) + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                        if (amount.equals("0.0   ")) {
                                        } else {
                                            totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                            buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.02f", totalwithgst) + "\n");
                                            grandgst = grandgst + totalwithgst;
                                        }
                                    }
                                }
                            }
                        }
                        else {
                            int length = itemname.length();
                            int l1 = 0;
                            if (length < 15) {
                                l1 = 15 - length;
                                String itemname1 = itemname.concat(l1 + " ");
                                itemname = String.format(itemname + "%" + (l1) + "s", "");
                            }
                            if (rate.length() == 3) {
                                count1++;
                                if(itemtax==0.0){
                                    buffer.append(itemname.substring(0, 15) +  " " + qty + "  " + rate + "  " + String.format("%.02f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");}
                                }
                                else {
                                    if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                        int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                        int p = result.indexOf(itemnamelist.get(k));
                                        int f = taxlist1.size();
                                        int h = itemnamelist.size();
                                        if (p == (result.size() - 1)) {
                                            buffer.append(itemname + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.valueOf(amount)) + "\n");
                                            if (amount.equals("0.0   ")) {
                                            } else {
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
                                                buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                            }
                                        } else if (result.size() == 1) {
                                            buffer.append(itemname + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.valueOf(amount)) + "\n");
                                            if (amount.equals("0.0   ")) {
                                            } else {
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
                                                buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                            }
                                        } else {
                                            if (g == f || count == count1) {
                                                buffer.append(itemname + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                                if (amount.equals("0.0   ")) {
                                                } else {
                                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                    finalgst = finalgst + sameitemtotalgst;
                                                    grandgst = grandgst + sameitemtotalgst;
                                                    buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                                }
                                            } else {
                                                buffer.append(itemname + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);

                                            }
                                        }
                                    } else {
                                        buffer.append(itemname + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                        if (amount.equals("0.0   ")) {
                                        } else {
                                            totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                            buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.02f", totalwithgst) + "\n");
                                            grandgst = grandgst + totalwithgst;
                                        }
                                    }
                                }
                            } else {
                                count1++;
                                if (itemtax == 0.0) {
                                    buffer.append(itemname.substring(0, 15) + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");}
                                } else {
                                    if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                        int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                        int p = result.indexOf(itemnamelist.get(k));
                                        int f = taxlist1.size();
                                        int h = itemnamelist.size();
                                        if (p == (result.size() - 1)) {
                                            buffer.append(itemname + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.valueOf(amount)) + "\n");
                                            if (amount.equals("0.0   ")) {
                                            } else {
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
                                                buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                            }
                                        } else if (result.size() == 1) {
                                            buffer.append(itemname + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                            if (amount.equals("0.0   ")) {
                                            } else {
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
                                                buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                            }
                                        } else {
                                            if (g == f || count == count1) {
                                                buffer.append(itemname + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                                if (amount.equals("0.0   ")) {
                                                } else {
                                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                    finalgst = finalgst + sameitemtotalgst;
                                                    grandgst = grandgst + sameitemtotalgst;
                                                    buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.02f", sameitemtotalgst) + "\n");
                                                }
                                            } else {
                                                buffer.append(itemname + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            }
                                        }
                                    } else {
                                        buffer.append(itemname + " " + qty + "  " + rate + "  " + String.format("%.02f", Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                        if (amount.equals("0.0   ")) {
                                        } else {
                                            totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                            buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.02f", totalwithgst) + "\n");
                                            grandgst = grandgst + totalwithgst;
                                        }
                                    }
                                }
                            }
                        }
                        j++;
                        finalgst = totalwithgst + finalgst;
                        totalwithgst = 0.0;
                    }

                    Double totalAmount = Double.parseDouble(tv_total_amt.getText().toString())- finalgst;
                    printClass.IntentPrint(String.valueOf(buffer));


                    double ddata = Double.parseDouble(tv_total_amt.getText().toString());
                    DecimalFormat f = new DecimalFormat("#####.00");

                    if(totalAmount<=0.0){
                        totalAmount=0.0;
                    }
                    else{
                        totalAmount=totalAmount;
                    }
                    if(grandgst<=0.0){
                        grandgst=0.0;
                    }
                    else{
                        grandgst=grandgst;
                    }
                    Double d12= Double.parseDouble(tv_total_amt.getText().toString());
                    if(d12<=0.0){
                        tv_total_amt.setText("0.0");
                    }
                    else{
                        tv_total_amt.setText(""+d12);
                    }

                    data = "         NET TOTAL (Rs): " + String.format("%.02f",totalAmount) + "\n";
                    printClass.IntentPrint(d);
                    printClass.IntentPrint(data);
                    data = "         Total GST (Rs): " + String.format("%.02f",grandgst) + "\n";
                    printClass.IntentPrint(d);
                    printClass.IntentPrint(data);
                    data = "     TOTAL With GST(Rs): " + String.format("%.02f",Double.parseDouble(tv_total_amt.getText().toString())) + "\n";
                    printClass.IntentPrint(d);
                    printClass.IntentPrint(data);

                    if (f1 == null || f1.length() == 0 || f1 == "") {
                        f1 = null;
                    } else {
                        data = "     " + f1 + "\n";
                        printClass.IntentPrint(data);
                    }
                    if (f2 == null || f2.length() == 0 || f2 == "") {
                        f2 = null;
                    } else {
                        data = "     " + f2 + "\n";
                        printClass.IntentPrint(data);
                    }
                    if (f3 == null || f3.length() == 0 || f3 == "") {
                        f3 = null;
                    } else {

                        data = "     " + f3 + "\n";
                        printClass.IntentPrint(data);
                    }
                    if (f4 == null || f4.length() == 0 || f4 == "") {
                        f4 = null;
                    } else {
                        data = "     " + f4 + "\n";
                        printClass.IntentPrint(data);
                    }
                    if (f5 == null || f5.length() == 0 || f5 == "") {
                        f5 = null;
                    } else {
                        data = "     " + f5 + "\n";
                        printClass.IntentPrint(data);
                    }

                    printClass.IntentPrint("\n\n\n\n");

                    clr_all_mtd();
                    ids = db.getAllbillID();
                    autoinnc();
                    Message.message(getContext(), "Bill saved");
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        private void autoinnc() {
        String resetbill=manager.getResetBill();
        try {
            ids = db.getAllbillID();
            String d="";
            int r =1;
            Date today = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            String dateToday = format.format(today);
            String yesterday="";
            if(resetbill.equals("Yes")) {
                while (ids.moveToNext()) {
                    d = ids.getString(0);
                    yesterday = ids.getString(1);
            }
                if (yesterday == null||d==null||d=="" ) {
                    tvbill.setText("" + r);
                }
                else if (!dateToday.equals(yesterday)) {

                    tvbill.setText("" + r);
                }
                else if (dateToday.equals(yesterday)) {
                    r = Integer.parseInt(d);
                    r = r + 1;
                    tvbill.setText("" + r);
                } else {
                    r = Integer.parseInt(d);
                    r = r + 1;
                    tvbill.setText("" + r);

                }
            }
            else if(resetbill.equals("No")) {
                while (ids.moveToNext()) {
                    d = ids.getString(0);
                    if (d == ""||d==null) {
                        tvbill.setText("1");
                    } else {
                        r = Integer.parseInt(ids.getString(0));
                        r = r + 1;
                        tvbill.setText("" + r);
                    }
                }
            }
        }
        catch (Exception ex) {
            Message.message(getContext(),""+ex.getMessage());
        }
    }

        private void RemoveMessageBox(String gst_data_blueprints_three_inch_gst_disable, String reprinting, String s, String s1) {

         }

    private void showRecycle() {
        final Dialog dialog = new Dialog(Objects.requireNonNull(getContext()));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_listview);
        Button btndialog = dialog.findViewById(R.id.btndialog);
        btndialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        RecyclerView thumbnailRecycle=dialog.findViewById(R.id.thumbnail_recycle);
        final LinearLayoutManager manager = new LinearLayoutManager(getContext());
        thumbnailRecycle.setLayoutManager(manager);
        thumbnailRecycle.setHasFixedSize(true);
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getContext(),R.anim.slide_from_right_layout);
        thumbnailRecycle.setLayoutAnimation(animation);
        adapter=new ThumbnailRecycle();
        thumbnailRecycle.setAdapter(adapter);
        Objects.requireNonNull(dialog.getWindow()).setLayout(RelativeLayout.LayoutParams.MATCH_PARENT,  RelativeLayout.LayoutParams.WRAP_CONTENT);
        dialog.show();
    }

    class ThumbnailRecycle extends RecyclerView.Adapter<CodewiseHolder>{

        @NonNull
        @Override
        public CodewiseHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View itemView = inflater.inflate(R.layout.layout_codewise,parent,false);
            return new CodewiseHolder(itemView);

        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull final CodewiseHolder holder, final int position) {
            ClassCodeWiseList codewise=itemArrayList.get(position);
            holder.tv_sr_no.setText(codewise.getSr_no() + "");
            holder.tv_item_name1.setText(codewise.getItem_name() + "");
            holder.tv_rate.setText(codewise.getRate() + "");
            holder.tv_qty1.setText(codewise.getQty() + "");
            holder.tv_amount.setText(codewise.getAmount() + "");
            holder.tv_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        cost = cost - (ModelSale.arr_item_price.get(position));
                        if(cost<=0 || tv_total_amt.getText().toString().equals("0.00") || tv_total_amt.getText().toString().equals("-0.00") || tv_total_amt.getText().toString().equals("-0") || tv_total_amt.getText().toString().equals("-0.0")) {
                            tv_total_amt.setText("0.00");
                            cost=0;
                        }
                        else
                            tv_total_amt.setText("" + String.format("%.02f", cost));

                        ModelSale.arr_item_rate.remove(position);
                        ModelSale.arry_item_name.remove(position);
                        ModelSale.arr_item_qty.remove(position);
                        ModelSale.arr_item_price.remove(position);
                        ModelSale.arr_item_basicrate.remove(position);
                        ModelSale.arr_item_dis_rate.remove(position);
                        ModelSale.arr_item_dis.remove(position);
                        ModelSale.arr_item_tax.remove(position);
                        ModelSale.array_item_amt.remove(position);

                        if(position==(itemArrayList.size()-1))
                            cnt=position+1;
                        itemArrayList.remove(position);
                        if(itemArrayList.size()==0)
                            cnt=1;
                        for(int i = (position);i<itemArrayList.size();i++){
                            itemArrayList.set(i,new ClassCodeWiseList(""+(i+1) , "" + ModelSale.arry_item_name.get(i), "" + ModelSale.arr_item_qty.get(i), "" +Double.valueOf(ModelSale.arr_item_basicrate.get(i)), "" + ModelSale.arr_item_price.get(i), ""));
                            cnt=i+2;
                        }
                        notifyDataSetChanged();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });

            holder.chkIos.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    int model_pos = Integer.parseInt(holder.tv_sr_no.getText().toString()) - 1;
                    if (b) {
                        String item = itemArrayList.get(model_pos).getItem_name();
                        if (!checkArray.contains(item))
                            checkArray.add(itemArrayList.get(model_pos).getItem_name());
                    }else{
                        checkArray.remove(itemArrayList.get(model_pos).getItem_name());
                    }
                }
            });

            for(int i=0;i<checkArray.size();i++) {
                if(itemArrayList.get(position).getItem_name().equals(checkArray.get(i))) {
                    holder.chkIos.setChecked(true);
                }
            }
        }

        @Override
        public int getItemCount() {
            return itemArrayList.size();
        }
    }

    @Override
    public int getMenuLayout() {
        return R.menu.codewise_menu;
    }

    @Override
    public int getFragmentLayout() {
        return R.layout.thumbnail_fragment;
    }
}
