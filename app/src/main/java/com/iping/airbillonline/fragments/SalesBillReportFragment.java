package com.iping.airbillonline.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.iping.airbillonline.R;
import com.iping.airbillonline.pojo.SalesBillReportPOJO;
import com.iping.airbillonline.recycle.SalesBillReportRecycle;
import com.iping.airbillonline.viewmodel.SalesBillReportViewModel;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class SalesBillReportFragment extends AbstractFragment implements SalesBillReportRecycle.ItemListener, SalesBillReportRecycle.DeleteItemListener {

    private SalesBillReportViewModel salesBillReportViewModel;

    private RecyclerView salesBillReportRecycle;

    private List<SalesBillReportPOJO> salesBillList;

    private Date today = new Date();
    private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

    private EditText et_from_date;
    private EditText et_to_date;

    private Button btn_submit;

    private String fromdate=format.format(today);
    private String todate=format.format(today);
    private String startdate="";
    private long miliSecsDate;

    private SalesBillReportFragment fragment=this;


    private TextView tvamt;

    @Override
    public void onStart() {
        super.onStart();
        View view=getView();
        salesBillReportViewModel= new ViewModelProvider(Objects.requireNonNull(getActivity())).get(SalesBillReportViewModel.class);
        salesBillReportViewModel.init(getActivity(),fromdate,todate);

        et_from_date= Objects.requireNonNull(view).findViewById(R.id.et_from_date);
        et_to_date= view.findViewById(R.id.et_to_date);
        btn_submit= view.findViewById(R.id.btn_submit);
        tvamt= view.findViewById(R.id.tvamt);

        salesBillReportRecycle=view.findViewById(R.id.sales_bill_report_recycle);
        final LinearLayoutManager manager = new LinearLayoutManager(getContext());
        salesBillReportRecycle.setLayoutManager(manager);
        salesBillReportRecycle.setHasFixedSize(true);
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getContext(),R.anim.slide_from_right_layout);
        salesBillReportRecycle.setLayoutAnimation(animation);

        et_from_date.setOnClickListener(this);
        et_to_date.setOnClickListener(this);
        btn_submit.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        salesBillReportViewModel.getSalesBillReports().observe(Objects.requireNonNull(getActivity()), new Observer<List<SalesBillReportPOJO>>() {
            @Override
            public void onChanged(List<SalesBillReportPOJO> salesBillReportPOJOS) {
                if (salesBillReportPOJOS.size()>0) {
                    SalesBillReportRecycle adapter = new SalesBillReportRecycle(salesBillReportPOJOS,getActivity());
                    adapter.setListener(fragment);
                    adapter.setDeleteSalesBillListener(fragment);
                    salesBillList=salesBillReportPOJOS;
                    salesBillReportRecycle.setAdapter(adapter);
                    tvamt.setText(salesBillReportViewModel.getTotalAmt());
                }else{
                    TextView view= Objects.requireNonNull(getView()).findViewById(R.id.no_found);
                    salesBillReportRecycle.setVisibility(View.INVISIBLE);
                    view.setVisibility(View.VISIBLE);
                    tvamt.setText("0.0");
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        if (v.getId()==et_from_date.getId())
            datePicker1();
        if(v.getId()==et_to_date.getId())
            datePicker2();
        if (v.getId()==btn_submit.getId()){
            if(et_from_date.getText().toString().equals("") || et_to_date.getText().toString().equals("")){
                Toast.makeText(getContext(),"Please select the From and To Date",Toast.LENGTH_SHORT).show();
            }else {
                Log.v("TAGGG","SALESBILLREPORT");
                salesBillReportViewModel.init(getActivity(),et_from_date.getText().toString(),et_to_date.getText().toString());
            }
        }
    }

    private void removeMessageBox(final String cat_id, final String catname, String title, String msg) {
        final Dialog dialog = new Dialog(Objects.requireNonNull(getActivity()));
        dialog.setContentView(R.layout.warning_layout1);
        Button bt_ok = dialog.findViewById(R.id.bt_ok);
        Button bt_cancel1 = dialog.findViewById(R.id.bt_cancel);
        TextView tv_title = dialog.findViewById(R.id.tv_title);
        TextView tv_errortext = dialog.findViewById(R.id.tv_errortext);
        final EditText et_remark= dialog.findViewById(R.id.et_remark);
        tv_title.setText(title);
        tv_errortext.setText(msg);
        bt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(et_remark.getText().toString().equals("")){
                    et_remark.setError("Please Enter Remark");
                }else if(!et_remark.getText().toString().matches("[a-zA-Z ]*")){
                    et_remark.setError("Enter Characters only");
                }else {
                    Toast.makeText(getActivity(), "Bill Deleted!!! "+cat_id+" "+catname, Toast.LENGTH_SHORT).show();
                    db.deleteSaleBill(cat_id, catname,et_remark.getText().toString());
                    dialog.dismiss();
                    salesBillReportViewModel.init(getActivity(),fromdate,todate);
                }

            }
        });
        bt_cancel1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }

    private void datePicker1(){
        final Calendar c1 = Calendar.getInstance();
        int mYear1 = c1.get(Calendar.YEAR);
        int mMonth1 = c1.get(Calendar.MONTH);
        int mDay1 = c1.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        et_from_date.setText(dayOfMonth+"-"+(monthOfYear+1)+"-"+year);
                        if ((monthOfYear+1)<10 && dayOfMonth<10 ) {
                            fromdate = year + "-0" + (monthOfYear + 1) + "-0" + dayOfMonth;
                        }
                        else if((monthOfYear+1)<10 && dayOfMonth>10)
                        {
                            fromdate = year + "-0" + (monthOfYear + 1) + "-" + dayOfMonth;
                        }
                        else if((monthOfYear+1)>=10 && dayOfMonth<10)
                        {
                            fromdate = year + "-" + (monthOfYear + 1) + "-0" + dayOfMonth;
                        }
                        else if((monthOfYear+1)>=10 && dayOfMonth==10)
                        {
                            fromdate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        }
                        else if((monthOfYear+1)>=10 && dayOfMonth>10)
                        {
                            fromdate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        }
                        startdate=year+"-"+(monthOfYear+1)+"-"+(dayOfMonth);
                        miliSecsDate = milliseconds(startdate);
                    }
                }, mYear1, mMonth1, mDay1);

        datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
            }
        });
        datePickerDialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    private void datePicker2(){
        final Calendar c2 = Calendar.getInstance();
        int mYear2 = c2.get(Calendar.YEAR);
        int mMonth2 = c2.get(Calendar.MONTH);
        int mDay2 = c2.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        et_to_date.setText(dayOfMonth+"-"+(monthOfYear+1)+"-"+year);
                        if ((monthOfYear+1)<10 && dayOfMonth<10 ) {
                            todate = year + "-0" + (monthOfYear + 1) + "-0" + dayOfMonth;
                        }
                        else if((monthOfYear+1)<10 && dayOfMonth>=10)
                        {
                            todate = year + "-0" + (monthOfYear + 1) + "-" + dayOfMonth;
                        }
                        else if((monthOfYear+1)>=10 && dayOfMonth<10)
                        {
                            todate = year + "-" + (monthOfYear + 1) + "-0" + dayOfMonth;
                        }
                        else if((monthOfYear+1)>=10 && dayOfMonth==10)
                        {
                            todate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        }
                        else if((monthOfYear+1)>=10 && dayOfMonth>10)
                        {
                            todate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        }
                    }
                }, mYear2, mMonth2, mDay2);

        datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {

            }
        });
        if(startdate.equals("")){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            }
        }else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                datePickerDialog.getDatePicker().setMinDate(miliSecsDate);
            }
        }
        datePickerDialog.show();
    }

    @Override
    public int getMenuLayout() {
        return R.menu.print_menu;
    }

    @Override
    public int getFragmentLayout() {
        return R.layout.sales_bill_report_fragment;
    }

    @Override
    public void onClick(SalesBillReportPOJO item) {

    }

    @Override
    public void onDeleteClick(String cat_id, String catname, String title, String msg) {
        removeMessageBox(cat_id, catname, "Remove", "Do you want to delete the Bill no  " + cat_id + "?");
    }
}
