package com.iping.airbillonline.viewmodel;

import android.content.Context;
import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.iping.airbillonline.database.DatabaseHelper;
import com.iping.airbillonline.pojo.SalesBillReportPOJO;
import java.util.ArrayList;
import java.util.List;

public class SalesBillReportViewModel extends ViewModel {

    private MutableLiveData<List<SalesBillReportPOJO>> salesBillReport;
    private DatabaseHelper db;
    private String totalAmt;

    public void init(Context context, String fromdate, String today){
        if (salesBillReport==null) {
            salesBillReport = new MutableLiveData<>();
        }
        db= DatabaseHelper.getInstance(context);
        getSalesBillReport(fromdate,today);
    }

    private void getSalesBillReport(String fromdate,String todate) {
        List<SalesBillReportPOJO> itemArrayList=new ArrayList<>();
        int i = 1;
        Cursor c =db.getSalesBillReport(fromdate, todate);
        double total=0.0;
        try {
            if (c.getCount() != 0) {
                while (c.moveToNext()) {
                    itemArrayList.add(new SalesBillReportPOJO(""+i, "" + c.getString(0), "" + c.getString(6), "" + c.getString(4),""+c.getString(2)));
                    total= total+Double.parseDouble(c.getString(6));
                    i++;
                }
            }
             //tvamt.setText(""+String.format("%.02f",total));
            this.totalAmt= String.format("%.02f",total);
            salesBillReport.setValue(itemArrayList);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public LiveData<List<SalesBillReportPOJO>> getSalesBillReports(){
        return salesBillReport;
    }

    public String getTotalAmt() {
        return totalAmt;
    }
}