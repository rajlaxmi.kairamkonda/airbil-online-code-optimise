package com.iping.airbillonline.viewmodel;

import android.content.Context;
import android.database.Cursor;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.iping.airbillonline.database.DatabaseHelper;
import com.iping.airbillonline.pojo.DeletedSalesBillReportPOJO;
import com.iping.airbillonline.pojo.SalesBillReportPOJO;

import java.util.ArrayList;
import java.util.List;

public class DeletedSalesBillReportViewModel extends ViewModel {

    private MutableLiveData<List<DeletedSalesBillReportPOJO>> deletedSalesBillReport;
    private DatabaseHelper db;
    private String totalAmt;

    public void init(Context context, String fromdate, String today){
        if (deletedSalesBillReport==null) {
            deletedSalesBillReport = new MutableLiveData<>();
        }
        db= DatabaseHelper.getInstance(context);
        getDeletedSalesBillReport(fromdate,today);
    }

    private void getDeletedSalesBillReport(String fromdate, String today) {
        List<DeletedSalesBillReportPOJO> itemArrayList=new ArrayList<>();
        int i = 1;
        String date1 = fromdate;//et_from_date.getText().toString();
        String date2 = today;// et_to_date.getText().toString();
        Cursor c = db.getDeletedSalesBillReport(date1, date2);
        double total=0.0;
        try {
            if (c.getCount() != 0) {
                // show message
                //Message.message(getApplicationContext(),"Nothing found");
                while (c.moveToNext()) {
                    String bdate = c.getString(2);
                    itemArrayList.add(new DeletedSalesBillReportPOJO("" + i, "" + c.getString(1), "" + c.getString(6), "" + c.getString(21)));
                    total= total+Double.parseDouble(c.getString(6));
                    i++;
                }
            }

           // tvamt.setText(String.format("%.2f",total));
            this.totalAmt= String.format("%.02f",total);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        deletedSalesBillReport.setValue(itemArrayList);
    }

    public LiveData<List<DeletedSalesBillReportPOJO>> getDeletedReport(){
        return deletedSalesBillReport;
    }

    public String getTotalAmt() {
        return totalAmt;
    }

}
