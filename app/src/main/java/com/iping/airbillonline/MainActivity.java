package com.iping.airbillonline;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.android.material.navigation.NavigationView;
import com.iping.airbillonline.fragments.AbstractFragment;
import com.iping.airbillonline.fragments.CodewiseFragment;
import com.iping.airbillonline.fragments.ListCategoryFragment;
import com.iping.airbillonline.fragments.ListUnitsFragment;
import com.iping.airbillonline.fragments.AdminDashboardFragment;
import com.iping.airbillonline.fragments.CustomerListFragment;
import com.iping.airbillonline.fragments.DeletedSalesBillReportFragment;
import com.iping.airbillonline.fragments.HeaderFooterSettingFragment;
import com.iping.airbillonline.fragments.HelpFragment;
import com.iping.airbillonline.fragments.ItemListFragment;
import com.iping.airbillonline.fragments.SalesBillReportFragment;
import com.iping.airbillonline.fragments.SupplierListFragment;
import com.iping.airbillonline.fragments.TableListFragment;
import com.iping.airbillonline.fragments.ThumbnailFragment;
import com.iping.airbillonline.fragments.TableFragment;
import com.iping.airbillonline.fragments.WaiterFragment;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity implements  View.OnClickListener {

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;

    private CircleImageView circleImage;

    private LinearLayout navSettings,layoutsetting;
    private LinearLayout headerSetting, mainSetting;
    private ImageView ivarrow;

    private LinearLayout navMaster,layoutMaster;
    private LinearLayout addItem,addCategory,addCustomer,addUnit,addSupplier,addTableList,addWaiterList;
    private ImageView masterIcon;

    private LinearLayout navSales,layoutSales;
    private LinearLayout thumbnail,codewise,tablebooking;
    private ImageView salesIcon;

    private LinearLayout navReport,layoutReport;
    private LinearLayout saleBillReport,deletedSalesBillReport;
    private ImageView reportIcon;

    private LinearLayout navDashboard;
    private LinearLayout navhelp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.navigation_view);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.open,R.string.close);
        drawerLayout.addDrawerListener(toggle);

        View view = navigationView.getHeaderView(0);
        circleImage = view.findViewById(R.id.circle_image);

        navDashboard = view.findViewById(R.id.nav_dashboard);
        navDashboard.setOnClickListener(this);

        navhelp=view.findViewById(R.id.nav_help);
        navhelp.setOnClickListener(this);
        masterLayout(view);
        salesLayout(view);
        reportsLayout(view);
        settingLayout(view);
        addFragment(new AdminDashboardFragment(),true);
    }

    public void addFragment(AbstractFragment fragment,boolean stack){
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
        fragmentTransaction.replace(R.id.fragment_layout, fragment);
        if (stack)
            fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


    private void settingLayout(View view) {
        navSettings = view.findViewById(R.id.nav_setting);
        layoutsetting = view.findViewById(R.id.layout_setting);
        headerSetting = view.findViewById(R.id.headerSetting);
        mainSetting = view.findViewById(R.id.main_setting);
        ivarrow = view.findViewById(R.id.iv_arrow);
        navSettings.setOnClickListener(this);
        headerSetting.setOnClickListener(this);
        mainSetting.setOnClickListener(this);
    }

    private void reportsLayout(View view) {
        navReport=view.findViewById(R.id.nav_reports);
        layoutReport=view.findViewById(R.id.layout_report);
        saleBillReport=view.findViewById(R.id.sales_bill_report);
        deletedSalesBillReport=view.findViewById(R.id.deleted_sales_bill);
        reportIcon=view.findViewById(R.id.report_arrow);
        navReport.setOnClickListener(this);
        saleBillReport.setOnClickListener(this);
        deletedSalesBillReport.setOnClickListener(this);
    }

    private void salesLayout(View view) {
        navSales=view.findViewById(R.id.nav_sales);
        layoutSales=view.findViewById(R.id.layout_sales);
        thumbnail=view.findViewById(R.id.thumbnail);
        codewise=view.findViewById(R.id.codewise);
        tablebooking=view.findViewById(R.id.tablebooking);
        salesIcon=view.findViewById(R.id.sales_arrow);
        navSales.setOnClickListener(this);
        thumbnail.setOnClickListener(this);
        codewise.setOnClickListener(this);
        tablebooking.setOnClickListener(this);
    }

    public void masterLayout(View view){
        navMaster=view.findViewById(R.id.nav_master);
        layoutMaster=view.findViewById(R.id.layout_master);
        addItem=view.findViewById(R.id.add_item);
        addCategory=view.findViewById(R.id.add_category);
        masterIcon=view.findViewById(R.id.master_arrow);
        addCustomer=view.findViewById(R.id.add_customer);
        addUnit=view.findViewById(R.id.add_unit);
        addSupplier=view.findViewById(R.id.add_supplier);
        addTableList=view.findViewById(R.id.add_table_list);
        addWaiterList=view.findViewById(R.id.add_waiter_list);
        navMaster.setOnClickListener(this);
        addCategory.setOnClickListener(this);
        addItem.setOnClickListener(this);
        addCustomer.setOnClickListener(this);
        addUnit.setOnClickListener(this);
        addSupplier.setOnClickListener(this);
        addTableList.setOnClickListener(this);
        addWaiterList.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        setMasterChildListener(view);
        setSalesChildListener(view);
        setReportChildListener(view);
        setSettingChildListener(view);
        if(navDashboard.getId()==view.getId()){
            Log.v("TAGG","dashboard");
            addFragment(new AdminDashboardFragment(),false);
            drawerLayout.closeDrawer(GravityCompat.START,true);
        }if (navhelp.getId()==view.getId()){
            addFragment(new HelpFragment(),false);
            drawerLayout.closeDrawer(GravityCompat.START,true);
        }if (view == navSettings) {
            if (layoutsetting.getVisibility()==View.GONE) {
                layoutsetting.setVisibility(View.VISIBLE);
                ivarrow.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
            }
            else {
                ivarrow.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                layoutsetting.setVisibility(View.GONE);
            }
        }if(view==navMaster){
            if (layoutMaster.getVisibility()==View.GONE) {
                layoutMaster.setVisibility(View.VISIBLE);
                masterIcon.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
            }
            else {
                masterIcon.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                layoutMaster.setVisibility(View.GONE);
            }
        }if(view==navReport){
            if (layoutReport.getVisibility()==View.GONE) {
                layoutReport.setVisibility(View.VISIBLE);
                reportIcon.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
            }
            else {
                reportIcon.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                layoutReport.setVisibility(View.GONE);
            }
        }if(view==navSales){
            if (layoutSales.getVisibility()==View.GONE) {
                layoutSales.setVisibility(View.VISIBLE);
                salesIcon.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
            }
            else {
                salesIcon.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                layoutSales.setVisibility(View.GONE);
            }
        }
    }

    private void setSettingChildListener(View view) {
        if(view.getId()==headerSetting.getId()){
            Log.v("TAGG","HEADER");
            addFragment(new HeaderFooterSettingFragment(),false);
            drawerLayout.closeDrawer(GravityCompat.START,true);
        }if(view.getId()==mainSetting.getId()){
            Log.v("TAGG","MAIN");
            Intent intent=new Intent(getApplicationContext(),MainSettingActivity.class);
            startActivity(intent);
            drawerLayout.closeDrawer(GravityCompat.START,true);
        }
    }

    private void setReportChildListener(View view) {
        if (saleBillReport.getId()==view.getId()){
            Log.v("TAGG","SASLES");
            addFragment(new SalesBillReportFragment(),false);
            drawerLayout.closeDrawer(GravityCompat.START,true);
        }if (deletedSalesBillReport.getId()==view.getId()){
            Log.v("TAG","DELTED");
            addFragment(new DeletedSalesBillReportFragment(),false);
            drawerLayout.closeDrawer(GravityCompat.START,true);
        }
    }

    private void setSalesChildListener(View view) {
        if(thumbnail.getId()==view.getId()){
            Log.v("TAGG","THUMBNAIL");
            addFragment(new ThumbnailFragment(),false);
            drawerLayout.closeDrawer(GravityCompat.START,true);
        }
        if(codewise.getId()==view.getId()){
            Log.v("TAGG","CODEWISE");
            addFragment(new CodewiseFragment(),false);
            drawerLayout.closeDrawer(GravityCompat.START,true);
        }
        if(tablebooking.getId()==view.getId()){
            Log.v("TAGG","TABLEBOOKING");
            addFragment(new TableFragment(),false);
            drawerLayout.closeDrawer(GravityCompat.START,true);
        }
    }

    private void setMasterChildListener(View view) {
        if(addItem.getId()==view.getId()){
            Log.v("TAGG","ITEM");
            addFragment(new ItemListFragment(),false);
            drawerLayout.closeDrawer(GravityCompat.START,true);
        }
        if(addCategory.getId()==view.getId()){
            Log.v("TAGG","CATEGORY");
            addFragment(new ListCategoryFragment(),false);
            drawerLayout.closeDrawer(GravityCompat.START,true);
        }
        if(addSupplier.getId()==view.getId()){
            Log.v("TAGG","SUPPLIER");
            addFragment(new SupplierListFragment(),false);
            drawerLayout.closeDrawer(GravityCompat.START,true);
        }
        if(addCustomer.getId()==view.getId()){
            Log.v("TAGG","CUSTOMER");
            addFragment(new CustomerListFragment(),false);
            drawerLayout.closeDrawer(GravityCompat.START,true);
        }
        if(addUnit.getId()==view.getId()){
            Log.v("TAGG","UNITS");
            addFragment(new ListUnitsFragment(),false);
            drawerLayout.closeDrawer(GravityCompat.START,true);
        }
        if(addTableList.getId()==view.getId()){
            Log.v("TAGG","TableList");
            addFragment(new TableListFragment(),false);
            drawerLayout.closeDrawer(GravityCompat.START,true);
        }
        if(addWaiterList.getId()==view.getId()){
            Log.v("TAGG","WaiterList");
            addFragment(new WaiterFragment(),false);
            drawerLayout.closeDrawer(GravityCompat.START,true);
        }
    }
}
