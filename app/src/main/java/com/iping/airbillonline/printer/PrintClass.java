package com.iping.airbillonline.printer;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.database.Cursor;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.iping.airbillonline.database.DatabaseHelper;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.Set;
import java.util.UUID;

public class PrintClass {
    private static PrintClass print;

    private Activity context;

    private Cursor res1, res2;

    private BluetoothAdapter bluetoothAdapter;
    private BluetoothSocket socket;
    private BluetoothDevice bluetoothDevice;

    private OutputStream outputStream;
    private InputStream inputStream;

    private Thread workerThread;
    private static byte[] ALIGNMENT_LEFT = new byte[]{27, 97, 0};
    private static byte[] ALIGNMENT_CENTER = new byte[]{27, 97, 1};
    private static byte[] ALIGNMENT_RIGHT = new byte[]{27, 97, 2};
    private byte[] readBuffer;
    private int readBufferPosition;
    private volatile boolean stopWorker;

    private String value = "";

    private DatabaseHelper db;

    private static String printerName;

    private PrintClass(Activity context, String printerName) {
        this.context = context;
        db = DatabaseHelper.getInstance(context);
        PrintClass.printerName = printerName;
        initPrinter(printerName);
    }

    public static PrintClass getInstance(Activity context, String printerNm) {
        if (print == null) {
            print = new PrintClass(context, printerNm);
        }
        if (printerNm != null && printerName != printerNm) {
            printerName = printerNm;
        }
        return print;
    }

    public boolean getStatus() {
        return bluetoothDevice == null;
    }

    public String getPrinterName() {
        return bluetoothDevice.getName();
    }


    public void IntentPrint(String txtvalue) {
        byte[] buffer = txtvalue.toString().getBytes();
        byte[] PrintHeader = getHeaderFooter();


        PrintHeader[3] = (byte) buffer.length;
        if (PrintHeader.length > 128) {
//            value += "\nValue is more than 128 size\n";
//            Toast.makeText(context, value, Toast.LENGTH_LONG).show();
        } else {
            try {
//                String text = "उपमा";
//                byte[] bytes = new byte[10];
//
//                String str = new String(bytes, Charset.forName("UTF-8"));
//                String str1 = new String(text.getBytes(), Charset.forName("UTF-8"));
                outputStream.write(txtvalue.getBytes());
                //socket.close();
            } catch (Exception ex) {
                value += ex.toString() + "\n" + "Exception IntentPrint \n";
//                Toast.makeText(context, value, Toast.LENGTH_LONG).show();
                Log.e("error3", value);
                Toast.makeText(context, "Please connect to printer", Toast.LENGTH_LONG).show();

            }
        }
    }

    public void initPrinter(String pairedDevice) {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        try {
            if (!bluetoothAdapter.isEnabled()) {
                Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                context.startActivityForResult(enableBluetooth, 0);
            }
            Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();
            if (pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {
                    if (device.getName().equals(printerName)) { //Note, you will need to change this to match the name of your device
                        Log.v("TAG", device.getAddress());
                        bluetoothDevice = device;
                        break;
                    }
                }
                UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb"); //Standard SerialPortService ID
                Method m = bluetoothDevice.getClass().getMethod("createRfcommSocket", new Class[]{int.class});
                socket = bluetoothDevice.createInsecureRfcommSocketToServiceRecord(uuid);// (BluetoothSocket) m.invoke(bluetoothDevice, 1);
                bluetoothAdapter.cancelDiscovery();
                socket.connect();
                beginListenForData();
            } else {
                value += "No Devices found";
//                Toast.makeText(context, value, Toast.LENGTH_LONG).show();
                Log.e("error1", value);
                Toast.makeText(context, "Please connect to printer", Toast.LENGTH_LONG).show();
                return;
            }
        } catch (Exception ex) {
            value += ex.toString() + "\n" + " InitPrinter \n";
            Log.e("error2", value);
            Toast.makeText(context, "Please connect to printer", Toast.LENGTH_LONG).show();
        }
    }

    public void beginListenForData() {
        try {
            outputStream = socket.getOutputStream();
            inputStream = socket.getInputStream();
            final Handler handler = new Handler();
            // this is the ASCII code for a newline character
            final byte delimiter = 10;
            stopWorker = false;
            readBufferPosition = 0;
            readBuffer = new byte[1024];
            workerThread = new Thread(new Runnable() {
                public void run() {
                    while (!Thread.currentThread().isInterrupted() && !stopWorker) {
                        try {
                            int bytesAvailable = inputStream.available();
                            if (bytesAvailable > 0) {
                                byte[] packetBytes = new byte[bytesAvailable];
                                inputStream.read(packetBytes);
                                for (int i = 0; i < bytesAvailable; i++) {
                                    byte b = packetBytes[i];
                                    if (b == delimiter) {
                                        byte[] encodedBytes = new byte[readBufferPosition];
                                        System.arraycopy(
                                                readBuffer, 0,
                                                encodedBytes, 0,
                                                encodedBytes.length
                                        );
                                        // specify US-ASCII encoding
                                        final String data = new String(encodedBytes, "US-ASCII");
                                        readBufferPosition = 0;
                                        // tell the user data were sent to bluetooth printer device
                                        handler.post(new Runnable() {
                                            public void run() {
                                                Log.d("e", data);
                                            }
                                        });
                                    } else {
                                        readBuffer[readBufferPosition++] = b;
                                    }
                                }
                            }
                        } catch (IOException ex) {
                            stopWorker = true;
                        }
                    }
                }
            });
            workerThread.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public byte[] getHeaderFooter() {
        StringBuffer buffer1 = new StringBuffer();
        res1 = db.getAllHeaderFooter();
        String h1 = null, h1size = "10", h2 = null, h2size = "9", h3 = null, h3size = "8", h4 = null, h4size = "7", h5 = null, h5size = "6", f1 = null, f1size = "10", f2 = null, f2size = "10", f3 = null, f3size = "10", f4 = null, f4size = "10", f5 = null, f5size = "10";
        if (res1.getCount()>0) {
            while (res1.moveToNext()) {
                h1 = res1.getString(1);
                h1size = res1.getString(2);
                h2 = res1.getString(3);
                h2size = res1.getString(4);
                h3 = res1.getString(5);
                h3size = res1.getString(6);
                h4 = res1.getString(7);
                h4size = res1.getString(8);
                h5 = res1.getString(9);
                h5size = res1.getString(10);
                f1 = res1.getString(11);
                f1size = res1.getString(12);
                f2 = res1.getString(13);
                f2size = res1.getString(14);
                f3 = res1.getString(15);
                f3size = res1.getString(16);
                f4 = res1.getString(17);
                f4size = res1.getString(18);
                f5 = res1.getString(19);
                f5size = res1.getString(20);
            }
        }
        buffer1.append("\n" + h1 + "\n" + h2 + "\n" + h3 + "\n" + h4 + "\n" + h5 + "\n" + f1 + "\n" + f2 + "\n" + f3 + "\n" + f4 + "\n" + f5);
        return buffer1.toString().getBytes();
    }
}