package com.iping.airbillonline;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.iping.airbillonline.database.DatabaseHelper;
import com.iping.airbillonline.printer.PrintClass;
import com.iping.airbillonline.utils.HeadFootSetting;
import com.iping.airbillonline.utils.SessionManager;
import com.iping.airbillonline.utils.SyncData;
import com.iping.airbillonline.utils.ThreadExecutor;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class MainSettingActivity extends AppCompatActivity {

    private RadioGroup rg_bill_printer;
    private RadioButton rb_bill_printer;
    private RadioGroup rg_gst_printer;
    private RadioButton rb_gst_printer;
    private RadioGroup rg_select_printer;
    private RadioButton rb_select_printer;
    private RadioGroup rg_multiple_printing;
    private RadioButton rb_multiple_printing;
    private RadioGroup rg_with_bill_printing;
    private RadioButton rb_with_bill_printing;
    private RadioGroup rg_reset_bill_number;
    private RadioButton rb_reset_bill_number;

    private Spinner printSelector;
    private LinearLayout ll_export;
    private LinearLayout ll_import;

    private DatabaseHelper db;
    private BluetoothAdapter bluetoothAdapter;
    private ThreadExecutor executor;
    private SessionManager manager;
    private HeadFootSetting headFootSetting;

    private MainSettingActivity activity=this;

    private String selectedprinter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        db=DatabaseHelper.getInstance(getApplicationContext());
        manager=SessionManager.getInstance(getApplicationContext());
        executor=ThreadExecutor.getInstance();
        headFootSetting = new HeadFootSetting(getApplicationContext());

        setContentView(R.layout.activity_main_setting);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        rg_bill_printer = findViewById(R.id.rg_bill_size);
        rg_gst_printer = findViewById(R.id.rg_gst_size);
        rg_multiple_printing = findViewById(R.id.rg_multiple_print);
        rg_with_bill_printing= findViewById(R.id.rg_bill_printing_popup);
        rg_reset_bill_number= findViewById(R.id.rg_reset_bill);
        ll_export= findViewById(R.id.ll_export);
        ll_import= findViewById(R.id.ll_import);
        printSelector = findViewById(R.id.print_selector);

        if (manager.getPrintOption().equals("2 inch"))
            ((RadioButton) rg_bill_printer.getChildAt(0)).setChecked(true);
        else
            ((RadioButton) rg_bill_printer.getChildAt(1)).setChecked(true);

        if (manager.getGstOption().equals("GST Enable"))
            ((RadioButton) rg_gst_printer.getChildAt(0)).setChecked(true);
        else
            ((RadioButton) rg_gst_printer.getChildAt(1)).setChecked(true);

        if (manager.getMultipleOption().equals("Yes"))
            ((RadioButton) rg_multiple_printing.getChildAt(0)).setChecked(true);
        else
            ((RadioButton) rg_multiple_printing.getChildAt(1)).setChecked(true);

        if (manager.getBillWithPrint().equals("Yes"))
            ((RadioButton) rg_with_bill_printing.getChildAt(0)).setChecked(true);
        else
            ((RadioButton) rg_with_bill_printing.getChildAt(1)).setChecked(true);

        if (manager.getResetBill().equals("Yes"))
            ((RadioButton) rg_reset_bill_number.getChildAt(0)).setChecked(true);
        else
            ((RadioButton) rg_reset_bill_number.getChildAt(1)).setChecked(true);

        final String select=manager.getPrinter();
        selectedprinter=select;

        final List<String> list = new ArrayList<>();
        if(!select.equals("Device"))
            list.add(0, select);
        else
            list.add(0, "Device");

//        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
//
//        if(!bluetoothAdapter.isEnabled()) {
//            Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
//            startActivityForResult(enableBluetooth, 0);
//        }

        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();
                if(pairedDevices.size() > 0) {
                    for (BluetoothDevice device : pairedDevices) {
                        Log.v("TAG",device.getAddress());
                        list.add(device.getName());
                    }
                }
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if (selectedprinter!=null)
                            list.remove(selectedprinter);
                        ArrayAdapter adapter = new ArrayAdapter<>(getApplicationContext(),
                                android.R.layout.simple_spinner_dropdown_item, list);
                        printSelector.setAdapter(adapter);
                    }
                });
            }
        };
//       executor.execute(runnable);

        printSelector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selected = (String) parent.getItemAtPosition(position);
                if (!selected.equals("Device")) {
                    if (!selectedprinter.equals(selected)) {
                        PrintClass printClass = PrintClass.getInstance(activity, selected);
                        printClass.initPrinter(selected);
                        selectedprinter=selected;
                        Toast.makeText(getApplicationContext(), selected, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Button bt_submit_bill_screen = findViewById(R.id.bt_submit_bill_screen);
        bt_submit_bill_screen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int selectedId = rg_bill_printer.getCheckedRadioButtonId();
                rb_bill_printer = findViewById(selectedId);
                int gstid = rg_gst_printer.getCheckedRadioButtonId();
                rb_gst_printer = findViewById(gstid);

                int selectmultipleprint = rg_multiple_printing.getCheckedRadioButtonId();
                rb_multiple_printing = findViewById(selectmultipleprint);

                int selectwithprint_or_withoutprint = rg_with_bill_printing.getCheckedRadioButtonId();
                rb_with_bill_printing = findViewById(selectwithprint_or_withoutprint);

                int reset_bill = rg_reset_bill_number.getCheckedRadioButtonId();
                rb_reset_bill_number = findViewById(reset_bill);

                headFootSetting.setBill("\n" + rb_bill_printer.getText().toString() + "\n" + selectedprinter + "\n" + rb_gst_printer.getText().toString()
                        + "\n" + rb_with_bill_printing.getText().toString()+ "\n" + rb_multiple_printing.getText().toString()+"\n" + rb_reset_bill_number.getText().toString());
                manager.setPrintOption(rb_bill_printer.getText().toString());
                manager.setGstOption(rb_gst_printer.getText().toString());
                manager.setPrinter(selectedprinter);
                manager.setMultipleOption(rb_multiple_printing.getText().toString());
                manager.setBillWithPrint(rb_with_bill_printing.getText().toString());
                manager.setResetBill(rb_reset_bill_number.getText().toString());
                Intent intent=new Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);
            }
        });
        Button btCancel = findViewById(R.id.bt_cancel_tax);
        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);
            }
        });

        ll_export.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SyncData syncData=new SyncData(getApplicationContext());
                syncData.getcount();
            }
        });

        ll_import.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SyncData syncData=new SyncData(getApplicationContext());
                syncData.uploaddatabilldetails();
                syncData.uploaddata();
            }
        });
    }

    public void onShowPairedPrinters(View view) {

    }
}
