package com.iping.airbillonline.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.iping.airbillonline.R;
import com.iping.airbillonline.database.DatabaseHelper;
import com.iping.airbillonline.viewmodel.SalesBillReportViewModel;

import org.w3c.dom.Text;

import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;

public class CustomDialog {

    public static void customMessageBoxSplasH(int type, String strmsg, final Activity context){
        final Dialog dialog1=new Dialog(context);
        dialog1.setContentView(R.layout.error_layout);
        Objects.requireNonNull(dialog1.getWindow()).getAttributes().windowAnimations=type;
        dialog1.setCancelable(false);
        dialog1.show();

        TextView tv_errortext=(TextView)dialog1.findViewById(R.id.tv_errortext);
        tv_errortext.setText(strmsg);

        final Timer timer2 = new Timer();
        timer2.schedule(new TimerTask() {
            public void run() {
                dialog1.dismiss();
                timer2.cancel(); //this will cancel the timer of the system
                context.finish();
            }
        }, 5000);
    }

//    public static void RemoveMessageBoxbill(final String sr_no, final String bill_no, String title, String msg, String status, final Context activity){
//        final SalesBillReportViewModel salesBillReportViewModel= new ViewModelProvider((ViewModelStoreOwner) Objects.requireNonNull(activity)).get(SalesBillReportViewModel.class);
//        final DatabaseHelper db = DatabaseHelper.getInstance(activity);
//        final Dialog dialog = new Dialog(activity);
//        dialog.setContentView(R.layout.warning_layout);
//        Button bt_ok = dialog.findViewById(R.id.bt_ok);
//        Button bt_cancel1 = dialog.findViewById(R.id.bt_cancel);
//        TextView tv_title = dialog.findViewById(R.id.tv_title);
//        TextView tv_errortext = dialog.findViewById(R.id.tv_errortext);
//        tv_title.setText(title);
//        tv_errortext.setText(msg);
//
//        bt_ok.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(activity, "Bill Deleted!!!", Toast.LENGTH_SHORT).show();
//                db.deleteSaleBill(sr_no,bill_no,"");
//                dialog.dismiss();
//               // salesBillReportViewModel.init(activity,fromdate,todate);
//            }
//        });
//
//
//
//
//        bt_cancel1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dialog.dismiss();
//            }
//        });
//
//        dialog.setCancelable(false);
//        dialog.show();
//    }


    public static void RemoveMessageBox(final String cat_id, final String catname, String title, String msg, final Context context) {
        final Dialog dialog = new Dialog(context);
        final DatabaseHelper db = DatabaseHelper.getInstance(context);
        dialog.setContentView(R.layout.warning_layout1);
        Button bt_ok = (Button) dialog.findViewById(R.id.bt_ok);
        Button bt_cancel1 = (Button) dialog.findViewById(R.id.bt_cancel);
        TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
        TextView tv_errortext = (TextView) dialog.findViewById(R.id.tv_errortext);
        final EditText et_remark=(EditText)dialog.findViewById(R.id.et_remark);
        tv_title.setText(title);
        tv_errortext.setText(msg);
        bt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(et_remark.getText().toString().equals("")){
                    et_remark.setError("Please Enter Remark");
                }else if(!et_remark.getText().toString().matches("[a-zA-Z ]*")){
                    et_remark.setError("Enter Characters only");
                }else {
                    Toast.makeText(context, "Bill Deleted!!! "+cat_id+" "+catname, Toast.LENGTH_SHORT).show();
                    db.deleteSaleBill(cat_id, catname,et_remark.getText().toString());
                    dialog.dismiss();
                }

            }
        });
        bt_cancel1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }
}
