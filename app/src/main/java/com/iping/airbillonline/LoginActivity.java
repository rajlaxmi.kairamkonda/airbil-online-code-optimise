package com.iping.airbillonline;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.iping.airbillonline.database.DatabaseHelper;
import com.iping.airbillonline.utils.Config;
import com.iping.airbillonline.utils.IRequestListener;
import com.iping.airbillonline.utils.Message;
import com.iping.airbillonline.utils.SessionManager;
import com.iping.airbillonline.utils.TokenService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements IRequestListener {

    Button bt_login,bt_cancel;
    EditText ename,epass;
    Context context;
    private DatabaseHelper db;
    private SessionManager manager;
    int login_flag=2;
    int emp_id,lid,cid;
    String InstallDate="",ExpireDate="",status="";
    private String token="";
    private TokenService tokenService;
    private static final String TAG = "SplashActivity";
    int login_active=0;

    //Context context;
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);
        db= DatabaseHelper.getInstance(getApplicationContext());
        manager=SessionManager.getInstance(getApplicationContext());
        //java code
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitNetwork().build());

        try {
            //Get token from Firebase
            FirebaseMessaging.getInstance().subscribeToTopic("test");
            final String token = FirebaseInstanceId.getInstance().getToken();
            Log.d(TAG, "Token: " + token);

            //Call the token service to save the token in the database
            tokenService = new TokenService(this, this);
            tokenService.registerTokenInDB(token);

        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(getApplicationContext(),"Error="+e, Toast.LENGTH_LONG).show();
        }

        bt_login= findViewById(R.id.bt_login);
        bt_cancel= findViewById(R.id.bt_cancel);
        ename= findViewById(R.id.et_username);
        epass= findViewById(R.id.et_password);

        manager.setPrintOption("2 inch");

        bt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                try{
                    if(ename.getText().toString().equals("")||epass.getText().toString().equals(""))
                    {
                        Message.message(getApplicationContext(),"Please enter Username/Password ");
                    }else {
                        String HttpUrl = Config.hosturl+"checklogin.php";
                        Log.d("URL",HttpUrl);
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String ServerResponse) {

                                        Log.d("Server Response",ServerResponse);
//                                    progressDialog.dismiss();

                                        try {
                                            //getting the whole json object from the response
                                            JSONObject obj = new JSONObject(ServerResponse);
                                            status = obj.getString("status");
                                            token=manager.getToken();
                                            if(status.equals("success"))
                                            {
                                                login_active= Integer.parseInt(obj.getString("login_active"));
                                                if(login_active==0) {
                                                    sync_times(obj.getString("cid"), obj.getString("lid"), obj.getString("emp_id"));
                                                    UpdateToken(ename.getText().toString(), epass.getText().toString(), getApplicationContext(), token);
                                                    manager.setLid(obj.getString("lid"));
                                                    manager.setCid(obj.getString("cid"));
                                                    manager.setEmpID(obj.getString("emp_id"));
                                                    manager.setEmployeeCode(obj.getString("employee_code"));
                                                    manager.setUsername(ename.getText().toString());
                                                    manager.setPanel("ShopOwner");
                                                    manager.setCatDatetime("0000-00-00 00:00:00");
                                                    manager.setUnitDatetime("0000-00-00 00:00:00");
                                                    manager.setSupDatetime("0000-00-00 00:00:00");
                                                    manager.setItemDatetiime("0000-00-00 00:00:00");
                                                    manager.setCustDatetime("0000-00-00 00:00:00");
                                                    manager.setPurcDatetime("0000-00-00 00:00:00");
                                                    manager.setTableDatetime("0000-00-00 00:00:00");
                                                    manager.setWaiterDatetime("0000-00-00 00:00:00");
                                                    sync_times(obj.getString("cid"), obj.getString("lid"), obj.getString("emp_id"));
                                                    Toast.makeText(getApplicationContext(), "Login Successfully", Toast.LENGTH_LONG).show();
                                                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                                    startActivity(i);
                                                    finish();
                                                }else {
                                                    Toast.makeText(getApplicationContext(),"Please Activate Your Account", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                            else {
                                                Toast.makeText(getApplicationContext(), obj.getString("status")+": Invalid Username and Password", Toast.LENGTH_LONG).show();
                                            }

                                        }catch (JSONException e) {
                                            e.printStackTrace();
                                            Toast.makeText(getApplicationContext(), "Error2=" + e, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError volleyError) {
                                        Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                                    }
                                }) {
                            @Override
                            protected Map<String, String> getParams() {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("username", ename.getText().toString());
                                params.put("password", epass.getText().toString());
                                return params;
                            }
                        };

                        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                        requestQueue.add(stringRequest);

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                homeIntent.addCategory( Intent.CATEGORY_HOME );
                homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
                finish();
            }
        });
    }

    @Override
    public void onComplete() {

    }

    @Override
    public void onError(String message) {

    }

    public void UpdateToken(final String Username, final String Password, final Context context, final String token){
        try {
            RequestQueue queue = Volley.newRequestQueue(context);
            String url = Config.hosturl+"callurl.php";

            // Request a string response from the provided URL.
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if(response.equals("Token updated successfully")){
                                manager.setTokenFlag("1");
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(context, "Something Went To Wrong!", Toast.LENGTH_LONG).show();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("mobile_no", Username);
                    params.put("password", Password);
                    params.put("token", token);
                    return params;
                }

            };
            // Add the request to the RequestQueue.
            queue.add(stringRequest);
        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(context,"Error="+e, Toast.LENGTH_LONG).show();
        }
    }
    public void sync_times(final String tcid, final String tlid, final String tempid){
        try {
            String HttpUrltime = Config.hosturl+"sync_time_api.php";
            Log.d("URL", HttpUrltime);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrltime,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject objtime = new JSONObject(response);

                                JSONArray tutorialsArraytime = objtime.getJSONArray("data");
                                for (int i = 0; i < tutorialsArraytime.length(); i++) {
                                    JSONObject timeObject = tutorialsArraytime.getJSONObject(i);
//                                    editor.putString("upload_interval",timeObject.getString("upload_interval"));
//                                    editor.putString("download_interval",timeObject.getString("download_interval"));
//                                    editor.commit();
                                    manager.setUploadInterval(timeObject.getString("upload_interval"));
                                    manager.setDownloadInterval(timeObject.getString("download_interval"));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("lid", "" + tlid);
                    params.put("cid", "" + tcid);
                    params.put("empid", "" + tempid);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static final int TIME_DELAY = 2000;
    private static long back_pressed;
    @Override
    public void onBackPressed() {
        if (back_pressed + TIME_DELAY > System.currentTimeMillis()) {
            //super.onBackPressed();
            Intent homeIntent = new Intent(Intent.ACTION_MAIN);
            homeIntent.addCategory( Intent.CATEGORY_HOME );
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(homeIntent);
            finish();
        } else {
            Toast.makeText(getBaseContext(), "Press once again to exit!",
                    Toast.LENGTH_SHORT).show();
        }
        back_pressed = System.currentTimeMillis();
    }
}