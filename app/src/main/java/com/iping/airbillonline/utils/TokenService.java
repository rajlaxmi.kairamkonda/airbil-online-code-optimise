package com.iping.airbillonline.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class TokenService {

    private static final String TAG = "TokenService";
    //public static final String BACKEND_SERVER_IP = "103.71.64.153:85";
    public static final String BACKEND_URL_BASE = Config.hosturl;
    private Context context;
    private IRequestListener listener;
    private SessionManager manager;

    public TokenService(Context context, Context listener) {
        this.context = context;
        this.listener = (IRequestListener) listener;
        manager=SessionManager.getInstance(context);
    }

    public void registerTokenInDB(final String token) {
        // The call should have a back off strategy
//        final SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
//        final SharedPreferences.Editor editor = pref.edit();
//        editor.putString("Token",token);
//        editor.commit();
        manager.setToken(token);
    }
}
