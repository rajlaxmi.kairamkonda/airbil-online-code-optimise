package com.iping.airbillonline.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class HeadFootSetting {

    Context context;

    private String bill;

    private String shopname;
    private String thankmsg;

    public String getH1() {
        h1=sharedPreferences.getString("h1","");
        return h1;
    }

    public void setH1(String h1) {
        this.h1 = h1;
        sharedPreferences.edit().putString("h1",h1).commit();
    }

    private String h1;

    public String getF1() {
        f1=sharedPreferences.getString("f1","");
        return f1;
    }

    public void setF1(String f1) {
        this.f1 = f1;
        sharedPreferences.edit().putString("f1",f1).commit();
    }

    private String f1;


    public String getShopname() {
        return shopname;
    }

    public void setShopname(String shopname) {
        this.shopname = shopname;
        sharedPreferences.edit().putString("shopname",shopname).commit();
    }

    public String getThankmsg() {
        return thankmsg;
    }

    public void setThankmsg(String thankmsg) {
        this.thankmsg = thankmsg;
        sharedPreferences.edit().putString("thankmsg",thankmsg).commit();
    }

    public String getBill() {
        return bill;
    }

    public void setBill(String bill) {
        this.bill = bill;
        sharedPreferences.edit().putString("bill",bill).commit();
    }
    SharedPreferences sharedPreferences;

    public HeadFootSetting(Context context){
        this.context=context;
        sharedPreferences=context.getSharedPreferences("bill", Context.MODE_PRIVATE);
    }
}