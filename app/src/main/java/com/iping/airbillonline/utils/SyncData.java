package com.iping.airbillonline.utils;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.apache.http.HttpResponse;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.iping.airbillonline.database.DatabaseHelper;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SyncData {

    private DatabaseHelper db;
    private Context context;
    private SessionManager manager;

    private String rescat="";
    private String postdata="";
    private String unitpostdata="";
    private String itempostdata="";
    private String suppostdata="";
    private String custpostdata="";
    private String prchpostdata="";

    private String cat_datetime="";
    private String unit_datetime="";
    private String item_datetime="";
    private String sup_datetime="";
    private String cust_datetime="";
    private String purc_datetime="";
    private String table_datetime="";
    private String waiter_datetime="";

    private String prev_cat_datetime="";
    private String prev_unit_datetime="";
    private String prev_item_datetime="";
    private String prev_sup_datetime="";
    private String prev_cust_datetime="";
    private String prev_purc_datetime="";
    private String prev_table_datetime="";
    private String prev_waiter_datetime="";



    private int cat_total=0;
    private int unit_total=0;
    private int item_total=0;
    private int sup_total=0;
    private int cust_total=0;
    private int invt_total=0;
    private int table_total=0;
    private int waiter_total=0;

    public SyncData(Context context){
        db=DatabaseHelper.getInstance(context);
        this.context=context;
        manager=SessionManager.getInstance(context);
    }

    public void point_of_contact(){
        final String lid=manager.getLid();
        final String cid=manager.getCid();
        final String empid=manager.getEmpId();

        final List<NameValuePair> pointconatctlist=new ArrayList<>();

        try {
            String HttpUrlpointcontact = Config.hosturl+"point_of_contact_api.php";
            Log.d("URL", HttpUrlpointcontact);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrlpointcontact,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject obj = new JSONObject(response);
                                JSONObject point_contactsyncid = new JSONObject();

                                JSONArray tutorialsArray = obj.getJSONArray("data");
                                for (int i = 0; i < tutorialsArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject pointObject = tutorialsArray.getJSONObject(i);
                                    db.Syncpointcontact(pointObject.getString("id"),pointObject.getString("point_of_contact"),pointObject.getString("is_active"),pointObject.getString("cid"),pointObject.getString("lid"),pointObject.getString("emp_id"),pointObject.getString("sync_flag"),pointObject.getString("created_at"),pointObject.getString("updated_at"));
                                    point_contactsyncid.put("" + i, pointObject.getString("id"));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(context, "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Toast.makeText(context, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("lid", "" + lid);
                    params.put("cid", "" + cid);
                    params.put("empid", "" + empid);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String readResponse(HttpResponse res) {
        InputStream is = null;
        String return_text = "";
        try {
            is = res.getEntity().getContent();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));
            String line = "";
            StringBuffer sb = new StringBuffer();
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }

            return_text = sb.toString();
        } catch (Exception e) {

        }
        return return_text;
    }

    public void insert_cat(final int firstcatcnt, final int seconcatcnt) {
        final String lid=manager.getLid();
        final String cid=manager.getCid();
        final String empid=manager.getEmpId();
        final List<NameValuePair> list=new ArrayList<NameValuePair>();

        try {
            String HttpUrl = Config.hosturl+"category_api.php";
            Log.d("URL", HttpUrl);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject obj = new JSONObject(response);
                                JSONObject syncid = new JSONObject();
                                //Toast.makeText(getApplicationContext(),"Rsponse Cat"+obj,Toast.LENGTH_LONG).show();
                                JSONArray tutorialsArray = obj.getJSONArray("data");
                                for (int i = 0; i < tutorialsArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject catObject = tutorialsArray.getJSONObject(i);
                                    db.SyncCategory(catObject.getString("cat_name"), catObject.getString("cat_id"), catObject.getString("cat_description"), catObject.getString("cat_image"),catObject.getString("type_id"),catObject.getString("created_at"),catObject.getString("updated_at"),catObject.getString("is_active"),catObject.getString("cid"),catObject.getString("lid"),catObject.getString("emp_id"));
                                    syncid.put("" + i, catObject.getString("cat_id"));
                                }
                                prev_cat_datetime=cat_datetime;
                                manager.setCatDatetime(prev_cat_datetime);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(context, "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

//                                    progressDialog.dismiss();

                            Toast.makeText(context, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("firstlimt", "" + firstcatcnt);
                    params.put("secondlimit", "" + seconcatcnt);
                    params.put("lid", "" + lid);
                    params.put("cid", "" + cid);
                    params.put("empid", "" + empid);
                    params.put("cat_datetime", "" + prev_cat_datetime);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(context);

            requestQueue.add(stringRequest);

        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insert_unit(final int firstcatcnt, final int seconcatcnt) {
        final String lid=manager.getLid();
        final String cid=manager.getCid();
        final String empid=manager.getEmpId();
        final List<NameValuePair> unitlist=new ArrayList<NameValuePair>();

        try {
            String HttpUrl = Config.hosturl+"unit_api.php";
            Log.d("URL", HttpUrl);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject obj = new JSONObject(response);
                                JSONObject syncid = new JSONObject();

                                JSONArray tutorialsArray = obj.getJSONArray("data");
                                for (int i = 0; i < tutorialsArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject unitObject = tutorialsArray.getJSONObject(i);
                                    //Toast.makeText(getApplicationContext(),""+unitObject.getString("Unit_name")+""+unitObject.getString("Unit_Id")+""+unitObject.getString("Unit_Taxvalue")+""+unitObject.getString("created_at")+""+unitObject.getString("is_active")+""+unitObject.getString("cid")+""+unitObject.getString("lid")+""+unitObject.getString("emp_id"),Toast.LENGTH_SHORT).show();
                                    db.SyncUnit(unitObject.getString("Unit_name"), unitObject.getString("Unit_Id"),unitObject.getString("Unit_Taxvalue"),unitObject.getString("created_at"),unitObject.getString("is_active"),unitObject.getString("cid"),unitObject.getString("lid"),unitObject.getString("emp_id"));
                                    syncid.put("" + i, unitObject.getString("Unit_Id"));
                                }
                                prev_unit_datetime=unit_datetime;
                                manager.setUnitDatetime(prev_unit_datetime);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(context, "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

//                                    progressDialog.dismiss();

                            Toast.makeText(context, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("firstlimt", "" + firstcatcnt);
                    params.put("secondlimit", "" + seconcatcnt);
                    params.put("lid", "" + lid);
                    params.put("cid", "" + cid);
                    params.put("empid", "" + empid);
                    params.put("unit_datetime", "" + prev_unit_datetime);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insert_item(final int firstcatcnt, final int seconcatcnt) {
        final String lid=manager.getLid();
        final String cid=manager.getCid();
        final String empid=manager.getEmpId();
        final List<NameValuePair> itemlist=new ArrayList<NameValuePair>();

        try {
            String HttpUrl = Config.hosturl+"item_api.php";
            Log.d("URL", HttpUrl);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject obj = new JSONObject(response);
                                JSONObject syncid = new JSONObject();

                                JSONArray tutorialsArray = obj.getJSONArray("data");
                                for (int i = 0; i < tutorialsArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject catObject = tutorialsArray.getJSONObject(i);
                                    db.SyncItem(catObject.getString("item_id"),catObject.getString("item_name"), catObject.getString("item_id"), catObject.getString("item_rate"),
                                            catObject.getString("item_dis"),catObject.getString("item_disrate"),catObject.getString("item_tax"),
                                            catObject.getString("item_taxvalue"),catObject.getString("item_final_rate"),catObject.getString("item_category"),
                                            catObject.getString("item_units"),catObject.getString("item_stock"),catObject.getString("item_barcode"),
                                            catObject.getString("item_hsncode"),catObject.getString("is_active"),catObject.getString("cid"),catObject.getString("lid"),
                                            catObject.getString("emp_id"),catObject.getString("sub_emp_id"),catObject.getString("item_code"));
                                    syncid.put("" + i, catObject.getString("item_id"));
                                }
                                prev_item_datetime=item_datetime;
                                manager.setItemDatetiime(prev_item_datetime);
                           } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(context, "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
//                                    progressDialog.dismiss();

                            Toast.makeText(context, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("firstlimt", "" + firstcatcnt);
                    params.put("secondlimit", "" + seconcatcnt);
                    params.put("lid", "" + lid);
                    params.put("cid", "" + cid);
                    params.put("empid", "" + empid);
                    params.put("item_datetime", "" + prev_item_datetime);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(context);

            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insert_sup(final int firstcatcnt, final int seconcatcnt) {
        final String lid=manager.getLid();
        final String cid=manager.getCid();
        final String empid=manager.getEmpId();
        final List<NameValuePair> suplist=new ArrayList<NameValuePair>();

        try {
            String HttpUrlsup = Config.hosturl+"supllier_api.php";
            Log.d("URL", HttpUrlsup);
            StringRequest stringRequestsup = new StringRequest(Request.Method.POST, HttpUrlsup,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject obj = new JSONObject(response);
                                JSONObject syncid = new JSONObject();

                                JSONArray tutorialsArray = obj.getJSONArray("data");
                                for (int i = 0; i < tutorialsArray.length(); i++) {
                                    JSONObject supObject = tutorialsArray.getJSONObject(i);
                                    db.SyncSup(supObject.getString("sup_id"), supObject.getString("sup_name"),supObject.getString("sup_address"),supObject.getString("sup_mobile_no"),supObject.getString("sup_email_id"),supObject.getString("sup_gst_no"),supObject.getString("created_at"),supObject.getString("updated_at"),supObject.getString("is_active"),supObject.getString("cid"),supObject.getString("lid"),supObject.getString("emp_id"));
                                    syncid.put("" + i, supObject.getString("sup_id"));
                                }
                                prev_sup_datetime=sup_datetime;
                                manager.setSupDatetime(prev_sup_datetime);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(context, "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

//                                    progressDialog.dismiss();

                            Toast.makeText(context, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("firstlimt", "" + firstcatcnt);
                    params.put("secondlimit", "" + seconcatcnt);
                    params.put("lid", "" + lid);
                    params.put("cid", "" + cid);
                    params.put("empid", "" + empid);
                    params.put("sup_datetime", "" + prev_sup_datetime);
                    return params;
                }
            };

            RequestQueue requestQueuesup = Volley.newRequestQueue(context);

            requestQueuesup.add(stringRequestsup);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insert_cust(final int firstcatcnt, final int seconcatcnt) {
        final String lid=manager.getLid();
        final String cid=manager.getCid();
        final String empid=manager.getEmpId();
        final List<NameValuePair> list=new ArrayList<NameValuePair>();

        try {
            String HttpUrlcust = Config.hosturl+"customer_api.php";
            Log.d("URL", HttpUrlcust);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrlcust,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject obj = new JSONObject(response);
                                JSONObject syncid = new JSONObject();

                                JSONArray tutorialsArray = obj.getJSONArray("data");
                                for (int i = 0; i < tutorialsArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject custObject = tutorialsArray.getJSONObject(i);
                                    db.SyncCust(custObject.getString("cust_id"), custObject.getString("cust_CompanyName"),custObject.getString("cust_name"),custObject.getString("address"),custObject.getString("mobile_no"),custObject.getString("email_id"),custObject.getString("cust_companyId_or_GST"),custObject.getString("created_at"),custObject.getString("updated_at"),custObject.getString("is_active"),custObject.getString("cid"),custObject.getString("lid"),custObject.getString("emp_id"));
                                    syncid.put("" + i, custObject.getString("cust_id"));
                                }
                                prev_cust_datetime=cust_datetime;
                                manager.setCustDatetime(prev_cust_datetime);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(context, "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

//                                    progressDialog.dismiss();

                            Toast.makeText( context, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("firstlimt", "" + firstcatcnt);
                    params.put("secondlimit", "" + seconcatcnt);
                    params.put("lid", "" + lid);
                    params.put("cid", "" + cid);
                    params.put("empid", "" + empid);
                    params.put("cust_datetime", "" + prev_cust_datetime);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(context);

            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insert_invt(final int firstcatcnt, final int seconcatcnt) {
        final String lid=manager.getLid();
        final String cid=manager.getCid();
        final String empid=manager.getEmpId();
        final List<NameValuePair> purchlist=new ArrayList<NameValuePair>();

        try {
            String HttpUrlinvt = Config.hosturl+"purchase_api.php";
            Log.d("URL", HttpUrlinvt);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrlinvt,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject obj = new JSONObject(response);
                                JSONObject syncid = new JSONObject();

                                JSONArray tutorialsArray = obj.getJSONArray("data");
                                for (int i = 0; i < tutorialsArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject invtObject = tutorialsArray.getJSONObject(i);
                                    db.Syncinvetory(invtObject.getString("inventoryid"),invtObject.getString("inventorysupid"),invtObject.getString("inventoryitemid"),invtObject.getString("inventoryitemquantity"),invtObject.getString("inventorystatus"),invtObject.getString("created_at"),invtObject.getString("updated_at"),invtObject.getString("isactive"),invtObject.getString("cid"),invtObject.getString("lid"),invtObject.getString("emp_id"));
                                    syncid.put("" + i, invtObject.getString("inventoryid"));
                                }
                                prev_purc_datetime=purc_datetime;
                                manager.setPurcDatetime(prev_purc_datetime);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(context, "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
//                                    progressDialog.dismiss();
                            Toast.makeText(context, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("firstlimt", "" + firstcatcnt);
                    params.put("secondlimit", "" + seconcatcnt);
                    params.put("lid", "" + lid);
                    params.put("cid", "" + cid);
                    params.put("empid", "" + empid);
                    params.put("purc_datetime", "" + prev_purc_datetime);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(context);

            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void getcount(){
        final String lid=manager.getLid();
        final String cid=manager.getCid();
        final String empid=manager.getEmpId();
        Log.v("TAGG",lid);
        Log.v("TAGG",cid);
        Log.v("TAG",empid);
        try {
            String HttpUrl = Config.hosturl+"cat_total.php";
            Log.d("URL", HttpUrl);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject obj = new JSONObject(response);

                                JSONArray limitsize = obj.getJSONArray("tbl_category");
                                 Log.d("Data=",""+limitsize);

                                JSONObject linitobject = limitsize.getJSONObject(0);
                                cat_total = Integer.parseInt(linitobject.getString("count"));
                                cat_datetime = linitobject.getString("cat_datetime");
                                // Toast.makeText(getApplicationContext(),""+linitobject.getString("cat_datetime"),Toast.LENGTH_LONG).show();
                                //Toast.makeText(getApplicationContext(),"tbl_category="+cat_total,Toast.LENGTH_SHORT).show();
                                int first_cat_limit=0,second_cat_limit=Config.limit;
                                for(int i=0;i<cat_total;i++){
                                    insert_cat(first_cat_limit,second_cat_limit);
                                    first_cat_limit=second_cat_limit;
                                    second_cat_limit=second_cat_limit+Config.limit;
                                }

                                JSONArray limitsizeunit = obj.getJSONArray("tbl_unit");
                                JSONObject linitobjectunit = limitsizeunit.getJSONObject(0);
                                unit_total = Integer.parseInt(linitobjectunit.getString("count"));
                                unit_datetime = linitobjectunit.getString("unit_datetime");
                                //Toast.makeText(getApplicationContext(),"tbl_unit="+unit_total,Toast.LENGTH_SHORT).show();
                                int first_unit_limit=0,second_unit_limit=Config.limit;
                                for(int i=0;i<unit_total;i++){
                                    insert_unit(first_unit_limit,second_unit_limit);
                                    first_unit_limit=second_unit_limit;
                                    second_unit_limit=second_unit_limit+Config.limit;
                                }

                                JSONArray limitsizeitem = obj.getJSONArray("tbl_item");
                                JSONObject linitobjectitem = limitsizeitem.getJSONObject(0);
                                item_total = Integer.parseInt(linitobjectitem.getString("count"));
                                item_datetime = linitobjectitem.getString("item_datetime");
                                //Toast.makeText(getApplicationContext(),"tbl_item="+item_total,Toast.LENGTH_SHORT).show();
                                int first_item_limit=0,second_item_limit=Config.limit;
                                for(int i=0;i<item_total;i++){
                                    insert_item(first_item_limit,second_item_limit);
                                    first_item_limit=second_item_limit;
                                    second_item_limit=second_item_limit+Config.limit;
                                }

                                JSONArray limitsizesup = obj.getJSONArray("tbl_supplier");
                                JSONObject linitobjectsup = limitsizesup.getJSONObject(0);
                                sup_total = Integer.parseInt(linitobjectsup.getString("count"));
                                sup_datetime = linitobjectsup.getString("sup_datetime");
                                //Toast.makeText(getApplicationContext(),"tbl_supplier="+sup_total,Toast.LENGTH_SHORT).show();
                                int first_sup_limit=0,second_sup_limit=Config.limit;
                                for(int i=0;i<sup_total;i++){
                                    insert_sup(first_sup_limit,second_sup_limit);
                                    first_sup_limit=second_sup_limit;
                                    second_sup_limit=second_sup_limit+Config.limit;
                                }

                                JSONArray limitsizecust = obj.getJSONArray("tbl_customer");
                                JSONObject linitobjectcust = limitsizecust.getJSONObject(0);
                                cust_total = Integer.parseInt(linitobjectcust.getString("count"));
                                cust_datetime = linitobjectcust.getString("cust_datetime");
                                //Toast.makeText(getApplicationContext(),"tbl_customer="+cust_total,Toast.LENGTH_SHORT).show();
                                int first_cust_limit=0,second_cust_limit=Config.limit;
                                for(int i=0;i<cust_total;i++){
                                    insert_cust(first_cust_limit,second_cust_limit);
                                    first_cust_limit=second_cust_limit;
                                    second_cust_limit=second_cust_limit+Config.limit;
                                }

                                JSONArray limitsizepurc = obj.getJSONArray("tbl_inventory");
                                JSONObject linitobjectpurc = limitsizepurc.getJSONObject(0);
                                invt_total = Integer.parseInt(linitobjectpurc.getString("count"));
                                purc_datetime = linitobjectpurc.getString("purc_datetime");
                                //Toast.makeText(getApplicationContext(),"tbl_inventory="+invt_total,Toast.LENGTH_SHORT).show();
                                int first_invt_limit=0,second_invt_limit=Config.limit;
                                for(int i=0;i<invt_total;i++){
                                    insert_invt(first_invt_limit,second_invt_limit);
                                    first_invt_limit=second_invt_limit;
                                    second_invt_limit=second_invt_limit+Config.limit;
                                }

                                JSONArray limitsizetable = obj.getJSONArray("bil_add_table");
                                JSONObject linitobjecttable = limitsizetable.getJSONObject(0);
                                table_total = Integer.parseInt(linitobjecttable.getString("count"));
                                table_datetime = linitobjecttable.getString("table_datetime");
                                int first_table_limit=0,second_table_limit=Config.limit;
                                for(int i=0;i<table_total;i++){
                                    insert_table(first_table_limit,second_table_limit);
                                    first_table_limit=second_table_limit;
                                    second_table_limit=second_table_limit+Config.limit;
                                }


                                JSONArray limitsizewaiter = obj.getJSONArray("bil_add_waiter");
                                JSONObject linitobjectwaiter = limitsizewaiter.getJSONObject(0);
                                waiter_total = Integer.parseInt(linitobjectwaiter.getString("count"));
                                waiter_datetime = linitobjecttable.getString("waiter_datetime");
                                int first_waiter_limit=0,second_waiter_limit=Config.limit;
                                for(int i=0;i<waiter_total;i++){
                                    insert_waiter(first_waiter_limit,second_waiter_limit);
                                    first_waiter_limit=second_waiter_limit;
                                    second_waiter_limit=second_waiter_limit+Config.limit;
                                }

                                setting();
                                point_of_contact();
                                payment();
                                check_user_isactive();
                                sync_times(cid,lid,empid);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(context, "Error2Table=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

//                                    progressDialog.dismiss();

                            Toast.makeText(context, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("lid", "" + lid);
                    params.put("cid", "" + cid);
                    params.put("empid", "" + empid);
                    return params;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(stringRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void payment(){
        final String lid=manager.getLid();
        final String cid=manager.getCid();
        final String empid=manager.getEmpId();
        final List<NameValuePair> paymnetlist=new ArrayList<NameValuePair>();
        try {
            String PaymnetHttpUrlinvt = Config.hosturl+"payment_type.php";
            Log.d("URL", PaymnetHttpUrlinvt);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, PaymnetHttpUrlinvt,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject obj = new JSONObject(response);
                                JSONObject syncid = new JSONObject();
                                //Toast.makeText(getApplicationContext(),"Response="+response,Toast.LENGTH_SHORT).show();
                                JSONArray tutorialsArray = obj.getJSONArray("data");
                                for (int i = 0; i < tutorialsArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject pyamnetObject = tutorialsArray.getJSONObject(i);
                                    db.Syncpayment(pyamnetObject.getString("id"),pyamnetObject.getString("payment_type"),pyamnetObject.getString("is_active"),pyamnetObject.getString("cid"),pyamnetObject.getString("lid"),pyamnetObject.getString("emp_id"),pyamnetObject.getString("sync_flag"),pyamnetObject.getString("created_at"),pyamnetObject.getString("updated_at"));
                                    syncid.put("" + i, pyamnetObject.getString("id"));
                                    //Toast.makeText(getApplicationContext(),pyamnetObject.getString("payment_type"),Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(context, "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Toast.makeText(context, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("lid", "" + lid);
                    params.put("cid", "" + cid);
                    params.put("empid", "" + empid);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setting(){
        final String lid=manager.getLid();
        final String cid=manager.getCid();
        final String empid=manager.getEmpId();
        final List<NameValuePair> settinglist=new ArrayList<NameValuePair>();
        try {
            String HttpUrlpointsetting = Config.hosturl+"setting_api.php";
            Log.d("URL", HttpUrlpointsetting);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrlpointsetting,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject obj = new JSONObject(response);
                                JSONObject settingsyncid = new JSONObject();

                                JSONArray tutorialsArray = obj.getJSONArray("data");
                                for (int i = 0; i < tutorialsArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject settingObject = tutorialsArray.getJSONObject(i);
                                    SyncSetting(settingObject.getString("id"),settingObject.getString("h1"),settingObject.getString("h2"),settingObject.getString("h3"),settingObject.getString("h4"),settingObject.getString("h5"), settingObject.getString("f1"),settingObject.getString("f2"),settingObject.getString("f3"), settingObject.getString("f4"),settingObject.getString("f5"),settingObject.getString("created_at"), settingObject.getString("updated_at"), settingObject.getString("is_active"),settingObject.getString("cid"), settingObject.getString("lid"),settingObject.getString("emp_id"),settingObject.getString("page_size"), settingObject.getString("gst_setting"),settingObject.getString("bill_printing"),settingObject.getString("multiple_print"), settingObject.getString("reset_bill"),settingObject.getString("sync_flag"));
                                    settingsyncid.put("" + i, settingObject.getString("id"));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(context, "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Toast.makeText(context, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("lid", "" + lid);
                    params.put("cid", "" + cid);
                    params.put("empid", "" + empid);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void SyncSetting(String id,String h1,String h2,String h3,String h4,String h5,String f1,String f2,String f3,String f4,String f5,String created_at,String updated_at,String is_active,String cid,String lid,String emp_id,String page_size,String gst_setting,String bill_printing,String multiple_print,String reset_bill,String sync_flag){
        if(gst_setting.equals("Yes")){
            manager.setGstOption("GST Enable");
        }else{
            manager.setGstOption("GST Disable");
        }

    }

    public void check_user_isactive(){
        final String empid=manager.getEmpId();
        try {
            String HttpUrluseractive = Config.hosturl+"check_user_isactive.php";
            Log.d("URL", HttpUrluseractive);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrluseractive,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject obj = new JSONObject(response);
                                JSONArray tutorialsArray = obj.getJSONArray("data");
                                for (int i = 0; i < tutorialsArray.length(); i++) {
                                    JSONObject isactivegObject = tutorialsArray.getJSONObject(i);
                                    Toast.makeText(context,"ISactive in admin="+isactivegObject.getString("is_active"),Toast.LENGTH_SHORT).show();
                                    manager.setIsActive(isactivegObject.getString("is_active"));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(context, "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Toast.makeText(context, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("empid", "" + empid);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sync_times(String tcid, String tlid,String tempid){
        final String lid=manager.getLid();
        final String cid=manager.getCid();
        final String empid=manager.getEmpId();

        try {
            String HttpUrltime = Config.hosturl+"sync_time_api.php";
            Log.d("URL", HttpUrltime);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrltime,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject objtime = new JSONObject(response);

                                JSONArray tutorialsArraytime = objtime.getJSONArray("data");
                                for (int i = 0; i < tutorialsArraytime.length(); i++) {
                                    JSONObject timeObject = tutorialsArraytime.getJSONObject(i);
                                    manager.setUploadInterval(timeObject.getString("upload_interval"));
                                    manager.setDownloadInterval(timeObject.getString("download_interval"));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(context, "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Toast.makeText( context, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("lid", "" + lid);
                    params.put("cid", "" + cid);
                    params.put("empid", "" + empid);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void uploaddata() {
        final String lid=manager.getLid();
        final String cid=manager.getCid();
        final String empid=manager.getEmpId();

        final List<String> master_bill_id_list=new ArrayList<>();
        int total_rows=0,startlimit=0,endlimit=0,divide=0,mod=0,finalcount=0;
        total_rows=db.total_rows_bill_master();
        Toast.makeText(context,"Master Total Rows="+total_rows, Toast.LENGTH_SHORT).show();
        startlimit=0;
        endlimit=Config.import_limit;

        if(total_rows>Config.import_limit){
            divide=total_rows/Config.import_limit;
            if(total_rows%Config.import_limit==0){
                mod=0;
            }else {
                mod=1;
            }
            finalcount=divide+mod;
        }else {
            finalcount=1;
        }
        //Toast.makeText(getApplicationContext(),"Master loops="+finalcount, Toast.LENGTH_SHORT).show();
        for(int r=0;r<finalcount;r++){
            master_bill_id_list.clear();
            Cursor c=db.getbillmaster(startlimit,endlimit);
            if(c==null || c.getCount() == 0) {

            }else {
                StringBuffer buffer = new StringBuffer();
                JSONObject jsonObject = new JSONObject();
                JSONArray arr = new JSONArray();
                ArrayList<String> personNames = new ArrayList<>();
                final JSONArray array = new JSONArray();
                personNames.clear();

                while (c.moveToNext()) {
                    try {
                        master_bill_id_list.add(c.getString(0));
                        JSONObject obj = new JSONObject();
                        obj.put("app_bill_id", c.getString(1));
                        obj.put("bill_date", c.getString(8));
                        obj.put("cust_id",c.getString(3));
                        obj.put("cash_or_credit",c.getString(4));
                        obj.put("discount",c.getString(5));
                        obj.put("gst_setting",c.getString(19));
                        obj.put("bill_totalamt",c.getString(6));
                        obj.put("bill_tax",c.getString(7));
                        obj.put("created_at_TIMESTAMP",c.getString(8));
                        obj.put("updated_at_TIMESTAMP",c.getString(9));
                        obj.put("isactive",c.getString(10));
                        obj.put("cid",cid);
                        obj.put("lid",lid);
                        obj.put("emp_id",empid);
                        obj.put("android_bill_id",c.getString(11));
                        obj.put("sync_flag",2);
                        obj.put("point_of_contact",c.getString(16));
                        obj.put("payment_details",c.getString(17));
                        obj.put("order_details",c.getString(18));
                        obj.put("bill_code",c.getString(20));
                        obj.put("remark",c.getString(21));
                        array.put(obj);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    jsonObject.put("array", array);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.i("Array", "" + jsonObject);
                //Toast.makeText(getApplicationContext(),""+array,Toast.LENGTH_LONG).show();

                try {
                    String HttpUrl = Config.hosturl + "app_api.php";
                    Log.d("URL", HttpUrl);
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        Toast.makeText(context, "Response=Sync Done Successfully", Toast.LENGTH_LONG).show();
                                        Log.d("testData=",response);
                                        db.updateBillMasterIDs(master_bill_id_list);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        Toast.makeText(context, "updated Error2=" + e, Toast.LENGTH_SHORT).show();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError volleyError) {
//                                    progressDialog.dismiss();
                                    Toast.makeText(context, volleyError.toString(), Toast.LENGTH_LONG).show();
                                }
                            }) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("json_array", "" + array);
                            params.put("lid", "" + lid);
                            params.put("cid", "" + cid);
                            params.put("empid", "" + empid);
                            return params;
                        }
                    };

                    RequestQueue requestQueue = Volley.newRequestQueue(context);
                    requestQueue.add(stringRequest);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            startlimit=endlimit;
            endlimit=endlimit+Config.import_limit;
        }
    }

    public void uploaddatabilldetails() {

        final String lid=manager.getLid();
        final String cid=manager.getCid();
        final String empid=manager.getEmpId();

        final List<String> details_bill_id_list=new ArrayList<>();

        int total_rows=0,startlimit=0,endlimit=0,divide=0,mod=0,finalcount=0;
        total_rows=db.total_rows_bill_deatils();
        Toast.makeText(context,"Details Total Rows="+total_rows, Toast.LENGTH_SHORT).show();
        startlimit=0;
        endlimit=Config.import_limit;

        if(total_rows>Config.import_limit){
            divide=total_rows/Config.import_limit;
            if(total_rows%Config.import_limit==0){
                mod=0;
            }else {
                mod=1;
            }
            finalcount=divide+mod;
        }else {
            finalcount=1;
        }
        //Toast.makeText(getApplicationContext(),"Details loops="+finalcount, Toast.LENGTH_SHORT).show();
        for(int r=0;r<finalcount;r++) {
            details_bill_id_list.clear();
            Cursor c = db.getbillmasterdetails(startlimit,endlimit);
            if (c == null || c.getCount() == 0) {

            } else {
                StringBuffer buffer = new StringBuffer();
                JSONObject jsonObject = new JSONObject();
                JSONArray arr = new JSONArray();

                ArrayList<String> personNames = new ArrayList<>();
                final JSONArray array = new JSONArray();
                personNames.clear();

                while (c.moveToNext()) {
                    try {
                        details_bill_id_list.add(c.getString(0));
                        JSONObject obj = new JSONObject();
                        obj.put("bill_no", c.getString(1));
                        obj.put("item_name", c.getString(2));
                        obj.put("item_qty", c.getString(3));
                        obj.put("item_rate", c.getString(4));
                        obj.put("item_totalrate", c.getString(5));
                        obj.put("created_at_TIMESTAMP", c.getString(6));
                        obj.put("updated_at_TIMESTAMP", c.getString(7));
                        obj.put("isactive", c.getString(8));
                        obj.put("cid", cid);
                        obj.put("lid", lid);
                        obj.put("emp_id", empid);
                        obj.put("android_bill_id", c.getString(9));
                        obj.put("bill_code", c.getString(14));
                        obj.put("sync_flag", c.getString(13));
                        array.put(obj);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                try {
                    jsonObject.put("array", array);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.i("Array", "" + jsonObject);

                try {
                    String HttpUrl = Config.hosturl + "bill_detail_api.php";
                    Log.d("URL", HttpUrl);
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        Toast.makeText(context, "Response=Sync Done Successfully", Toast.LENGTH_LONG).show();
                                        db.updateBillDetailsIDs(details_bill_id_list);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        Toast.makeText(context, "updated Error2=" + e, Toast.LENGTH_SHORT).show();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError volleyError) {

//                                    progressDialog.dismiss();

                                    Toast.makeText(context, volleyError.toString(), Toast.LENGTH_LONG).show();
                                }
                            }) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("json_array", "" + array);
                            params.put("lid", "" + lid);
                            params.put("cid", "" + cid);
                            params.put("empid", "" + empid);
                            return params;
                        }
                    };

                    RequestQueue requestQueue = Volley.newRequestQueue(context);
                    requestQueue.add(stringRequest);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            startlimit=endlimit;
            endlimit=endlimit+Config.import_limit;
        }

    }

    public void insert_table(final int firstcatcnt, final int seconcatcnt) {
        final String lid=manager.getLid();
        final String cid=manager.getCid();
        final String empid=manager.getEmpId();
        final List<NameValuePair> list=new ArrayList<NameValuePair>();

        try {
            String HttpUrl = Config.hosturl+"table_api.php";
            Log.d("URL", HttpUrl);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject obj = new JSONObject(response);
                                JSONObject syncid = new JSONObject();
                                //Toast.makeText(getApplicationContext(),"Rsponse Cat"+obj,Toast.LENGTH_LONG).show();
                                JSONArray tutorialsArray = obj.getJSONArray("data");
                                for (int i = 0; i < tutorialsArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject catObject = tutorialsArray.getJSONObject(i);
                                    db.SyncTable(catObject.getString("table_id"), catObject.getString("table_no"), catObject.getString("capacity"), catObject.getString("is_active"),catObject.getString("created_at"),catObject.getString("updated_at"),catObject.getString("cid"),catObject.getString("lid"),catObject.getString("emp_id"));
                                    syncid.put("" + i, catObject.getString("table_id"));
                                }
                                prev_table_datetime=table_datetime;
                                manager.setTableDatetime(prev_table_datetime);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(context, "ErrorTABLE=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

//                                    progressDialog.dismiss();

                            Toast.makeText(context, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("firstlimt", "" + firstcatcnt);
                    params.put("secondlimit", "" + seconcatcnt);
                    params.put("lid", "" + lid);
                    params.put("cid", "" + cid);
                    params.put("empid", "" + empid);
                    params.put("table_datetime", "" + prev_table_datetime);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(context);

            requestQueue.add(stringRequest);

        }catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void insert_waiter(final int firstcatcnt, final int seconcatcnt) {
        final String lid=manager.getLid();
        final String cid=manager.getCid();
        final String empid=manager.getEmpId();
        final List<NameValuePair> list=new ArrayList<NameValuePair>();

        try {
            String HttpUrl = Config.hosturl+"waiter_api.php";
            Log.d("URL", HttpUrl);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject obj = new JSONObject(response);
                                JSONObject syncid = new JSONObject();
                                //Toast.makeText(getApplicationContext(),"Rsponse Cat"+obj,Toast.LENGTH_LONG).show();
                                JSONArray tutorialsArray = obj.getJSONArray("data");
                                for (int i = 0; i < tutorialsArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject catObject = tutorialsArray.getJSONObject(i);
                                    db.SyncWaiter(catObject.getString("id"), catObject.getString("waiter_name"), catObject.getString("cid"), catObject.getString("lid"),catObject.getString("emp_id"),catObject.getString("is_active"),catObject.getString("created_at"),catObject.getString("updated_at"));
                                    syncid.put("" + i, catObject.getString("id"));
                                }
                                prev_waiter_datetime=waiter_datetime;
                                manager.setTableDatetime(prev_waiter_datetime);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(context, "ErrorWaiter=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

//                                    progressDialog.dismiss();

                            Toast.makeText(context, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("firstlimt", "" + firstcatcnt);
                    params.put("secondlimit", "" + seconcatcnt);
                    params.put("lid", "" + lid);
                    params.put("cid", "" + cid);
                    params.put("empid", "" + empid);
                    params.put("waiter_datetime", "" + prev_waiter_datetime);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(context);

            requestQueue.add(stringRequest);

        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
