package com.iping.airbillonline.utils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ThreadExecutor {

    private static ThreadExecutor threadExecutor;
    private ExecutorService executor;

    private ThreadExecutor(){
        executor= Executors.newFixedThreadPool(10);

    }

    public synchronized static ThreadExecutor getInstance(){
        if (threadExecutor==null) {
            threadExecutor = new ThreadExecutor();
        }
        return threadExecutor;
    }

    public void execute(Runnable runnable){
        executor.execute(runnable);
    }

    public Future<?> submit(Runnable runnable){
        return  executor.submit(runnable);
    }
}
