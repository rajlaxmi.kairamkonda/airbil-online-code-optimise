package com.iping.airbillonline.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

public class SessionManager {

    @SuppressLint("StaticFieldLeak")
    private static SessionManager manager;

    private static final String SHARED_PREF_NAME = "airbill";

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    private SessionManager(Context context) {
        sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
    }

    public static SessionManager getInstance(Context context){
        if (manager==null)
            manager=new SessionManager(context);
        return manager;
    }

    private static final String SELECT_PRINTER ="selectprinter_option";
    private static final String PRINT_OPTION="print_option";
    private static final String GST_OPTION = "gst_option";
    private static final String MULTIPLE_OPTION = "multiple_printing";
    private static final String BILL_WITH_PRINT ="bill_with_print";
    private static final String RESET_BILL="reset_bill";

    private static final String PANEL="panel";
    private static final String USERNAME="Username";
    private static final String FIRSTTIMELOGIN="FirstTimeLogin";

    private static final String INSTALL_DATE="install_date";
    private static final String EXPIRETIME="Expire_date";

    private static final String LID="lid";
    private static final String CID = "cid";
    private static final String EMP_ID = "emp_id";
    private static final String EMPLOYEE_CODE="employee_code";
    private static final String CAT_DATETIME="cat_datetime";
    private static final String UNIT_DATETIME="unit_datetime";
    private static final String ITEM_DATETIME="item_datetime";
    private static final String SUP_DATETIME="sub_datetime";
    private static final String CUST_DATETIME="cust_datetime";
    private static final String PURC_DATETIME="purc_datetime";
    private static final String TABLE_DATETIME="tableDatetime";
    private static final String WAITER_DATETIME="waiterDatetime";
    private static final String TOKEN="token";
    private static final String TOKEN_FLAG="token_flag";
    private static final String UPLOAD_INTERVAL="upload_interval";
    private static final String DOWNLOAD_INTERVAL="download_interval";
    private static final String IS_ACTIVE="is_active";

    public void setIsActive(String downloadInterval){
        editor = sharedPreferences.edit();
        editor.putString(IS_ACTIVE, downloadInterval);
        editor.apply();
    }

    public String getIsActive(){
        return sharedPreferences.getString(IS_ACTIVE,"in valid");
    }


    public void setDownloadInterval(String downloadInterval){
        editor = sharedPreferences.edit();
        editor.putString(DOWNLOAD_INTERVAL, downloadInterval);
        editor.apply();
    }

    public String getDownloadInterval(){
        return sharedPreferences.getString(DOWNLOAD_INTERVAL,"in valid");
    }

    public void setUploadInterval(String uploadInterval){
        editor = sharedPreferences.edit();
        editor.putString(UPLOAD_INTERVAL, uploadInterval);
        editor.apply();
    }

    public String getUploadInterval(){
        return sharedPreferences.getString(UPLOAD_INTERVAL,"in valid");
    }

    public void setTokenFlag(String token){
        editor = sharedPreferences.edit();
        editor.putString(TOKEN_FLAG, token);
        editor.apply();
    }

    public String getTokenFlag(){
        return sharedPreferences.getString(TOKEN_FLAG,"in valid");
    }

    public void setToken(String token){
        editor = sharedPreferences.edit();
        editor.putString(TOKEN, token);
        editor.apply();
    }

    public String getToken(){
        return sharedPreferences.getString(TOKEN,"in valid");
    }

    public void setPurcDatetime(String purcDatetime){
        editor = sharedPreferences.edit();
        editor.putString(PURC_DATETIME, purcDatetime);
        editor.apply();
    }

    public String getPurcDatetime(){
        return sharedPreferences.getString(PURC_DATETIME,"in valid");
    }

    public void setCustDatetime(String custDatetime){
        editor = sharedPreferences.edit();
        editor.putString(CUST_DATETIME, custDatetime);
        editor.apply();
    }

    public String getCustDatetime(){
        return sharedPreferences.getString(CUST_DATETIME,"in valid");
    }

    public void setSupDatetime(String supDatetime){
        editor = sharedPreferences.edit();
        editor.putString(SUP_DATETIME, supDatetime);
        editor.apply();
    }

    public String getSupDatetime(){
        return sharedPreferences.getString(SUP_DATETIME,"in valid");
    }

    public void setItemDatetiime(String itemDatetiime){
        editor = sharedPreferences.edit();
        editor.putString(ITEM_DATETIME, itemDatetiime);
        editor.apply();
    }

    public String getItemDatetime(){
        return sharedPreferences.getString(ITEM_DATETIME,"in valid");
    }

    public void setUnitDatetime(String unitDatetime){
        editor = sharedPreferences.edit();
        editor.putString(UNIT_DATETIME, unitDatetime);
        editor.apply();
    }

    public String getUnitDatetime(){
        return sharedPreferences.getString(UNIT_DATETIME,"in valid");
    }



    public void setCatDatetime(String catDatetime){
        editor = sharedPreferences.edit();
        editor.putString(CAT_DATETIME, catDatetime);
        editor.apply();
    }

    public String getCatDatetime(){
        return sharedPreferences.getString(CAT_DATETIME,"in valid");
    }


    public void setEmployeeCode(String employeeCode){
        editor = sharedPreferences.edit();
        editor.putString(EMPLOYEE_CODE, employeeCode);
        editor.apply();
    }

    public String getEmployeeCode(){
        return sharedPreferences.getString(EMPLOYEE_CODE,"in valid");
    }




    public void setEmpID(String empID){
        editor = sharedPreferences.edit();
        editor.putString(EMP_ID, empID);
        editor.apply();
    }

    public String getEmpId(){
        return sharedPreferences.getString(EMP_ID,"in valid");
    }


    public void setCid(String cid){
        editor = sharedPreferences.edit();
        editor.putString(CID, cid);
        editor.apply();
    }

    public String getCid(){
        return sharedPreferences.getString(CID,"in valid");
    }


    public void setLid(String lid){
        editor = sharedPreferences.edit();
        editor.putString(LID, lid);
        editor.apply();
    }

    public String getLid(){
         return sharedPreferences.getString(LID,"in valid");
    }

    public void setExpiretime(String expiretime){
        editor = sharedPreferences.edit();
        editor.putString(EXPIRETIME, expiretime);
        editor.apply();
    }

    public String getExpiretime(){
        return sharedPreferences.getString(EXPIRETIME,"");
    }

    public void setFirsttimelogin(int login){
        editor = sharedPreferences.edit();
        editor.putInt(FIRSTTIMELOGIN, login);
        editor.apply();
    }

    public int getFirsttimelogin(){
        return sharedPreferences.getInt(FIRSTTIMELOGIN,-1);
    }

    public void setUsername(String username){
        editor = sharedPreferences.edit();
        editor.putString(USERNAME, username);
        editor.apply();
    }

    public String getUsername(){
        return sharedPreferences.getString(USERNAME,"username");
    }

    public void setPanel(String panel){
        editor = sharedPreferences.edit();
        editor.putString(PANEL, panel);
        editor.apply();
    }

    public String getPanel(){
        return sharedPreferences.getString(PANEL,"panel");
    }

    public void setResetBill(String option){
        editor = sharedPreferences.edit();
        editor.putString(RESET_BILL, option);
        editor.apply();
    }

    public String getResetBill(){
        return sharedPreferences.getString(RESET_BILL,"device");
    }

    public void setBillWithPrint(String option){
        editor = sharedPreferences.edit();
        editor.putString(BILL_WITH_PRINT, option);
        editor.apply();
    }

    public String getBillWithPrint(){
        return sharedPreferences.getString(BILL_WITH_PRINT, "device");
    }

    public void setMultipleOption(String option){
        editor = sharedPreferences.edit();
        editor.putString(MULTIPLE_OPTION, option);
        editor.apply();
    }

    public String getMultipleOption(){
        return sharedPreferences.getString(MULTIPLE_OPTION, "device");
    }

    public void setGstOption(String option){
        editor = sharedPreferences.edit();
        editor.putString(GST_OPTION, option);
        editor.apply();
    }

    public String getGstOption(){
        return sharedPreferences.getString(GST_OPTION, "Device");
    }

    public void setPrinter(String printerName){
        editor = sharedPreferences.edit();
        editor.putString(SELECT_PRINTER, printerName);
        editor.apply();
    }

    public String getPrinter(){
        return sharedPreferences.getString(SELECT_PRINTER, "Device");
    }

    public void setPrintOption(String option){
        editor = sharedPreferences.edit();
        editor.putString(PRINT_OPTION, option);
        editor.apply();
    }

    public String getPrintOption(){
        return sharedPreferences.getString(PRINT_OPTION, "option");
    }

    public void setInstallDate(String installDate) {
        editor = sharedPreferences.edit();
        editor.putString(INSTALL_DATE, installDate);
        editor.apply();
    }

    public void setTableDatetime(String tableDatetime){
        editor = sharedPreferences.edit();
        editor.putString(TABLE_DATETIME, tableDatetime);
        editor.apply();
    }

    public String getTableDatetime(){
        return sharedPreferences.getString(TABLE_DATETIME,"in valid");
    }


    public void setWaiterDatetime(String waiterDatetime){
        editor = sharedPreferences.edit();
        editor.putString(WAITER_DATETIME, waiterDatetime);
        editor.apply();
    }

    public String getWaiterDatetime(){
        return sharedPreferences.getString(WAITER_DATETIME,"in valid");
    }

    public String getInstallDate(){
        return sharedPreferences.getString(INSTALL_DATE, "");
    }
}