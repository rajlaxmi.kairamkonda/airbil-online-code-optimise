package com.iping.airbillonline.utils;

public interface IRequestListener {

    void onComplete();

    void onError(String message);
}
