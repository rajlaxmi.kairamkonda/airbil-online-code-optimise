package com.iping.airbillonline.pojo;

public class ItemListPOJO {

    private String code;
    private String name;
    private String rate;


    public ItemListPOJO(String code, String name, String rate) {
        this.code = code;
        this.name = name;
        this.rate = rate;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
