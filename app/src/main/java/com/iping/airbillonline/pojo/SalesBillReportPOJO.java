package com.iping.airbillonline.pojo;

public class SalesBillReportPOJO {
    private String serNo;
    private String billNo;
    private String totalAmount;
    private String cashCredit;
    private String bdate;

    public SalesBillReportPOJO(String serNo, String billNo, String totalAmt, String cashCredit,String bdate){
        this.serNo = serNo;
        this.billNo=billNo;
        this.totalAmount=totalAmt;
        this.cashCredit=cashCredit;
        this.bdate=bdate;
    }

    public String getTotalAmt() {
        return totalAmount;
    }

    public void setTotalAmt(String totalAmt) {
        this.totalAmount = totalAmt;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getCashCredit() {
        return cashCredit;
    }

    public void setCashCredit(String cashCredit) {
        this.cashCredit = cashCredit;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getBdate() {
        return bdate;
    }

    public void setBdate(String bdate) {
        this.bdate = bdate;
    }
}
