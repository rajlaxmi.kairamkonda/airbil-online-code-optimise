package com.iping.airbillonline.pojo;

public class DeletedSalesBillReportPOJO {

    private String serNo;
    private String billNo;
    private String totalAmount;
    private String cashCredit;

    public DeletedSalesBillReportPOJO(String serNo, String billNo, String totalAmt, String cashCredit){
        this.serNo = serNo;
        this.billNo=billNo;
        this.totalAmount=totalAmt;
        this.cashCredit=cashCredit;
    }

    public String getTotalAmt() {
        return totalAmount;
    }

    public void setTotalAmt(String totalAmt) {
        this.totalAmount = totalAmt;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getCashCredit() {
        return cashCredit;
    }

    public void setCashCredit(String cashCredit) {
        this.cashCredit = cashCredit;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }
}


